#ifdef  _MSC_VER
#pragma warning(disable : 4255 4668 4820 )
#pragma setlocale( "C" )
#endif

#include <windows.h>

#include <stdlib.h>
#include <wchar.h>
#include <stdint.h>

#include "stdioshim.h"
#include "cryptoshim.h"

int wmain(void)
{
	wchar_t s[256 / sizeof(wchar_t)];
	short int i = 0;

	(void)SecureZeroMemory(&s, 256);

	printf("We are here: 00.\n");
	if (getentropy(s, 256)) return EXIT_FAILURE;

	printf("We are here: 01.\n");
	while (i < 256 / sizeof(wchar_t))
	{
		/*
		 * Visual Studio 2015 specifies h for wchar_t,
		 * while Visual Studio 2013 accepts w for wchar_t.
		 */
		wprintf(L"s[%03hd]: 0x%04hx\n", i, s[i]);
		i++;
	}

	return EXIT_SUCCESS;
}
