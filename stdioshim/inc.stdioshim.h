#define printf stdioshim_printf
#define fprintf stdioshim_fprintf
#define wprintf stdioshim_wprintf
#define fwprintf stdioshim_fwprintf

/*_
 * fwprintf()
 *
 * MSDN: https://msdn.microsoft.com/en-us/library/239ffwa0.aspx *
 * Open Group: http://pubs.opengroup.org/onlinepubs/9699919799/functions/fwprintf.html *
 */
int printf(const char *__restrict format, ...);
int fprintf(FILE *__restrict stream, const char *__restrict format, ...);
int wprintf(const wchar_t *__restrict format, ...);
int fwprintf(FILE *__restrict stream, const wchar_t *format, ...);
