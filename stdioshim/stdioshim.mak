!include ../own.mak

all: stdioshim.dll cleanexe.exe

stdioshim.dll: fwprintf.obj wprintf.obj fprintf.obj printf.obj
!ifndef NDEBUG
	$(LINK) $(LINKFLAGS) /incremental:no /dll /def:stdioshim.def /out:stdioshim.dll /pdb:stdioshim.pdb fwprintf.obj wprintf.obj fprintf.obj printf.obj
!else
	$(LINK) $(LINKFLAGS) /incremental:no /dll /def:stdioshim.def /out:stdioshim.dll fwprintf.obj wprintf.obj fprintf.obj printf.obj
!endif

cleanexe.exe: cleanexe.c
	$(CC) $(CFLAGS) $(CEFLAGS) $(RTCFLAGS) cleanexe.c /link $(LINKFLAGS) /incremental:no user32.lib

fwprintf.obj: fwprintf.c stdioshim.h
	$(CC) $(CFLAGS) $(CEFLAGS) $(RTCFLAGS) /DSTDIOSHIM_MUTEX_FWPRINTF /Fofwprintf.obj /c fwprintf.c

wprintf.obj: fwprintf.c stdioshim.h
	$(CC) $(CFLAGS) $(CEFLAGS) $(RTCFLAGS) /DSTDIOSHIM_MUTEX_WPRINTF /Fowprintf.obj /c fwprintf.c

fprintf.obj: fwprintf.c stdioshim.h
	$(CC) $(CFLAGS) $(CEFLAGS) $(RTCFLAGS) /DSTDIOSHIM_MUTEX_FPRINTF /Fofprintf.obj /c fwprintf.c

printf.obj: fwprintf.c stdioshim.h
	$(CC) $(CFLAGS) $(CEFLAGS) $(RTCFLAGS) /DSTDIOSHIM_MUTEX_PRINTF /Foprintf.obj /c fwprintf.c

clean:
	del /q *.obj *.exp *.lib *.dll *.pdb
