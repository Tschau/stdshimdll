#ifdef  _MSC_VER
#pragma warning( disable : 4255 4668 4820 )
#pragma setlocale( "C" )
#endif

#if defined(__cplusplus)
#define	BEGIN_EXTERN_C extern "C" {
#define	END_EXTERN_C }
#else
#define	BEGIN_EXTERN_C
#define	END_EXTERN_C
#endif

BEGIN_EXTERN_C
#include <windows.h>
#include <strsafe.h> /* StringCchLengthW. */

#include <malloc.h> /* _aligned_malloc(). */
#include <stdio.h> /* FILE, _vsnwprintf(), _vsnwprintf_s(), stdout. */
#include <stdarg.h> /* va_list, va_start, va_end, vfwprintf_s(). */
#include <errno.h> /* errno_t et al. */
#include <stdint.h>

#ifdef _MSC_VER
#define restrict __restrict
#define inline __inline
#endif

#include "stdioshim.h"

/*_
 * STDIOSHIM_U16_REPLACEMENT_CHARACTER
 *
 * Unicode codepoints only representable in UCS-4 are translated to U+FFFD,
 * since Unicode support in Windows Console is limited to UCS-2.
 */

/*_
 * $(CC) is supposed to set one of the macros that ensue, *
 * the priority of which comes in descendant order. *
 *
 * STDIOSHIM_MUTEX_PRINTF *
 * STDIOSHIM_MUTEX_FPRINTF *
 * STDIOSHIM_MUTEX_WPRINTF *
 * STDIOSHIM_MUTEX_FWPRINTF *
 *
 * DO NOT SET STDIOSHIM_MUTEX_* MACROS w/ $(CC)! *
 */

/* EXACTLY ONE of those flags that ensue is meant to be set. */
#if defined( STDIOSHIM_MUTEX_PRINTF )
#define STDIOSHIMFWPRINTF_PRINTF
#elif defined( STDIOSHIM_MUTEX_FPRINTF )
#define STDIOSHIMFWPRINTF_FPRINTF
#elif defined( STDIOSHIM_MUTEX_WPRINTF )
#define STDIOSHIMFWPRINTF_WPRINTF
#else /* defined( STDIOSHIM_MUTEX_FWPRINTF ) */
#define STDIOSHIMFWPRINTF_FWPRINTF
#endif

/*static inline uint_least16_t ___local_gcdof_least16___(uint_least16_t m, uint_least16_t n)
{
	uint_least16_t i = 0;

	if (0 == m && 0 != n) return n;
	else if (m == n) return m;

	while (n)
	{
		i = m;
		m = n;
		n = i%n;
	}
	return m;
}*/

/* m != 0 && n != 0 is assumed. */
/*static inline uint_least32_t ___local_lcmof_least16___(uint_least16_t m, uint_least16_t n)
{
	uint_least16_t i = ___local_gcdof_least16___(m, n);
	m /= i;
	return m*n;
}*/

/*_
 * cryptoshim_fwprintf
 *
 * NOTE: cryptoshim_fwprintf() outputs Unicode without _setmode from CRT.
 * NOTE: WriteConsoleW is used EXCLUSIVELY.
 *
 * EINVAL is set if format is NULL.
 *
 * EOVERFLOW is set if required intermediate buffers are deemed too large to fulfil.
 * EOVERFLOW is set if the size of format exceeds STDIOSHIM_STDC_OBJ_THLD_CBLEN.
 *
 * EILSEQ is set if illegal Unicode sequence is found through WideCharToMultiByte()
 * within format or those derived therefrom.
 *
 * ENOMEM is set if memory allocation fails.
 *
 * NOTE: EOVERFLOW is specified with http://pubs.opengroup.org/onlinepubs/9699919799/functions/fwprintf.html
 * NOTE: ERANGE is specified with https://msdn.microsoft.com/en-us/library/d3xd30zz.aspx
 * NOTE: EOVERFLOW is preferred over ERANGE.
 * NOTE: http://man.openbsd.org/OpenBSD-current/man2/intro.2
 * NOTE: http://netbsd.gw.com/cgi-bin/man-cgi?errno++NetBSD-current
 *
 * WideCharToMultiByte: https://msdn.microsoft.com/en-us/library/windows/desktop/dd374130.aspx
 *
 * NOTE: WriteConsoleW records the number of characters written with DWORD,
 * while the return type for the printf family of functions is int.
 *
 * DWORD: https://msdn.microsoft.com/en-us/library/cc230318.aspx
 * and https://msdn.microsoft.com/en-us/library/s3f49ktz.aspx ;
 * WriteConsole: https://msdn.microsoft.com/en-us/library/windows/desktop/ms687401.aspx .
 *
 * PS: NDEBUG activates internal parameter validation.
 *
 * PS: In maximal compliance with 7.19.1.1, 7.19.6.3, 7.24.2.1, and 7.24.2.11 of ISO/IEC 9899:TC3,
 * PS: available at http://www.open-std.org/jtc1/sc22/WG14/www/docs/n1256.pdf ,
 * PS: and 7.21.6.1, 7.21.6.3, 7.29.2.1 and 7.29.2.11 of ISO/IEC 9899:201x,
 * PS: available at http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1570.pdf ,
 * PS: the length of the output string is capped at 4095, excluding the terminating null, for the sake of portability:
 * PS: "The number of characters that can be produced by any single conversion shall be at least 4095."
 *
 * PS: MSFT's fwprintf_s() is called for regular files,
 * PS: along with non-console character devices as deemed by _isatty().
 * PS: Otherwise, output is mangled so that it conforms to UCS-2,
 * PS: if STDIOSHIM_U16_REPLACEMENT_CHARACTER is defined.
 */
#if defined( STDIOSHIMFWPRINTF_PRINTF )
int stdioshim_printf(const char *restrict format, ...)
#elif defined( STDIOSHIMFWPRINTF_FPRINTF )
int stdioshim_fprintf(FILE *restrict stream, const char *restrict format, ...)
#elif defined( STDIOSHIMFWPRINTF_WPRINTF )
int stdioshim_wprintf(const wchar_t *restrict format, ...)
#else
int stdioshim_fwprintf(FILE *restrict stream, const wchar_t *restrict format, ...)
#endif
{
#ifdef STDIOSHIM_U16_REPLACEMENT_CHARACTER
	int idxPostFmtBuffer = 0, subidxPostFmtBuffer = 0;
	int bSurrogate = 0;
	wchar_t Utf16CodeUnit = L'\x0000';
#endif /* #ifdef STDIOSHIM_U16_REPLACEMENT_CHARACTER */
#if defined( STDIOSHIMFWPRINTF_PRINTF ) || defined( STDIOSHIMFWPRINTF_FPRINTF )
	char *U8PostFmtBuffer = NULL;
	int U8PostFmtCch = 0;
	unsigned int U8PostFmtMaxCch = 0;
	size_t U8PostFmtCb = 0;
#endif
	size_t AnteFmtCch = 0; /* size_t is required by StringCchLengthW(). */

#ifndef NDEBUG
	int nNumberOfCharsToWrite = 0; /* _vsnwprintf() returns int, NOT size_t. */
	int nNumberOfCharsWritten = 0; /* vide supra. */
	/* _vsnwprintf: https://msdn.microsoft.com/en-us/library/1kt27hek.aspx */
#endif

	/*
	 * format is the ante-formatting string, while *
	 * PostFmtBuffer is the post-formatting one. *
	 */
	wchar_t *PostFmtBuffer = NULL;
	int PostFmtCch = 0;
	unsigned int PostFmtMaxCch = 0;
	size_t PostFmtCb = 0;

	/*DWORD flProtect = 0;*/

	va_list ap;
	HANDLE hConsole = NULL;
	DWORD ConsoleMode = 0;
	/*_
	 * C11 guarantees a minimum of maximal compiler-sponsored object size of 65,535 in bytes.
	 * As such, ___local_gcdof_least16___() and ___local_lcmof_least16___() might need rewriting,
	 * for portability across STDC verions.
	 *
	 * However, this is the ONLY use case for BOTH ___local_gcdof_least16___() and ___local_lcmof_least16___().
	 * Unless certain compiler extension/flaw permits sizeof to return zero for empty structs,
	 * and some jerk redefined wchar_t as an empty struct, this line is safe.
	 *
	 * As the name suggests, this variable is to keep track of the REAL offset of a wchar_t variable,
	 * with alignment considered. Its ONLY use is in the ONLY MoveMemory() instance.
	 *
	 * Regardless, it is wise ALWAYS to avoid raw memory operations unless required.
	 */
	/*const size_t OffsetOfWchar = ___local_lcmof_least16___(sizeof(wchar_t), __alignof(wchar_t));*/

	(void)SecureZeroMemory(&ap, sizeof(va_list));

#if defined( STDIOSHIMFWPRINTF_PRINTF ) || defined( STDIOSHIMFWPRINTF_WPRINTF )
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
#else
	if (NULL == stream || stdin == stream) goto jreturn_e_inval;
	else if (stdout == stream) hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	else if (stderr == stream) hConsole = GetStdHandle(STD_ERROR_HANDLE);
	else goto jreturn_file;
	
	if (FILE_TYPE_CHAR != GetFileType(hConsole)) goto jreturn_file;
#endif
	if (NULL == hConsole || INVALID_HANDLE_VALUE == hConsole) goto jreturn_e;
	else if (!GetConsoleMode(hConsole, &ConsoleMode)) { SetLastError(ERROR_SUCCESS);  goto jreturn_file; }

	if (NULL == format) goto jreturn_e_inval;

#if defined( STDIOSHIMFWPRINTF_PRINTF ) || defined( STDIOSHIMFWPRINTF_FPRINTF )
	AnteFmtCch = STDIOSHIM_STDC_OBJ_THLD_CBLEN / sizeof(char);
	if (FAILED(StringCchLengthA(format, AnteFmtCch, &AnteFmtCch))) goto jreturn_e_overflow;
	if (0 == AnteFmtCch) { goto jreturn_idle; }
	/*_
	 * NOTE: format comes with a character length of 1 or 1+ at this point, terminating null excluded.
	 * NOTE: StringCchLength: https://msdn.microsoft.com/en-us/library/windows/desktop/ms647539.aspx
	 *
	 * ERROR_NO_UNICODE_TRANSLATION == GetLastError() is assumed.
	 *
	 * size_t to int conversion here is safe,
	 * since AnteFmtCch tops at STDIOSHIM_STDC_OBJ_THLD_CBLEN / sizeof(char).
	 */
	if (0 == MultiByteToWideChar(
		CP_UTF8,
		MB_ERR_INVALID_CHARS,
		format,
		(int)AnteFmtCch,
		NULL,
		0
		)) goto jreturn_e_ilseq;
#else
	AnteFmtCch = STDIOSHIM_STDC_OBJ_THLD_CBLEN / sizeof(wchar_t);
	if (FAILED(StringCchLengthW(format, AnteFmtCch, &AnteFmtCch))) goto jreturn_e_overflow;
	if (0 == AnteFmtCch) { goto jreturn_idle; }
	/*_
	 * NOTE: format comes with a wide character length of 1 or 1+ at this point, terminating null excluded.
	 * NOTE: StringCchLength: https://msdn.microsoft.com/en-us/library/windows/desktop/ms647539.aspx
	 *
	 * ERROR_NO_UNICODE_TRANSLATION == GetLastError() is assumed.
	 *
	 * size_t to int conversion here is safe,
	 * since AnteFmtCch tops at STDIOSHIM_STDC_OBJ_THLD_CBLEN / sizeof(wchar_t).
	 */
	if (0 == WideCharToMultiByte(
		CP_UTF8,
		WC_ERR_INVALID_CHARS,
		format,
		(int)AnteFmtCch,
		NULL,
		0,
		NULL,
		NULL
		)) goto jreturn_e_ilseq;
#endif

#if defined( STDIOSHIMFWPRINTF_PRINTF ) || defined( STDIOSHIMFWPRINTF_FPRINTF )
	va_start(ap, format);
	U8PostFmtCch = (const int)_vscprintf(format, ap);
	va_end(ap);
#else
	va_start(ap, format);
	PostFmtCch = (const int)_vscwprintf(format, ap);
	va_end(ap);
#ifndef NDEBUG
	nNumberOfCharsToWrite = PostFmtCch;
#endif
#endif

#if defined( STDIOSHIMFWPRINTF_PRINTF ) || defined( STDIOSHIMFWPRINTF_FPRINTF )
	if (0 > U8PostFmtCch) {  goto jreturn_e_overflow; }
	else if (0 == U8PostFmtCch) {  goto jreturn_e; }
	else if (STDIOSHIM_STDC_FPRINTF_CCHTHLD < U8PostFmtCch) {  goto jreturn_e_overflow; }
#else
	if (0 > PostFmtCch) goto jreturn_e_overflow;
	else if (0 == PostFmtCch) goto jreturn_e;
	else if (STDIOSHIM_STDC_FWPRINTF_CCHTHLD < PostFmtCch) goto jreturn_e_overflow;
#endif

#if defined( STDIOSHIMFWPRINTF_PRINTF ) || defined( STDIOSHIMFWPRINTF_FPRINTF )
	U8PostFmtMaxCch = U8PostFmtCch > 1 + U8PostFmtCch ? U8PostFmtCch : 1 + U8PostFmtCch;
	U8PostFmtCb = U8PostFmtMaxCch*sizeof(char);
	/*U8PostFmtBuffer = VirtualAlloc(NULL, U8PostFmtCb, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);*/
	U8PostFmtBuffer = _aligned_malloc(U8PostFmtCb, __alignof(char));
	if (NULL == U8PostFmtBuffer) { goto jreturn_e_nomem; }
	/*(void)SecureZeroMemory(U8PostFmtBuffer, U8PostFmtCb);*/
	/* U8PostFmtCb <= _aligned_msize(U8PostFmtBuffer, __alignof(char), 0) is implied. */
	(void)SecureZeroMemory(U8PostFmtBuffer, _aligned_msize(U8PostFmtBuffer, __alignof(char), 0));
	va_start(ap, format);
	U8PostFmtCch = _vsnprintf_s(U8PostFmtBuffer, U8PostFmtMaxCch, U8PostFmtCch, format, ap);
	va_end(ap);
	/*(void)VirtualProtect(U8PostFmtBuffer, U8PostFmtCb, PAGE_READONLY, &flProtect);*/
	U8PostFmtMaxCch = U8PostFmtCch > 1 + U8PostFmtCch ? U8PostFmtCch : 1 + U8PostFmtCch;
	U8PostFmtCb = U8PostFmtMaxCch*sizeof(char);
	PostFmtMaxCch = MultiByteToWideChar(CP_UTF8, MB_ERR_INVALID_CHARS, U8PostFmtBuffer, (int)U8PostFmtCb, NULL, 0);
	if (0 == PostFmtMaxCch)
	{
		/*(void)VirtualFree(U8PostFmtBuffer, 0, MEM_RELEASE);*/ _aligned_free(U8PostFmtBuffer); U8PostFmtBuffer = NULL;
		goto jreturn_e_ilseq;
	}
#endif

#if !defined( STDIOSHIMFWPRINTF_PRINTF ) && !defined( STDIOSHIMFWPRINTF_FPRINTF )
	PostFmtMaxCch = (const unsigned int)(PostFmtCch > 1 + PostFmtCch ? PostFmtCch : 1 + PostFmtCch);
#endif
	PostFmtCb = (const size_t)(PostFmtMaxCch*sizeof(wchar_t));
	/*PostFmtBuffer = VirtualAlloc(NULL, PostFmtCb, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);*/
	PostFmtBuffer = _aligned_malloc(PostFmtCb, __alignof(wchar_t));
	if (NULL == PostFmtBuffer)
	{
#if defined( STDIOSHIMFWPRINTF_PRINTF ) || defined( STDIOSHIMFWPRINTF_FPRINTF )
		/*(void)VirtualFree(U8PostFmtBuffer, 0, MEM_RELEASE);*/ _aligned_free(U8PostFmtBuffer);  U8PostFmtBuffer = NULL;
#endif
		goto jreturn_e_nomem;
	}
	/*(void)SecureZeroMemory(PostFmtBuffer, PostFmtCb);*/
	/* PostFmbCb <= _aligned_msize(PostFmtBuffer, __alignof(wchar_t), 0) is implied. */
	(void)SecureZeroMemory(PostFmtBuffer, _aligned_msize(PostFmtBuffer, __alignof(wchar_t), 0));

#if defined( STDIOSHIMFWPRINTF_PRINTF ) || defined( STDIOSHIMFWPRINTF_FPRINTF )
	(void)MultiByteToWideChar(CP_UTF8, MB_ERR_INVALID_CHARS, U8PostFmtBuffer, (int)U8PostFmtCb, PostFmtBuffer, PostFmtMaxCch);
	/*(void)VirtualFree(U8PostFmtBuffer, 0, MEM_RELEASE);*/ _aligned_free(U8PostFmtBuffer); U8PostFmtBuffer = NULL;
	if (L'\x0000' == PostFmtBuffer[PostFmtMaxCch - 1]) PostFmtCch = PostFmtMaxCch - 1;
	else PostFmtCch = PostFmtMaxCch;
	/*(void)VirtualProtect(PostFmtBuffer, PostFmtCb, PAGE_READONLY, &flProtect);*/
#ifndef NDEBUG
	nNumberOfCharsToWrite = PostFmtCch;
#endif
#else
	va_start(ap, format);
	PostFmtCch = _vsnwprintf_s(PostFmtBuffer, PostFmtMaxCch, PostFmtCch, format, ap);
	va_end(ap);
	/*(void)VirtualProtect(PostFmtBuffer, PostFmtCb, PAGE_READONLY, &flProtect);*/
#endif
	/* --- */
#ifndef NDEBUG
	nNumberOfCharsWritten = PostFmtCch;
#endif
	/* --- */

	if (0 > PostFmtCch) goto jreturn_e_overflow_w_free_PostFmtBuf;
	else if (0 == PostFmtCch) goto jreturn_e_w_free_PostFmtBuf;
#if defined( STDIOSHIMFWPRINTF_PRINTF ) || defined( STDIOSHIMFWPRINTF_FPRINTF )
	else if (STDIOSHIM_STDC_FPRINTF_CCHTHLD < PostFmtCch)
#else
	else if (STDIOSHIM_STDC_FWPRINTF_CCHTHLD < PostFmtCch)
#endif
	{
		goto jreturn_e_overflow_w_free_PostFmtBuf;
	}

#ifndef NDEBUG
	/* ERROR_NO_UNICODE_TRANSLATION == GetLastError() is assumed. */
	if (0 == WideCharToMultiByte(
		CP_UTF8,
		WC_ERR_INVALID_CHARS,
		(const wchar_t *)PostFmtBuffer,
		PostFmtCch,
		NULL,
		0,
		NULL,
		NULL
		)) goto jreturn_e_ilseq_w_free_PostFmtBuf;

	if (nNumberOfCharsToWrite == nNumberOfCharsWritten) goto jreturn_write;
	else
	{
		/*(void)VirtualFree(PostFmtBuffer, 0, MEM_RELEASE);*/ _aligned_free(PostFmtBuffer); PostFmtBuffer = NULL;
		RaiseException(EXCEPTION_BREAKPOINT, 0, 1, (ULONG_PTR*)&format);
		/*DebugBreak();*/
		/*goto jreturn_e_w_free_PostFmtBuf;*/
	}
#else /* #ifndef NDEBUG */
	goto jreturn_write;
#endif /* #ifndef NDEBUG */

jreturn_write:
#ifdef STDIOSHIM_U16_REPLACEMENT_CHARACTER
	/* ONLY UCS-2 is suppported. Blame MSFT. */
	/*(void)VirtualProtect(PostFmtBuffer, PostFmtCb, PAGE_READWRITE, &flProtect);*/
	idxPostFmtBuffer = 0;
	bSurrogate = 0;
	Utf16CodeUnit = PostFmtBuffer[idxPostFmtBuffer];
	while (
		/*1 + idxPostFmtBuffer <= PostFmtCch
		&& 1 + idxPostFmtBuffer > idxPostFmtBuffer*/
		1 + (size_t)idxPostFmtBuffer <= (size_t)PostFmtMaxCch
		&& L'\x0000' != Utf16CodeUnit
		)
	{
		if (Utf16CodeUnit >= STDIOSHIM_U16_SURROGATE_RANGE_FIRST && Utf16CodeUnit <= STDIOSHIM_U16_SURROGATE_RANGE_LAST)
		{
			if (bSurrogate == 0)
			{
				/*if (1 + idxPostFmtBuffer == PostFmtCch || 1 + idxPostFmtBuffer < idxPostFmtBuffer)*/
				if (1 + idxPostFmtBuffer == PostFmtCch)
				{
					goto jreturn_e_ilseq_w_free_PostFmtBuf;
				}
				Utf16CodeUnit = PostFmtBuffer[1 + idxPostFmtBuffer];
				if (!(Utf16CodeUnit >= STDIOSHIM_U16_SURROGATE_RANGE_FIRST && Utf16CodeUnit <= STDIOSHIM_U16_SURROGATE_RANGE_LAST))
				{
					goto jreturn_e_ilseq_w_free_PostFmtBuf;
				}

				PostFmtBuffer[idxPostFmtBuffer] = STDIOSHIM_U16_REPLACEMENT_CHARACTER;
				bSurrogate++;
			}

			/* ZERO WIDTH SPACE fails to work with Windows Console. Blame MSFT. */
			/* PostFmtBuffer[idxPostFmtBuffer] = STDIOSHIM_U16_ZERO_WIDTH_SPACE; */
			/* --- */
			/* PostFmtBuffer can be terminated without \x0000, or MoveMemory() would have already sufficed. */
			/*MoveMemory(
				&PostFmtBuffer[idxPostFmtBuffer],
				&PostFmtBuffer[1 + idxPostFmtBuffer],
				(PostFmtCch - idxPostFmtBuffer - 1)*OffsetOfWchar
				);*/
			for (subidxPostFmtBuffer = 1 + idxPostFmtBuffer; 1 + (size_t)subidxPostFmtBuffer < (size_t)PostFmtMaxCch; subidxPostFmtBuffer++)
			{
				PostFmtBuffer[subidxPostFmtBuffer] = PostFmtBuffer[1 + subidxPostFmtBuffer];
			}
			PostFmtBuffer[PostFmtCch - 1] = L'\x0000';
			/* --- */
			bSurrogate--;
			/* --- */
			PostFmtCch--;
		}
		else if (bSurrogate != 0)
		{
			goto jreturn_e_ilseq_w_free_PostFmtBuf;
		}
		Utf16CodeUnit = PostFmtBuffer[++idxPostFmtBuffer];
	}
	/*(void)VirtualProtect(PostFmtBuffer, PostFmtCb, PAGE_READONLY, &flProtect);*/
#endif /* #ifdef STDIOSHIM_U16_REPLACEMENT_CHARACTER */

	WriteConsoleW(hConsole, PostFmtBuffer, (DWORD)PostFmtCch, NULL, NULL);
	/*(void)VirtualFree(PostFmtBuffer, 0, MEM_RELEASE);*/ _aligned_free(PostFmtBuffer); PostFmtBuffer = NULL;
	return PostFmtCch;

jreturn_idle:
	return 0;

jreturn_e_inval:
	errno = EINVAL;
	return (-1);

jreturn_e_overflow:
	errno = EOVERFLOW;
	return (-1);

jreturn_e_overflow_w_free_PostFmtBuf:
	/*(void)VirtualFree(PostFmtBuffer, 0, MEM_RELEASE);*/ _aligned_free(PostFmtBuffer); PostFmtBuffer = NULL;
	errno = EOVERFLOW;
	return (-1);

jreturn_e_ilseq:
	errno = EILSEQ;
	return (-1);

jreturn_e_ilseq_w_free_PostFmtBuf:
	/*(void)VirtualFree(PostFmtBuffer, 0, MEM_RELEASE);*/ _aligned_free(PostFmtBuffer); PostFmtBuffer = NULL;
	errno = EILSEQ;
	return (-1);

jreturn_e_nomem:
	errno = ENOMEM;
	return (-1);

jreturn_e:
	return (-1);

jreturn_e_w_free_PostFmtBuf:
	/*(void)VirtualFree(PostFmtBuffer, 0, MEM_RELEASE);*/ _aligned_free(PostFmtBuffer); PostFmtBuffer = NULL;
	return (-1);

jreturn_file:
	va_start(ap, format);
#if defined( STDIOSHIMFWPRINTF_PRINTF )
	PostFmtCch = vprintf_s(format, ap);
#elif defined( STDIOSHIMFWPRINTF_FPRINTF )
	PostFmtCch = vfprintf_s(stream, format, ap);
#elif defined( STDIOSHIMFWPRINTF_WPRINTF )
	PostFmtCch = vwprintf_s(format, ap);
#else
	PostFmtCch = vfwprintf_s(stream, format, ap);
#endif
	va_end(ap);
	return PostFmtCch;
}

/*_
 * If ever a DllMain is to be written here, do note that
 * DLL_PROCESS_ATTACH/DLL_PROCESS_DETACH is for the process main thread, while
 * DLL_THREAD_ATTACH/DLL_THREAD_DETACH is for other threads.
 */
END_EXTERN_C
