namespace cryptoshim
{
	namespace cprng
	{
		namespace core
		{
			void acqsemaphore( \
				cryptoshim_globalstate_t *globalstate, \
				cryptoshim_arc4random_semaphore_t *semaphore, \
				cryptoshim_arc4random_rngstate_t const *volatile LIBLOCAL_RESTRICT *state, \
				DWORD volatile *LIBLOCAL_RESTRICT idx_state) noexcept;
			void relsemaphore( \
				cryptoshim_globalstate_t *globalstate,
				cryptoshim_arc4random_semaphore_t *semaphore, \
				DWORD *p_heap, DWORD key_state) noexcept;
			void acqmutex(SRWLOCK *lock, CONDITION_VARIABLE *cond_nonsaturation, unsigned char *mask) noexcept;
			void relmutex(SRWLOCK *lock, CONDITION_VARIABLE *cond_nonsaturation, unsigned char *mask) noexcept;
		} /* END: core */
	} /* END: cprng */
} /* END: cryptoshim */
