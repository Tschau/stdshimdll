/*_
 * CHAR_BIT is at least 8,
 * per Section 5.2.4.2.1 of
 * http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1570.pdf .
 *
 * "unsigned char" is defined
 * as UCHAR and BYTE,
 * per https://msdn.microsoft.com/en-us/library/cc230382.aspx
 * and https://msdn.microsoft.com/en-us/library/cc230305.aspx .
 */
#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */
int getentropy(void *buf, size_t buflen);
#ifdef __cplusplus
}
#endif /* __cplusplus */

/*_
 * arc4random*() family of functions WILL NO LONGER TERMINATE UPON SEEDING FAILURE!!!
 * Users are adviced to check errno against EIO as a means of detection.
 * However, only in extremely rare cases will this occur.
 *
 * Some folks have adviced me to write another function that
 * allows the user to specify a handler function in the event such occurs.
 * However, considering the presence of atexit() and at_quick_exit(),
 * why should arc4random*() abandon the simplicity of exit(), or quick_exit()
 * in favor of making us a third member of the *exit*() family of functions?
 *
 * Some other folks have then adviced me to just raise a signal.
 * If the signal is amongst the standard family of signals, then
 * any such signal will trigger an attempt to clean up arc4random*'s
 * resource table... If the signal is a custom one, then the library
 * writer would be playing God and it steps into terra incognita
 * of custom signal conflicts.
 *
 * Yes, signal / software-exception-on-Windows is a compromise.
 *
 * However, it has long been established that arc4random*() seeds from getentropy()
 * and getentropy() sets EIO. Therefore, that user is expected to check errno is
 * as reasonable as requiring condition variable sleeps to be placed inside a loop.
 * If user wanna handle the seeding failure, just let him/her handle it.
 * If he/she does not, then he/she belongs to ignorami, while the rest of
 * illuminati will move on with their lives having checked errno. After all,
 * the failure is supposed to be rare, very rare.
 *
 * Maybe Vectored Exception Handling will finally resolve this mess...
 */

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */
int arc4random_onefillcore(/* _Out_write_bytes_all_(sizeof(uint32_t)) */ uint32_t *rcv);
int arc4random_fillbufcore(void *buf, size_t nbytes);
int arc4random_uniformcore(uint32_t *rcv, uint32_t upper_bound);
#ifdef __cplusplus
}
#endif /* __cplusplus */

/* BEGIN: arc4random */
#ifdef __cplusplus
uint32_t arc4random(void)
{
	uint32_t ret;

	if ((-1) != arc4random_onefillcore(&ret))
	{
		return ret;
	}
	else
	{
		throw std::system_error(EIO, std::generic_category());
	}
}
#else /* __cplusplus */
uint32_t arc4random(void);
#endif /* __cplusplus */
/* END: arc4random */

/* BEGIN: arc4random_uniform */
#ifdef __cplusplus
uint32_t arc4random_uniform(uint32_t upper_bound)
{
	uint32_t ret;

	if ((-1) == arc4random_uniformcore(&ret, upper_bound))
	{
		/*_
		 * getentropy() FAILED!!!
		 * GAME OVER.
		 */
		throw std::system_error(EIO, std::generic_category());
	}
	else return ret;
}
#else /* __cplusplus */
uint32_t arc4random_uniform(uint32_t upper_bound);
#endif /* __cpluslpus */
/* END: arc4random_uniform */

/* BEGIN: arc4random_buf */
#ifdef __cplusplus
void arc4random_buf(void *buf, size_t nbytes)
{
	/*
	if (buf == NULL || 0 == nbytes) return;
	*/

	if ((-1) == arc4random_fillbufcore(buf, nbytes))
	{
		throw std::system_error(EIO, std::generic_category());
	}
	else return;
}
#else /* __cplusplus */
void arc4random_buf(void *buf, size_t nbytes);
#endif /* __cplusplus */
/* END: arc4random_buf */
