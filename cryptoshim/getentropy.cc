/*	$OpenBSD: getentropy_win.c,v 1.4 2015/09/11 11:52:55 deraadt Exp $	*/
/*
 * Copyright (c) 2014, Theo de Raadt <deraadt@openbsd.org> 
 * Copyright (c) 2014, Bob Beck <beck@obtuse.com>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Emulation of getentropy(2) as documented at:
 * http://man.openbsd.org/getentropy.2
 */

#ifdef  _MSC_VER
#pragma warning( disable : 4255 4577 4668 4820 )
#pragma setlocale( "C" )
#endif

#include <windows.h>
#include <synchapi.h>
#include <bcrypt.h>

#include <errno.h>
#include <stdint.h>

#ifdef __AVX__
#include <intrin.h>
#endif

#include "getentropy.h"
#include "cryptoshim.h"

/*_
 * Windows NT's cng.sys, or Kernel Mode Cryptographics Primitive Library, is documented at
 * http://csrc.nist.gov/groups/STM/cmvp/documents/140-1/140sp/140sp1328.pdf once for Windows 7,
 * and http://csrc.nist.gov/groups/STM/cmvp/documents/140-1/140sp/140sp1335.pdf once for Windows Server 2008 R2,
 * wherein implementation details of SystemPrng() can be found, with a prototype of
 * BOOL SystemPrng(unsigned char *pbRandomData, size_t cbRandomData);
 * which receivs also further mention in
 * http://csrc.nist.gov/groups/STM/cmvp/documents/140-1/140sp/140sp2605.pdf for NT 6.4,
 * http://csrc.nist.gov/groups/STM/cmvp/documents/140-1/140sp/140sp2356.pdf for NT 6.3,
 * and http://csrc.nist.gov/groups/STM/cmvp/documents/140-1/140sp/140sp1891.pdf for NT 6.2.
 *
 * Further, http://csrc.nist.gov/groups/STM/cmvp/documents/140-1/140sp/140sp2606.pdf documents ProcessPrng,
 * found in bcryptprimitivies.dll, apart from the bcrypt.dll entry point.
 *
 * However, Microsoft as of the writing of this section ONLY manages to document SystemPrng, per
 * https://msdn.microsoft.com/en-us/library/dd408060.aspx . MSFT documents, at the time of writing,
 * that SystemPrng() always returns TRUE.
 */

/*_
 * DllMain takes the responsibility for initializing the cryptoshim_globalstate variable.
 * As such, NULL == cryptoshim_globalstate is NOT tested.
 */
int cryptoshim::getentropy(void *buf, size_t buflen) noexcept
{ /* NULL == buf is NOT tested. */
		NTSTATUS ret;

	if (256 < buflen)
	{
		errno = EIO;
		return (-1);
	}

		/*
		ret = cryptoshim_globalstate->fn.x_bcrypt.pBCryptOpenAlgorithmProvider(&srv.x_bcrypt, BCRYPT_RNG_ALGORITHM, MS_PRIMITIVE_PROVIDER, 0);
		if (NULL == srv.x_bcrypt || EXIT_SUCCESS != ret)
		{
			errno = EIO;
			return (-1);
		}
		*/

		/*_
		 * BCryptGenRandom is based on AES counter mode per https://msdn.microsoft.com/en-us/library/windows/desktop/aa375534.aspx .
		 *
		 * MS_PRIMITIVE_PROVIDER is dictated per https://msdn.microsoft.com/en-us/library/windows/desktop/bb204776.aspx .
		 *
		 * CryptGenRandom is retired since it is based on SHA-1 per Section 7.1.3 of RFC4096.
		 */
		/*ret = BCryptGenRandom(srv.x_bcrypt, buf, (ULONG)buflen, 0);*/
		ret = BCryptGenRandom(cryptoshim_globalstate->h_alg.x_bcrypt.h_rng, (PUCHAR)buf, (ULONG)buflen, 0);
		if (EXIT_SUCCESS == ret) return (0); /* Life moves on as usual. */
		else
		{
			if (STATUS_INVALID_PARAMETER == ret)
			{
				errno = EFAULT;
				return (-1);
			}
			else
			{
				errno = EIO;
				return (-1);
			}
		}
}

extern "C" int getentropy(void *buf, size_t buflen) noexcept { return cryptoshim::getentropy(buf, buflen); }
