!include ../own.mak

all: cryptoshim.dll cleanexe.exe

cryptoshim.dll: init.obj dllmain.obj getentropy.obj \
	arc4random-synchronization.obj arc4random-crypto.obj arc4random-vanilla.obj arc4random-fillbuf.obj arc4random-uniform.obj
!ifndef NDEBUG
	$(LINK) $(LINKFLAGS) /incremental:no /dll /def:cryptoshim.def /out:cryptoshim.dll /pdb:cryptoshim.pdb \
		init.obj dllmain.obj getentropy.obj \
		arc4random-synchronization.obj arc4random-crypto.obj arc4random-vanilla.obj arc4random-fillbuf.obj arc4random-uniform.obj \
		bcrypt.lib
!else
	$(LINK) $(LINKFLAGS) /incremental:no /dll /def:cryptoshim.def /out:cryptoshim.dll \
		init.obj dllmain.obj getentropy.obj \
		arc4random-synchronization.obj arc4random-crypto.obj arc4random-vanilla.obj arc4random-fillbuf.obj arc4random-uniform.obj \
		bcrypt.lib
!endif

cleanexe.exe: cleanexe.c
	$(CC) $(CFLAGS) $(CEFLAGS) $(RTCFLAGS) cleanexe.c /link $(LINKFLAGS) /incremental:no user32.lib

init.obj: init.cc
	$(CXX) $(CXXFLAGS) $(CXXEFLAGS) $(CXXRTCFLAGS) /Fo:init.obj /c init.cc

dllmain.obj: dllmain.c
	$(CC) $(CFLAGS) $(CEFLAGS) $(RTCFLAGS) /c dllmain.c

getentropy.obj: getentropy.cc
	$(CXX) $(CXXFLAGS) $(CEFLAGS) $(RTCFLAGS) /c getentropy.cc

arc4random-synchronization.obj: arc4random-synchronization.cc
	$(CXX) $(CXXFLAGS) $(CEFLAGS) $(CXXRTCFLAGS) /Oi /c arc4random-synchronization.cc

arc4random-crypto.obj: arc4random-crypto.cc
	$(CXX) $(CXXFLAGS) $(CEFLAGS) $(OFLAGS) /Oi /c arc4random-crypto.cc

arc4random-vanilla.obj: arc4random-vanilla.cc
	$(CXX) $(CXXFLAGS) $(CXXEFLAGS) $(OFLAGS) /Oi /c arc4random-vanilla.cc

arc4random-fillbuf.obj: arc4random-fillbuf.cc
	$(CXX) $(CXXFLAGS) $(CXXEFLAGS) $(OFLAGS) /Oi /c arc4random-fillbuf.cc

arc4random-uniform.obj: arc4random-uniform.cc
	$(CXX) $(CXXFLAGS) $(CXXEFLAGS) $(CXXRTCFLAGS) /c arc4random-uniform.cc

clean:
	del /q *.obj *.exp *.lib *.dll *.pdb
