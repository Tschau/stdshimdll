/*_
 * windows.h,
 * synchapi.h, and
 * stdint.h are required.
 */

#define CRYPTOSHIM_ARC4RANDOM_CB_KEY 32
#define CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_KEY 8
#define CRYPTOSHIM_ARC4RANDOM_CB_IV 12
#define CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_IV 3
#define CRYPTOSHIM_ARC4RANDOM_CB_SEED (CRYPTOSHIM_ARC4RANDOM_CB_KEY + CRYPTOSHIM_ARC4RANDOM_CB_IV)
#define CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED (CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_KEY + CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_IV)
#define CRYPTOSHIM_ARC4RANDOM_CB_BLOCK 64
#define CRYPTOSHIM_ARC4RANDOM_CBLOCK_BUF 16
#define CRYPTOSHIM_ARC4RANDOM_CB_BUF (CRYPTOSHIM_ARC4RANDOM_CB_BLOCK*CRYPTOSHIM_ARC4RANDOM_CBLOCK_BUF)
#define CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_BUF (CRYPTOSHIM_ARC4RANDOM_CB_BUF / 4)
#define CRYPTOSHIM_ARC4RANDOM_CB_STREAM 1600000
#define CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_STREAM (CRYPTOSHIM_ARC4RANDOM_CB_STREAM / 4)

#define CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_SATURATION 1
#define CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_INPROGRESS 2
#define CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_COND_NONSATURATION 4

#define CRYPTOSHIM_ARC4RANDOM_MUTEX_MASK_SATURATION 1
#define CRYPTOSHIM_ARC4RANDOM_MUTEX_MASK_COND_NONSATURATION 2

#define CRYPTOSHIM_ARC4RANDOM_EXCEPTION_MASK_ANNULMENT 1

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
typedef union {
#ifdef __AVX__
	__m256i blk256[2];
	__m128i blk128[4];
	uint32_t blk32[16];
#else
	uint32_t blk32[16];
#endif /* __AVX__ */
} cryptoshim_arc4random_rngstate_cipherstate_t;

typedef union {
#ifdef __AVX__
	__m256i blk256[CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_BUF / 8];
	__m128i blk128[CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_BUF / 4];
	uint32_t blk32[CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_BUF];
	unsigned char blk8[CRYPTOSHIM_ARC4RANDOM_CB_BUF];
#else
	uint32_t blk32[CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_BUF];
	unsigned char blk8[CRYPTOSHIM_ARC4RANDOM_CB_BUF];
#endif /* __AVX__ */
} cryptoshim_arc4random_rngstate_buf_t;

typedef struct {
	/* possibly fork-immutable */
	/*
	struct {
#ifdef __AVX__
		local_chacha_state_vec_t cipherstate;
#else
		local_chacha_state_t cipherstate;
#endif
		cryptoshim_arc4random_buf_t buf;
	} st_preservable;
	*/
	/* fork-mutable */
	//struct {
	//	union {
	//		size_t cb; /* Byte Counter until Buffer's EOF */
	//		size_t cch; /* uint32_t Counter until Buffer's EOF */
	//	} eof;
	//	union {
	//		size_t cb; /* Byte Counter until RST/Reseeding */
	//		size_t cch; /* uint32_t Counter until RST/Reseeding */
	//	} rst;
	//	DWORD pid;
	//} st_mutable;
#ifdef __AVX__
	cryptoshim_arc4random_rngstate_buf_t buf;
	cryptoshim_arc4random_rngstate_cipherstate_t cipherstate;
#endif /* __AVX__ */

	/*_
	 * VALUE REPRESENTATION
	 * 11:31 --- RESEEDING BYTE COUNTER
	 * 00:10 --- EOF BYTE COUNTER --- 0x07ff
	 */
	unsigned long counter;
#ifndef __AVX__
	cryptoshim_arc4random_rngstate_buf_t buf;
	cryptoshim_arc4random_rngstate_cipherstate_t cipherstate;
#endif
	DWORD pid;
} cryptoshim_arc4random_rngstate_t; /* arc4random_state_t is per process. */
#ifdef __cplusplus
}
#endif /* __cplusplus */

/* CRYPTOSHIM_GLOBALSTATE_MASK_ARC4RANDOM_MUTEX is the default. */
#define CRYPTOSHIM_GLOBALSTATE_MASK_ARC4RANDOM_SEMAPHORE 1

#include "fn.h"

/*_
 * With MSFT's failure to provide SRWLOCK and CONDITION_VARIABLE
 * with robustness, the two synchronization primitives ex infra
 * are NOT fool-proof. However, some would argue the kernel-bound
 * mutex object is, yet such is not fork-safe. If you,
 * the user, managed to implement a threading library/scheduler,
 * it would be more prudent to re-write the synchronization primitives
 * so that they are based on the safe-and-robust implementations.
 */
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
typedef struct {
	SRWLOCK lock;
	CONDITION_VARIABLE cond_nonsaturation;
	unsigned char mask;
} cryptoshim_arc4random_mutex_t;

typedef struct {
	SRWLOCK lock_semaphore;
	CONDITION_VARIABLE cond_semaphore_nonsaturation;
	CONDITION_VARIABLE cond_semaphore_notinprogress;
	DWORD counter_semaphore;
	DWORD counter_semaphore_queue;
	DWORD counter_heap;
	unsigned char mask_semaphore;
} cryptoshim_arc4random_semaphore_t;

typedef struct {
	unsigned char *fence_cross;
	DWORD code;
	unsigned char mask;
} cryptoshim_arc4random_exception_t;

typedef struct {
	union {
		cryptoshim_arc4random_mutex_t mutex;
		cryptoshim_arc4random_semaphore_t semaphore;
	} controller;
	cryptoshim_arc4random_exception_t exception;
} cryptoshim_arc4random_globalcontroller_t;
#ifdef __cplusplus
}
#endif /* __cplusplus */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
typedef struct {
	void *base;
	size_t len; /* Length of the Allocation starting at "base" */
	HANDLE h_heap; /* GetProcessHeap(); */
	DWORD allocalign;
	DWORD corecount;
	uint8_t mask;
	struct {
		/*_
		 * CryptGenRandom-based getentropy is disabled due to advapi32-hashing's poor memory model.
		 * However, this is NOT a message against SHA1-based generators.
		 */
		struct {
			BCRYPT_ALG_HANDLE h_rng;
		} x_bcrypt;
		/*
		struct {
		HCRYPTPROV h_rsa_aes;
		} x_advapi32;
		*/
	} h_alg;
	struct {
		struct {
			HANDLE h_mmap;
			void *p_base;
			cryptoshim_arc4random_rngstate_t *p_ent;
			DWORD nstates; /* nstates >= 1 ALWAYS holds. */
			DWORD nbytes;
		} mem;
		union {
			struct {
				struct {
					void *p_base;
					DWORD *p_ent;
				} implicitheap;
			} semaphore;
		} aux;
	} arc4random;
} cryptoshim_globalstate_t;
#ifdef __cplusplus
}
#endif /* __cplusplus */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
	extern cryptoshim_globalstate_t *cryptoshim_globalstate;
	extern cryptoshim_arc4random_globalcontroller_t cryptoshim_arc4random_globalcontroller;
#ifdef __cplusplus
}
#endif /* __cplusplus */
