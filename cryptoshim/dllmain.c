#ifdef  _MSC_VER
#pragma warning( disable : 4255 4668 4820 )
#pragma setlocale( "C" )
#endif

#include <windows.h>

#include <stdint.h>

#ifdef __AVX__
#include <intrin.h>
#endif

#ifdef _MSC_VER
#if ( _MSC_VER < 1900 )
#define inline __inline
#endif /* ( _MSC_VER < 1900 ) */
#if __STDC_VERSION__ < 201112L
#define _Alignof __alignof
#endif /* __STDC_VERSION__ < 201112L */
#define restrict __restrict
#endif /* _MSC_VER */

#include "cryptoshim.h"

BOOL WINAPI DllMain(_In_ HINSTANCE hinstDLL, _In_ DWORD fdwReason, _In_ LPVOID lpvReserved) {
	UNREFERENCED_PARAMETER(hinstDLL);
	UNREFERENCED_PARAMETER(lpvReserved);
	if (DLL_PROCESS_ATTACH == fdwReason)
	{
		return NULL == cryptoshim_globalstate ? FALSE : TRUE;
	}
	/*
	else if (DLL_PROCESS_DETACH == fdwReason);
	else if (DLL_THREAD_ATTACH == fdwReason);
	else if (DLL_THREAD_DETACH == fdwReason);
	*/
	return TRUE;
}
