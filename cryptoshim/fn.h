#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
typedef BOOL(*cryptoshim_CryptReleaseContext_t)(HCRYPTPROV, DWORD);
typedef NTSTATUS(*cryptoshim_BCryptCloseAlgorithmProvider_t)(BCRYPT_ALG_HANDLE, ULONG);

typedef BOOL(*cryptoshim_CryptAcquireContextW_t)(HCRYPTPROV*, LPCWSTR, LPCWSTR, DWORD, DWORD);
typedef NTSTATUS(*cryptoshim_BCryptOpenAlgorithmProvider_t)(BCRYPT_ALG_HANDLE*, LPCWSTR, LPCWSTR, DWORD);

typedef NTSTATUS(*cryptoshim_BCryptGetProperty_t)(BCRYPT_HANDLE, LPCWSTR, PUCHAR, ULONG, ULONG*, ULONG);

typedef BOOL(*cryptoshim_CryptDestroyHash_t)(HCRYPTHASH);
typedef NTSTATUS(*cryptoshim_BCryptDestroyHash_t)(BCRYPT_HASH_HANDLE);

typedef BOOL(*cryptoshim_CryptCreateHash_t)(HCRYPTPROV, ALG_ID, HCRYPTKEY, DWORD, HCRYPTHASH*);
typedef NTSTATUS(*cryptoshim_BCryptCreateHash_t)(BCRYPT_ALG_HANDLE, BCRYPT_HASH_HANDLE*, PUCHAR, ULONG, PUCHAR, ULONG, ULONG);

typedef BOOL(*cryptoshim_CryptGetHashParam_t)(HCRYPTHASH, DWORD, BYTE*, DWORD*, DWORD);

typedef NTSTATUS(*cryptoshim_BCryptFinishHash_t)(BCRYPT_HASH_HANDLE, PUCHAR, ULONG, ULONG);

typedef BOOL(*cryptoshim_CryptHashData_t)(HCRYPTHASH, BYTE*, DWORD, DWORD);
typedef NTSTATUS(*cryptoshim_BCryptHashData_t)(BCRYPT_HASH_HANDLE, PUCHAR, ULONG, ULONG);

typedef BOOL(*cryptoshim_CryptGenRandom_t)(HCRYPTPROV, DWORD, BYTE*);
typedef NTSTATUS(*cryptoshim_BCryptGenRandom_t)(BCRYPT_ALG_HANDLE, PUCHAR, ULONG, ULONG);

typedef BOOL(*cryptoshim_SystemPrng_t)(unsigned char*, size_t);
#ifdef __cplusplus
}
#endif /* __cplusplus */
