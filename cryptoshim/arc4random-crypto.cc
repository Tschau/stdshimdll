#ifdef  _MSC_VER
#pragma warning( disable : 4255 4530 4577 4668 4711 4820 )
#pragma setlocale( "C" )
#endif

#ifdef _MSC_VER
#if ( _MSC_VER < 1900 )
#define inline __inline
#endif /* _MSC_VER < 1900 */
#if __STDC_VERSION__ < 201112L
#define _Alignof __alignof
#define _Noreturn __declspec( noreturn )
#endif /* __STDC_VERSION__ < 201112L */
#define LIBLOCAL_RESTRICT __restrict
#endif /* _MSC_VER */

#include <windows.h>

#include <cstdint>
#ifdef _WIN64
using std::uint64_t;
#endif /* _WIN64 */
using std::uint32_t;

#ifdef __AVX__
#include <intrin.h>
#endif /* __AVX__ */

#ifdef __AVX__
#define LOCAL_CHACHA_QROUND_VEC128_CORE
#define LOCAL_CHACHA20_RFC7539_GETBLOCK_VEC128_CORE
#endif /* __AVX__ */
#include "local-chacha20-rfc7539.h"

#include "cryptoshim.h"
#include "getentropy.h"

#include "arc4random-crypto.h"

namespace cryptoshim
{
	namespace cprng
	{
		namespace core
		{
#ifdef __AVX__
			/* SSE */ /*inline void initcipher(cryptoshim_arc4random_rngstate_cipherstate_t *cipherstate, __m128i *buf) noexcept;*/
			/* AVX */ inline void initcipher(cryptoshim_arc4random_rngstate_cipherstate_t *cipherstate, __m256i *buf) noexcept;
#else /* __AVX__ */
			inline void initcipher(uint32_t *buf) noexcept;
#endif /* __AVX__ */

			void fillrngbuf(cryptoshim_arc4random_rngstate_buf_t *buf, cryptoshim_arc4random_rngstate_cipherstate_t *cipherstate) noexcept;
			void refillrngbuf(cryptoshim_arc4random_rngstate_buf_t *buf, cryptoshim_arc4random_rngstate_cipherstate_t *cipherstate, unsigned long *counter) noexcept;

#ifdef __AVX__
			/* SSE */ /*void saltrngbuf(cryptoshim_arc4random_rngstate_buf_t *p_rngbuf, __m128i *p_seedbuf) noexcept;*/
			/* AVX */ void saltrngbuf(cryptoshim_arc4random_rngstate_buf_t *p_rngbuf, __m256i *p_seedbuf) noexcept;
#else /* __AVX__ */
			/* AVX */ void saltrngbuf(cryptoshim_arc4random_rngstate_buf_t *p_rngbuf, uint32_t *p_seedbuf) noexcept;
#endif /* __AVX__ */
		} /* END: core */
	} /* END: cprng */
} /* END: cryptoshim */

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4559 )
#endif /* _MSC_VER */
/* buf is expected to be large enough to hold cryptoshim_arc4random_seed_t . */
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
#ifdef __AVX__
inline void cryptoshim::cprng::core::initcipher(
	cryptoshim_arc4random_rngstate_cipherstate_t *const LIBLOCAL_RESTRICT cipherstate,
	/* SSE */ /* _In_reads_(3) */ /* __m128i *const LIBLOCAL_RESTRICT buf) noexcept*/
	/* AVX */ /* _In_reads_bytes_(48) */ __m256i *const LIBLOCAL_RESTRICT buf) noexcept
{
	/* SSE */ /*local_chacha_rfc7539_blk128_initstate_v(cipherstate->blk128, buf);*/
	/*_
	 * Expectation (Low -> High):
	 * Key, Low;
	 * Key, High;
	 * Block Counter + Nonce.
	 */

	/* AVX */ local_chacha_rfc7539_blk256_initstate_v(_mm256_load_si256(buf), _mm_load_si128(2 + (__m128i*)buf), cipherstate->blk256);
	/*_
	 * Expectation (Low (__m256i) -> High (__m128i)):
	 * Key, High + Block Counter + Nonce;
	 * Key, Low.
	 */
}
#else /* __AVX__ */
inline void cryptoshim::cprng::core::initcipher(
	cryptoshim_arc4random_rngstate_cipherstate_t *const LIBLOCAL_RESTRICT cipherstate,
	/* _In_reads_(CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED) */ uint32_t *const LIBLOCAL_RESTRICT buf) noexcept
{
	local_chacha_rfc7539_blk32_initstate(cipherstate->blk32, buf, buf + CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_KEY, 0);
}
#endif /* __AVX__ */

/* buf != NULL => sizeof(*buf) >= sizeof(cryptoshim_arc4random_seed_t) is assumed. */
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
void cryptoshim::cprng::core::fillrngbuf(
	cryptoshim_arc4random_rngstate_buf_t *const LIBLOCAL_RESTRICT buf,
	cryptoshim_arc4random_rngstate_cipherstate_t *const LIBLOCAL_RESTRICT cipherstate) noexcept
{
#ifdef __AVX__
	/* SSE */ /*__m128i block[4];*/
	/* AVX */ __m256i block[2];

	/* SSE */ /*for (i = 0; i < (CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_BUF / 4); i++)
	{
		_mm_store_si128(i + buf->blk128, _mm_setzero_si128());
	}*/
	/* AVX */
	{
		long i = 0;
		for (; i < (CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_BUF / 8); i++)
		{
			/*_mm256_store_si256(i + buf->blk256, _mm256_setzero_si256());*/
			_mm256_stream_si256(i + buf->blk256, _mm256_setzero_si256());
		}
	}

	/*_
	 * sizeof(state->buf.blk32) is designed to be a multiple of 64,
	 * which is CRYPTOSHIM_ARC4RANDOM_CB_BLOCK.
	 */
	{
		long i = 0;
		for (; i < CRYPTOSHIM_ARC4RANDOM_CBLOCK_BUF; i++)
		{
			/* SSE */ /*local_chacha20_rfc7539_blk128_getblock_v(block, cipherstate->blk128);*/
			/* AVX */ local_chacha20_rfc7539_blk256_getblock_v(block, cipherstate->blk256);
			cipherstate->blk32[12]++; /* Entry (4, 1) is a block counter. */
			/* SSE */ /*for (j = 0; j < 4; j++) { _mm_store_si128((j | (i << 2)) + buf->blk128, _mm_load_si128(j + block)); }*/
			/* AVX */
			{
				long j = 0;
				for (j = 0; j < 2; j++)
				{
					/*_mm256_store_si256((j | (i << 1)) + buf->blk256, _mm256_load_si256(j + block));*/
					_mm256_store_si256((j | (i << 1)) + buf->blk256, _mm256_load_si256(j + block));
				}
			}
		}
	}
#else /* __AVX__ */
	uint32_t block[16];

	{
		long i = 0;
		for (; i < CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_BUF; i++) buf->blk32[i] = static_cast<uint32_t>(0);
	}
	/*_
	 * sizeof(state->buf.blk32) is designed to be a multiple of 64,
	 * which is CRYPTOSHIM_ARC4RANDOM_CB_BLOCK.
	 */
	{
		long i = 0;
		for (; i < CRYPTOSHIM_ARC4RANDOM_CBLOCK_BUF; i++)
		{
			local_chacha20_rfc7539_blk32_getblock(block, cipherstate->blk32);
			cipherstate->blk32[12]++; /* Entry (4, 1) is block counter. */
			{
				long j = 0;
				for (; j < 16; j++)
				{
					buf->blk32[j | (i << 4)] = block[j];
				}
			}
		}
	}
#endif /* __AVX__ */
}

#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
void cryptoshim::cprng::core::refillrngbuf(
	cryptoshim_arc4random_rngstate_buf_t *const LIBLOCAL_RESTRICT buf,
	cryptoshim_arc4random_rngstate_cipherstate_t *const LIBLOCAL_RESTRICT cipherstate,
	unsigned long *const LIBLOCAL_RESTRICT counter) noexcept
{
#ifdef __AVX__
	static_assert(CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED == 11, "CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED != 11");
#ifdef _WIN64
	/* SSE */ /*alignas(alignof(__m128i)) uint32_t payload[1 + CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED];*/
	/* AVX */ alignas(alignof(__m256i)) uint32_t payload[1 + CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED];

	/* SSE */ /*for (i = 0; i < 2; i++)
	{
		_mm_store_si128(i + payload, _mm_load_si128(i + buf->blk128));
		_mm_stream_si128(i + buf->blk128, _mm_setzero_si128());
	}*/
	/* _WIN64 */ /**(5 + (uint64_t*)payload) = *((uint64_t*)(8 + buf->blk32));
	*((uint64_t*)(8 + buf->blk32)) = 0;
	*(9 + (uint32_t*)payload) = *(10 + buf->blk32); *(10 + buf->blk32) = static_cast<uint32_t>(0);*/

	_mm256_store_si256((__m256i*)payload, _mm256_load_si256(buf->blk256));
	_mm256_stream_si256(buf->blk256, _mm256_setzero_si256());

	/* _WIN64 */ *(5 + (uint64_t*)payload) = *(4 + (uint64_t*)&buf); *(4 + (uint64_t*)&buf) = static_cast<uint64_t>(0);
	*(9 + payload) = *(10 + buf->blk32); *(10 + buf->blk32) = static_cast<uint32_t>(0);
	/*_
	 * X X X X
	 * X X X X
	 * ? X X X
	 */

	/* SSE */ /*payload[8] = static_cast<uint32_t>(0);*/
	/* AVX */ payload[8] = payload[4]; payload[4] = static_cast<uint32_t>(0);

	/* SSE */ /*cryptoshim::cprng::core::initcipher(cipherstate, buf->blk128);*/
	/* AVX */ cryptoshim::cprng::core::initcipher(cipherstate, (__m256i*)payload);

	/* SSE */ /*for (i = 0; i < 2; i++) _mm_store_si128((__m128i*)payload, _mm_setzero_si128());*/
	/* AVX */ _mm256_stream_si256((__m256i*)payload, _mm256_setzero_si256());
	_mm_store_si128(2 + (__m128i*)payload, _mm_setzero_si128());

	/* SSE */ /*for (i = 0; i < (CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_BUF / 4); i++)
	{
		_mm_store_si128(i + buf->blk128, _mm_setzero_si128());
	}*/
	/* AVX */ _mm256_stream_si256((__m256i*)(buf->blk256), _mm256_setzero_si256());

	_mm_stream_si64((__int64*)(8 + buf->blk32), static_cast<__int64>(0));
	_mm_stream_si32((int*)(10 + buf->blk32), static_cast<int>(0));
#else /* _WIN64 */
	/* SSE */ /*alignas(alignof(__m128i)) uint32_t payload[1 + CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED];*/
	/* AVX */ alignas(alignof(__m256i)) uint32_t payload[1 + CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED];

	/* SSE */ /*for (i = 0; i < 2; i++)
	{
		_mm_store_si128(i + payload, _mm_load_si128(i + buf->blk128));
		_mm_stream_si128(i + buf->blk128, _mm_setzero_si128());
	}*/
	/* _WIN32 */ /*for (i = 0; i < 3; i++)
	{
		*((i + 9) + (uint32_t*)payload) = *((i + 8) + buf->blk32);
		*((i + 8) + buf->blk32) = static_cast<uint32_t>(0);
	}*/

	_mm256_store_si256((__m256i*)payload, _mm256_load_si256(buf->blk256));
	_mm256_stream_si256(buf->blk256, _mm256_setzero_si256());

	/* _WIN32 */
	/*_
	 * X X X X
	 * X X X X
	 * ? X X X
	 */
	{
		long i = 0;
		for (; i < 3; i++)
		{
			*((i + 9) + payload) = *((i + 8) + buf->blk32);
			*((i + 8) + buf->blk32) = static_cast<uint32_t>(0);
		}
	}

	/* SSE */ /*payload[8] = static_cast<uint32_t>(0);*/
	/* AVX */ payload[8] = payload[4]; payload[4] = static_cast<uint32_t>(0);

	/* SSE */ /*cryptoshim::cprng::core::initcipher(cipherstate, buf->blk128);*/
	/* AVX */ cryptoshim::cprng::core::initcipher(cipherstate, (__m256i*)payload);

	/* SSE */ /*for (i = 0; i < 2; i++) _mm_store_si128((__m128i*)payload, _mm_setzero_si128());*/
	/* AVX */ _mm256_stream_si256((__m256i*)payload, _mm256_setzero_si256());
	_mm_store_si128(2 + (__m128i*)payload, _mm_setzero_si128());

	/* SSE */ /*for (i = 0; i < (CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_BUF / 4); i++)
	{
		_mm_store_si128(i + buf->blk128, _mm_setzero_si128());
	}*/
	/* AVX */ _mm256_stream_si256((__m256i*)(buf->blk256), _mm256_setzero_si256());

	{
		long i = 0;
		for (; i < 3; i++)
		{
			_mm_stream_si32((int*)((i + 8) + buf->blk32), static_cast<int>(0));
		}
	}
#endif /* _WIN64 */
#else /* __AVX__ */
	cryptoshim::cprng::core::initcipher(cipherstate, buf->blk32);

	{
		long i = 0;
		for (; i < CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED; i++) buf->blk32[i] = 0UL;
	}
#endif /* __AVX__ */

	*counter &= ~0x07ff;
	*counter ^= 0x07ffUL & sizeof(uint32_t)*(CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_BUF - CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED);
}

#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
void cryptoshim::cprng::core::saltrngbuf(
	cryptoshim_arc4random_rngstate_buf_t *const LIBLOCAL_RESTRICT p_rngbuf,
#ifdef __AVX__
	/* _In_reads_bytes_(sizeof(__m128i) * 2 + sizeof(uint32_t) * 3) */ /*__m128i *const LIBLOCAL_RESTRICT p_seedbuf) noexcept*/
	/* _In_reads_bytes_(sizeof(__m256i) + sizeof(uint32_t)*3) */ __m256i *const LIBLOCAL_RESTRICT p_seedbuf) noexcept
#else
	/* _In_reads_(CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED) */ uint32_t *const LIBLOCAL_RESTRICT p_seedbuf) noexcept
#endif /* __AVX__ */
{
	/*_
	 * X X X X
	 * X X X X
	 * X X X ?
	 */
#ifdef __AVX__
	static_assert(CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED == 11, "CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED != 11");
#ifdef _WIN64
	/* _mm256_xor_si256 is found in AVX2, NOT AVX. */
	{
		long i = 0;
		for (; i < 2; i++)
		{
			_mm_store_si128(i + p_rngbuf->blk128, _mm_xor_si128(
				_mm_load_si128(i + (__m128i*)p_seedbuf),
				_mm_load_si128(i + p_rngbuf->blk128) ));
		}
	}

	/* _WIN64 */ *(4 + (uint64_t*)p_rngbuf->blk32) ^= *(4 + (uint64_t*)p_seedbuf);
	p_rngbuf->blk32[10] ^= ((uint32_t*)p_seedbuf)[10];
#else /* _WIN64 */
	/* _mm256_xor_si256 is found in AVX2, NOT AVX. */
	{
		long i = 0;
		for (; i < 2; i++)
		{
			_mm_store_si128(i + p_rngbuf->blk128, _mm_xor_si128(
				_mm_load_si128(i + (__m128i*)p_seedbuf),
				_mm_load_si128(i + p_rngbuf->blk128) ));
		}
	}

	/* _WIN32 */
	{
		long i = 0;
		for (; i < 3; i++)
		{
			p_rngbuf->blk32[i + 8] ^= ((uint32_t*)p_seedbuf)[i + 8];
		}
	}
#endif /* _WIN64 */
#else /* __AVX__ */
	{
		long i = 0;
		for (; i < CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_KEY; i++)
		{
			p_rngbuf->blk32[i] ^= p_seedbuf[i];
		}
		for (; i < CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED; i++)
		{
			p_rngbuf->blk32[i] ^= p_seedbuf[i];
		}
	}
#endif /* __AVX__ */
}
#ifdef _MSC_VER
#pragma warning( pop )
#endif /* _MSC_VER */

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4133 4559 )
#endif /* _MSC_VER */
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
void cryptoshim::cprng::core::rekey(
	/* _Inout_ */ cryptoshim_arc4random_rngstate_cipherstate_t *LIBLOCAL_RESTRICT p_rngcipherstate,
	/* _Inout_ */ cryptoshim_arc4random_rngstate_buf_t *LIBLOCAL_RESTRICT p_rngbuf,
	/* _Inout_ */ unsigned long *LIBLOCAL_RESTRICT p_rngctr,
#ifdef __AVX__
	/* _In_reads_bytes_(sizeof(__m128i) * 2 + sizeof(uint32_t) * 3) */ /*__m128i *const LIBLOCAL_RESTRICT seedbuf) noexcept*/
	/* _In_reads_bytes_(sizeof(__m256i) + sizeof(uint32_t) * 3) */ __m256i *const LIBLOCAL_RESTRICT seedbuf) noexcept
#else
	/* _In_reads_(CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED) */ uint32_t *const LIBLOCAL_RESTRICT seedbuf) noexcept
#endif /* __AVX__ */
{	
	cryptoshim::cprng::core::fillrngbuf(p_rngbuf, p_rngcipherstate);
	/* state->buf is filled with keystream at this point. */
	cryptoshim::cprng::core::saltrngbuf(p_rngbuf, seedbuf); /* Charge is user's to zero the last 32 bits of buf[2]. */
	cryptoshim::cprng::core::refillrngbuf(p_rngbuf, p_rngcipherstate, p_rngctr);
}
#ifdef _MSC_VER
#pragma warning( pop )
#endif /* _MSC_VER */

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4133 4559 )
#endif /* _MSC_VER */
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
void cryptoshim::cprng::core::rekey(
	/* _Inout_ */ cryptoshim_arc4random_rngstate_cipherstate_t *LIBLOCAL_RESTRICT p_rngcipherstate,
	/* _Inout_ */ cryptoshim_arc4random_rngstate_buf_t *LIBLOCAL_RESTRICT p_rngbuf,
	/* _Inout_ */ unsigned long *LIBLOCAL_RESTRICT p_rngctr) noexcept
{
	cryptoshim::cprng::core::fillrngbuf(p_rngbuf, p_rngcipherstate);
	/* state->buf is filled with keystream at this point. */
	cryptoshim::cprng::core::refillrngbuf(p_rngbuf, p_rngcipherstate, p_rngctr);
}
#ifdef _MSC_VER
#pragma warning( pop )
#endif /* _MSC_VER */

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4559 )
#endif /* _MSC_VER */
int cryptoshim::cprng::core::reseed(cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state) noexcept
{
#ifdef __AVX__
	static_assert(CHAR_BIT == 8, "CHAR_BIT != 8");
	static_assert(CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED == 11, "CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED != 11");

	/* SSE */ /*__declspec( align( 16 ) ) uint32_t seedbuf[1 + CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED];*/
	/* AVX */ __declspec(align(32)) uint32_t seedbuf[1 + CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED];
	int ret;

	ret = cryptoshim::getentropy(&seedbuf, sizeof(uint32_t)*CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED);
	/*_
	 * X X X X
	 * X X X X
	 * X X X ?
	 */
	if (ret) return (-1);

	if (state->pid)
	{
		/* SSE */ /*cryptoshim::cprng::core::rekey(&state->cipherstate, &state->buf, &state->counter, (__m128i*)seedbuf);*/
		/* AVX */ cryptoshim::cprng::core::rekey(&state->cipherstate, &state->buf, &state->counter, (__m256i*)seedbuf);
	}
	else
	{
		/* Charge is user's to set the block counter; */
		/* SSE */ /**(11 + seedbuf) = *(8 + seedbuf); *(8 + seedbuf) = static_cast<uint32_t>(0);*/
		/* AVX */ *(11 + seedbuf) = *(4 + seedbuf); *(4 + seedbuf) = static_cast<uint32_t>(0);

		/* SSE */ /*cryptoshim::cprng::core::initcipher(&state->cipherstate, (__m128i*)seedbuf);*/
		/* AVX */ cryptoshim::cprng::core::initcipher(&state->cipherstate, (__m256i*)seedbuf);
	}

	/* seedbuf unzeroed is DANGEROUS! */
	/* SSE */ /*for (i = 0; i < 3; i++) _mm_store_si128(i + (__m128i*)seedbuf, _mm_setzero_si128());*/
	/* AVX */ _mm256_stream_si256((__m256i*)seedbuf, _mm256_setzero_si256());
	_mm_stream_si128((__m128i*)seedbuf, _mm_setzero_si128());

	/* SSE */ /*{
		long i = 0;
		for (; i < 2; i++)
		{
		_mm_store_si128(i + state->buf.blk128, _mm_setzero_si128());
		}
	}*/
	/* AVX */ _mm256_stream_si256((__m256i*)(state->buf.blk256), _mm256_setzero_si256());

#ifdef _WIN64
	_mm_stream_si64((__int64*)(8 + state->buf.blk32), static_cast<__int64>(0));
	_mm_stream_si32((int*)(10 + state->buf.blk32), static_cast<int>(0));
#else /* _WIN64 */
	{
		long i = 0;
		for (; i < 3; i++)
		{
			_mm_stream_si32((int*)((i + 8) + state->buf.blk32), static_cast<int>(0));
		}
	}
#endif /* _WIN64 */
#else /* __AVX__ */
	long i;
	uint32_t seedbuf[CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED];
	int ret;

	ret = cryptoshim::getentropy(&seedbuf, sizeof(uint32_t)*CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED);
	/*_
	 * X X X X
	 * X X X X
	 * X X X ?
	 */
	if (ret) return (-1);

	if (state->pid) cryptoshim::cprng::core::rekey(&state->cipherstate, &state->buf, &state->counter, seedbuf);
	else cryptoshim::cprng::core::initcipher(&state->cipherstate, seedbuf);

	/* seedbuf unzeroed is DANGEROUS! */
	for (i = 0; i < CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED; i++) seedbuf[i] = static_cast<uint32_t>(0);
	for (i = 0; i < CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_BUF; i++) state->buf.blk32[i] = static_cast<uint32_t>(0);
#endif /* __AVX__ */

	/* state->buf is invalidated. */
	state->counter = 0L | (sizeof(uint32_t)*CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_STREAM << 11);

	return (0);
}
#ifdef _MSC_VER
#pragma warning( pop )
#endif /* _MSC_VER */
