#ifdef __cplusplus
namespace cryptoshim
{
	namespace cprng
	{
		namespace core
		{
#ifdef _WIN64
			typedef uint64_t blk64i_t;
#else /* _WIN64 */
			typedef struct { uint32_t lo; uint32_t hi; } blk64i_t;
#endif /* _WIN64 */
			constexpr unsigned short rngbuflen = sizeof(uint32_t)*CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_BUF;
			constexpr unsigned short seedbuflen = sizeof(uint32_t)*CRYPTOSHIM_ARC4RANDOM_BLK32_CCH_SEED;

			template<typename T>
			constexpr unsigned char seedreslen = seedbuflen % sizeof(T);
			template<typename T>
			constexpr uintptr_t offsetofstream = seedreslen<T> ? seedbuflen + (sizeof(T) - seedreslen<T>) : seedbuflen;
			template<typename T>
			constexpr unsigned char rngbufcch = (unsigned char)((rngbuflen - seedbuflen) / sizeof(T));

			/* BEGIN: ::clean() */
			template<typename T> void clean(unsigned long *counter, unsigned char nelements) noexcept;
			template<typename T> typename std::enable_if<!std::is_same<unsigned char, T>::value, void>::type clean(unsigned long *counter) noexcept;
			template<typename T> typename std::enable_if<std::is_same<unsigned char, T>::value, void>::type clean(unsigned long *counter) noexcept;
			template<unsigned long BYTECOUNT> void clean(unsigned long *counter) noexcept;
			/* END: ::clean() */

			/* BEGIN: ::sentinel() */
			template<typename T> int sentinel(unsigned long *counter, DWORD *pid, unsigned char nelements) noexcept;
			template<typename T> typename std::enable_if<!std::is_same<unsigned char, T>::value, int>::type sentinel(unsigned long *counter, DWORD *pid) noexcept;
			template<typename T> typename std::enable_if<std::is_same<unsigned char, T>::value, int>::type sentinel(unsigned long *counter, DWORD *pid) noexcept;
			template<unsigned long BYTECOUNT> int sentinel(unsigned long *counter, DWORD *pid) noexcept;
			/* END: ::sentinel() */

			/* BEGIN: ::getoffsetofstream() */
			template<typename T> typename std::enable_if<!std::is_same<unsigned char, T>::value, uintptr_t>::type getoffsetofstream(unsigned short counter) noexcept;
			template<typename T> typename std::enable_if<std::is_same<unsigned char, T>::value, uintptr_t>::type getoffsetofstream(unsigned short counter) noexcept;
			/* END: ::getoffsetofstream() */

			/* BEGIN: ::drain() */
			template<typename T> typename std::enable_if<std::is_same<uint32_t, T>::value, void>::type drain(T *buf, T *stream, unsigned char nelements, unsigned char rem) noexcept;
#ifdef __AVX__
			template<typename T> typename std::enable_if<std::is_same<__m128i, T>::value, void>::type drain(T *buf, T *stream, unsigned char nelements, unsigned char rem) noexcept;
			template<typename T> typename std::enable_if<std::is_same<__m256i, T>::value, void>::type drain(T *buf, T *stream, unsigned char nelements, unsigned char rem) noexcept;
#endif /* __AVX__ */
#ifdef __AVX__
			template<typename T> typename std::enable_if<!std::is_same<uint32_t, T>::value && !std::is_same<cryptoshim::cprng::core::blk64i_t, T>::value && !std::is_same<__m128i, T>::value, void>::type drain(T *buf, T *stream, unsigned char rem) noexcept;
#else /* __AVX__ */
			template<typename T> typename std::enable_if<!std::is_same<uint32_t, T>::value && !std::is_same<cryptoshim::cprng::core::blk64i_t, T>::value, void>::type drain(T *buf, T *stream, unsigned char rem) noexcept;
#endif /* __AVX__ */
			template<typename T> typename std::enable_if<std::is_same<unsigned char, T>::value, void>::type drain(T *buf, T *stream) noexcept;
			template<typename T> typename std::enable_if<std::is_same<uint32_t, T>::value, void>::type drain(T *buf, T *stream, unsigned char rem) noexcept;
#ifdef _WIN64
			template<typename T> typename std::enable_if<std::is_same<cryptoshim::cprng::core::blk64i_t, T>::value, void>::type drain(T *buf, uint64_t *stream, unsigned char rem) noexcept;
#else /* _WIN64 */
			template<typename T> typename std::enable_if<std::is_same<cryptoshim::cprng::core::blk64i_t, T>::value, void>::type drain(T *buf, uint32_t *stream, unsigned char rem) noexcept;
#endif /* _WIN64 */
#ifdef __AVX__
			template<typename T> typename std::enable_if<std::is_same<__m128i, T>::value, void>::type drain(T *buf, T *stream, unsigned char rem) noexcept;
#endif /* __AVX__ */
			template<typename T> typename std::enable_if<std::is_same<uint32_t, T>::value, void>::type drain(T *buf, T *stream) noexcept;
#ifdef __AVX__
			template<typename T> typename std::enable_if<std::is_same<__m128i, T>::value, void>::type drain(T *buf, T *stream) noexcept;
			template<typename T> typename std::enable_if<std::is_same<__m256i, T>::value, void>::type drain(T *buf, T *stream) noexcept;
#endif /* __AVX__ */
			/* END: ::drain() */

			/* BEGIN: ::onefill() */
			template<typename T> typename std::enable_if<!std::is_same<unsigned char, T>::value && !std::is_same<cryptoshim::cprng::core::blk64i_t, T>::value, int>::type onefill(cryptoshim_arc4random_rngstate_t *state, T *buf) noexcept;
			template<typename T> typename std::enable_if<std::is_same<unsigned char, T>::value, int>::type onefill(cryptoshim_arc4random_rngstate_t *state, T *buf) noexcept;
			template<typename T> typename std::enable_if<std::is_same<cryptoshim::cprng::core::blk64i_t, T>::value, int>::type onefill(cryptoshim_arc4random_rngstate_t *state, T *buf) noexcept;
			/* END: ::onefill() */

			template<typename T>
			int nfill(cryptoshim_arc4random_rngstate_t *state, T *buf, unsigned char nelements) noexcept;

			/* BEGIN: ::onefill() */
			/*_
			 * 0 --- SUCCESS
			 * -1 --- getentropy() failure w/ EIO
			 */
			int onefill(cryptoshim_arc4random_rngstate_t *state, unsigned char *buf) noexcept;
			int onefill(cryptoshim_arc4random_rngstate_t *state, uint16_t *buf) noexcept;
			int onefill(cryptoshim_arc4random_rngstate_t *state, uint32_t *buf) noexcept;
#ifdef _WIN64
			int onefill(cryptoshim_arc4random_rngstate_t *state, cryptoshim::cprng::core::blk64i_t *buf) noexcept;
#else /* _WIN64 */
#ifdef __AVX__
			int onefill(cryptoshim_arc4random_rngstate_t *state, cryptoshim::cprng::core::blk64i_t *buf) noexcept;
#endif /* __AVX__ */
#endif /* _WIN64 */
#ifdef __AVX__
			int onefill(cryptoshim_arc4random_rngstate_t *state, __m128i *buf) noexcept;
#endif /* __AVX__ */
			/* END: ::onefill() */

			/* BEGIN: ::nfill() */
			int nfill(cryptoshim_arc4random_rngstate_t *state, uint32_t *buf, unsigned char nelements) noexcept;
#ifdef __AVX__
			int nfill(cryptoshim_arc4random_rngstate_t *state, __m128i *buf, unsigned char nelements) noexcept;
			int nfill(cryptoshim_arc4random_rngstate_t *state, __m256i *buf, unsigned char nelements) noexcept;
#endif /* __AVX__ */
			/* END: ::nfill() */

			int fillbuf(cryptoshim_arc4random_rngstate_t *state, uint32_t *buf, size_t nbytes) noexcept;
		} /* END: core */
	} /* END: cprng */
} /* END: cryptoshim */

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4559 )
#endif /* _MSC_VER */
template<typename T>
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
int cryptoshim::cprng::core::sentinel(
	unsigned long *const LIBLOCAL_RESTRICT counter,
	DWORD *const LIBLOCAL_RESTRICT pid,
	const unsigned char nelements) noexcept
{
	if (*pid)
	{
		if (sizeof(T)*nelements < (*counter >> 11)) return (0);
		else return (-1);
	}
	else
	{
		/*_
		 * Zeroing the buffer per CPRNG state is not necessary.
		 * Since mmap/MapViewOfFile's copy-on-write capability
		 * already has it handled.
		 */
		*counter = 0L;
		*pid = GetCurrentProcessId();
		return (-1);
	}
}

template<typename T>
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
typename std::enable_if<!std::is_same<unsigned char, T>::value, int>::type
cryptoshim::cprng::core::sentinel(
	unsigned long *const LIBLOCAL_RESTRICT counter,
	DWORD *const LIBLOCAL_RESTRICT pid) noexcept
{
	if (*pid)
	{
		if (sizeof(T) < (*counter >> 11)) return (0);
		else return (-1);
	}
	else
	{
		/*_
		* Zeroing the buffer per CPRNG state is not necessary.
		* Since mmap/MapViewOfFile's copy-on-write capability
		* already has it handled.
		*/
		*counter = 0L;
		*pid = GetCurrentProcessId();
		return (-1);
	}
}

template<typename T>
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
typename std::enable_if<std::is_same<unsigned char, T>::value, int>::type
cryptoshim::cprng::core::sentinel(
	unsigned long *const LIBLOCAL_RESTRICT counter,
	DWORD *const LIBLOCAL_RESTRICT pid) noexcept
{
	if (*pid)
	{
		if (*counter >> 12) /* *counter >> 11 <= (1) */ return (0);
		else return (-1);
	}
	else
	{
		/*_
		* Zeroing the buffer per CPRNG state is not necessary.
		* Since mmap/MapViewOfFile's copy-on-write capability
		* already has it handled.
		*/
		*counter = 0L;
		*pid = GetCurrentProcessId();
		return (-1);
	}
}

template<unsigned long BYTECOUNT>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
int cryptoshim::cprng::core::sentinel(
	unsigned long *const LIBLOCAL_RESTRICT counter,
	DWORD *const LIBLOCAL_RESTRICT pid) noexcept
{
	if (*pid)
	{
		if ((0x01ffff & BYTECOUNT) >= (*counter >> 11))
		{
			return (-1);
		}
		else
		{
			return (0);
		}
	}
	else
	{
		/*_
		 * Zeroing the buffer per CPRNG state is not necessary.
		 * Since mmap/MapViewOfFile's copy-on-write capability
		 * already has it handled.
		 */
		*counter = static_cast<unsigned long>(0);
		*pid = GetCurrentProcessId();
		return (-1);
	}
}
#ifdef _MSC_VER
#pragma warning( pop )
#endif /* _MSC_VER */

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4559 )
#endif /* _MSC_VER */
template<typename T>
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
void cryptoshim::cprng::core::clean(
	unsigned long *const LIBLOCAL_RESTRICT counter,
	const unsigned char nelements) noexcept
{
	if ((*counter >> 11) <= sizeof(T)*nelements)
	{
		*counter &= 0x07ff;
		return;
	}
	else
	{
		*counter -= sizeof(T) << 11;
		return;
	}
}

template<typename T>
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
typename std::enable_if<!std::is_same<unsigned char, T>::value, void>::type
cryptoshim::cprng::core::clean(
	unsigned long *const LIBLOCAL_RESTRICT counter) noexcept
{
	if ((*counter >> 11) <= sizeof(T))
	{
		*counter &= 0x07ff;
		return;
	}
	else
	{
		*counter -= sizeof(T) << 11;
		return;
	}
}

template<typename T>
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
typename std::enable_if<std::is_same<unsigned char, T>::value, void>::type
cryptoshim::cprng::core::clean(
	unsigned long *const LIBLOCAL_RESTRICT counter) noexcept
{
	if (*counter >> 12) /* (*counter >> 11) <= (1) */
	{
		*counter &= 0x07ff;
		return;
	}
	else
	{
		*counter -= (1) << 11;
		return;
	}
}

template<unsigned long BYTECOUNT>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
void cryptoshim::cprng::core::clean(unsigned long *const LIBLOCAL_RESTRICT counter) noexcept
{
	if (*counter >> 11 <= BYTECOUNT) *counter &= 0x07ff;
	else *counter -= BYTECOUNT << 11;
}
#ifdef _MSC_VER
#pragma warning( pop )
#endif /* _MSC_VER */

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4559 )
#endif /* _MSC_VER */
template<typename T>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
typename std::enable_if<!std::is_same<unsigned char, T>::value, uintptr_t>::type
cryptoshim::cprng::core::getoffsetofstream(unsigned short counter) noexcept
{
	counter -= counter%sizeof(T);
	return static_cast<unsigned short>(cryptoshim::cprng::core::rngbuflen - counter);
}

template<typename T>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
typename std::enable_if<std::is_same<unsigned char, T>::value, uintptr_t>::type
cryptoshim::cprng::core::getoffsetofstream(unsigned short counter) noexcept
{
	return static_cast<unsigned short>(cryptoshim::cprng::core::rngbuflen - counter);
}
#ifdef _MSC_VER
#pragma warning( pop )
#endif /* _MSC_VER */

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4559 )
#endif /* _MSC_VER */
template<typename T>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
typename std::enable_if<std::is_same<uint32_t, T>::value, void>::type
cryptoshim::cprng::core::drain(
	T *const LIBLOCAL_RESTRICT buf,
	T *const LIBLOCAL_RESTRICT stream,
	const unsigned char nelements,
	const unsigned char rem) noexcept
{
#ifdef __AVX__
	if (0 < nelements)
	{
		_mm_stream_si32((int*)buf + (nelements - 1), ((int*)stream)[nelements - 1]);
		if (0 < rem)
		{
			_mm_stream_si32((int*)stream + (nelements - 1), *((int*)stream - 1));
			_mm_stream_si32((int*)stream - 1, static_cast<int>(0));
		}
		else
		{
			_mm_stream_si32((int*)stream + (nelements - 1), static_cast<int>(0));
		}
	}
	{
		unsigned char i = static_cast<unsigned char>(0);
		for (; 1 + i < nelements; i++)
		{
			_mm_stream_si32((int*)buf + i, ((int*)stream)[i]);
			_mm_stream_si32((int*)stream + i, static_cast<int>(0));
		}
	}
#else /* __AVX__ */
	if (0 < nelements)
	{
		buf[nelements - 1] = stream[nelements - 1];
		if (0 < rem)
		{
			stream[nelements - 1] = *(stream - 1);
			*(stream - 1) = 0UL;
		}
		else
		{
			buf[nelements - 1] = stream[nelements - 1];
			*(stream + (nelements - 1)) = 0UL;
		}
	}
	{
		unsigned char i = static_cast<unsigned char>(0);
		for (; 1 + i < nelements; i++)
		{
			buf[i] = stream[i];
			stream[i] = 0UL;
		}
	}
#endif /* __AVX__ */
}

#ifdef __AVX__
template<typename T>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
typename std::enable_if<std::is_same<__m128i, T>::value, void>::type
cryptoshim::cprng::core::drain(
	T *const LIBLOCAL_RESTRICT buf,
	T *const LIBLOCAL_RESTRICT stream,
	const unsigned char nelements,
	const unsigned char rem) noexcept
{
	if (0 < nelements)
	{
		_mm_stream_si128(buf + (nelements - 1), _mm_load_si128(stream + (nelements - 1)));
		if (0 < rem)
		{
			_mm_stream_si128(stream + (nelements - 1), _mm_load_si128(stream - 1));
			_mm_stream_si128(stream - 1, _mm_setzero_si128());
		}
		else
		{
			_mm_stream_si128((nelements - 1) + stream, _mm_setzero_si128());
		}
	}
	{
		unsigned char i;
		for (i = 0; 1 + i < nelements; i++)
		{
			_mm_stream_si128(buf + i, _mm_load_si128(stream + i));
			_mm_stream_si128(stream + i, _mm_setzero_si128());
		}
	}
}

template<typename T>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
typename std::enable_if<std::is_same<__m256i, T>::value, void>::type
cryptoshim::cprng::core::drain(
	T *const LIBLOCAL_RESTRICT buf,
	T *const LIBLOCAL_RESTRICT stream,
	const unsigned char nelements,
	const unsigned char rem) noexcept
{
	if (0 < nelements)
	{
		_mm256_stream_si256(buf + (nelements - 1), _mm256_load_si256(stream + (nelements - 1)));
		if (0 < rem)
		{
			_mm256_stream_si256(stream + (nelements - 1), _mm256_load_si256(stream - 1));
			_mm256_stream_si256(stream - 1, _mm256_setzero_si256());
		}
		else
		{
			_mm256_stream_si256((nelements - 1) + stream, _mm256_setzero_si256());
		}
	}
	{
		unsigned char i;
		for (i = 0; 1 + i < nelements; i++)
		{
			_mm256_stream_si256(buf + i, _mm256_load_si256(stream + i));
			_mm256_stream_si256(stream + i, _mm256_setzero_si256());
		}
	}
}
#endif /* __AVX__ */

template<typename T>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
#ifdef __AVX__
typename std::enable_if<!std::is_same<uint32_t, T>::value && !std::is_same<cryptoshim::cprng::core::blk64i_t, T>::value && !std::is_same<__m128i, T>::value, void>::type
#else /* __AVX__ */
typename std::enable_if<!std::is_same<uint32_t, T>::value && !std::is_same<cryptoshim::cprng::core::blk64i_t, T>::value, void>::type
#endif /* __AVX__ */
cryptoshim::cprng::core::drain(
	T *const LIBLOCAL_RESTRICT buf,
	T *const LIBLOCAL_RESTRICT stream,
	const unsigned char rem) noexcept
{
	static_assert(std::is_arithmetic<T>::value, "is_arithmetic<T> fails.");

	*buf = *stream;
	if (rem)
	{
		*stream = *(stream - 1);
		*(stream - 1) = static_cast<T>(0);
	}
	else
	{
		*stream = static_cast<T>(0);
	}
}

template<typename T>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
typename std::enable_if<std::is_same<unsigned char, T>::value, void>::type
cryptoshim::cprng::core::drain(
	T *const LIBLOCAL_RESTRICT buf,
	T *const LIBLOCAL_RESTRICT stream) noexcept
{
	*buf = *stream;
	*stream = static_cast<unsigned char>(0);
}

template<typename T>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
typename std::enable_if<std::is_same<uint32_t, T>::value, void>::type
cryptoshim::cprng::core::drain(
	T *const LIBLOCAL_RESTRICT buf,
	T *const LIBLOCAL_RESTRICT stream,
	const unsigned char rem) noexcept
{
#ifdef __AVX__
	_mm_stream_si32((int*)buf, static_cast<int>(*stream));
	if (rem)
	{
		_mm_stream_si32((int*)stream, static_cast<int>(stream[-1]));
		_mm_stream_si32((int*)stream - 1, static_cast<int>(0));
	}
	else
	{
		_mm_stream_si32((int*)stream, static_cast<int>(0));
	}
#else /* __AVX__ */
	*buf = *stream;
	if (rem)
	{
		*stream = stream[-1];
		*(stream - 1) = static_cast<uint32_t>(0);
	}
	else
	{
		*stream = static_cast<uint32_t>(0);
	}
#endif /* __AVX__ */
}

template<typename T>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
typename std::enable_if<std::is_same<cryptoshim::cprng::core::blk64i_t, T>::value, void>::type
cryptoshim::cprng::core::drain(
	T *const LIBLOCAL_RESTRICT buf,
#ifdef _WIN64
	uint64_t *const LIBLOCAL_RESTRICT stream,
#else /* _WIN64 */
	uint32_t *const LIBLOCAL_RESTRICT stream,
#endif /* _WIN64 */
	const unsigned char rem) noexcept
{
#ifdef _WIN64
#ifdef __AVX__
	_mm_stream_si64((__int64*)buf, *(__int64*)stream);
	if (rem)
	{
		_mm_stream_si64((__int64*)stream, *((__int64*)stream - 1));
		_mm_stream_si64((__int64*)stream - 1, static_cast<__int64>(0));
	}
	else
	{
		_mm_stream_si64((__int64*)stream, static_cast<__int64>(0));
	}
#else /* __AVX__ */
	*buf = *stream;
	if (rem)
	{
		*stream = *(stream - 1);
		*(stream - 1) = 0ULL;
	}
	else
	{
		*stream = 0ULL;
	}
#endif /* __AVX__ */
#else /* _WIN64 */
#ifdef __AVX__
	_mm_stream_si32((int*)buf + 1, *((int*)stream + 1));
	if (rem)
	{
		_mm_stream_si32((int*)stream + 1, *((int*)stream - 1));
		_mm_stream_si32((int*)stream - 1, static_cast<int>(0));
	}
	else
	{
		_mm_stream_si32((int*)stream + 1, static_cast<int>(0));
	}
	_mm_stream_si32((int*)buf, *(int*)stream);
	_mm_stream_si32((int*)stream, static_cast<int>(0));
#else /* __AVX__ */
	buf[1] = stream[1];
	if (rem)
	{
		stream[1] = stream[-1];
		stream[-1] = 0UL;
	}
	else
	{
		stream[1] = 0UL;
	}
	*buf = *stream;
	*stream = 0UL;
#endif /* __AVX__ */
#endif /* _WIN64 */
}

#ifdef __AVX__
template<typename T>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
typename std::enable_if<std::is_same<__m128i, T>::value, void>::type
cryptoshim::cprng::core::drain(
	T *const LIBLOCAL_RESTRICT buf,
	T *const LIBLOCAL_RESTRICT stream,
	const unsigned char rem) noexcept
{
	_mm_stream_si128(buf, _mm_load_si128(stream));
	if (rem)
	{
		_mm_stream_si128(stream, _mm_load_si128(stream - 1));
		_mm_stream_si128(stream - 1, _mm_setzero_si128());
	}
	else
	{
		_mm_stream_si128(stream, _mm_setzero_si128());
	}
}
#endif /* __AVX__ */

template<typename T>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
typename std::enable_if<std::is_same<uint32_t, T>::value, void>::type
cryptoshim::cprng::core::drain(
	T *const LIBLOCAL_RESTRICT buf,
	T *const LIBLOCAL_RESTRICT stream) noexcept
{
	unsigned char i = 0;
	for (; i < cryptoshim::cprng::core::rngbufcch<T>; i++)
	{
#ifdef __AVX__
		_mm_stream_si32(i + (int*)buf, ((int*)stream)[i]);
		_mm_stream_si32(i + (int*)stream, static_cast<int>(0));
#else /* __AVX__ */
		buf[i] = stream[i];
		stream[i] = static_cast<int>(0);
#endif /* __AVX__ */
	}
}

#ifdef __AVX__
template<typename T>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
typename std::enable_if<std::is_same<__m128i, T>::value, void>::type
cryptoshim::cprng::core::drain(
	T *const LIBLOCAL_RESTRICT buf,
	T *const LIBLOCAL_RESTRICT stream) noexcept
{
	unsigned char i = 0;
	for (; i < cryptoshim::cprng::core::rngbufcch<T>; i++)
	{
		_mm_stream_si128(i + buf, _mm_load_si128(i + stream));
		_mm_stream_si128(i + stream, _mm_setzero_si128());
	}
}

template<typename T>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
typename std::enable_if<std::is_same<__m256i, T>::value, void>::type
cryptoshim::cprng::core::drain(
	T *const LIBLOCAL_RESTRICT buf,
	T *const LIBLOCAL_RESTRICT stream) noexcept
{
	unsigned char i = 0;
	for (; i < cryptoshim::cprng::core::rngbufcch<T>; i++)
	{
		_mm256_stream_si256(i + buf, _mm256_load_si256(i + stream));
		_mm256_stream_si256(i + stream, _mm256_setzero_si256());
	}
}
#endif /* __AVX__ */
#ifdef _MSC_VER
#pragma warning( pop )
#endif /* _MSC_VER */

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4559 )
#endif /* _MSC_VER */
template<typename T>
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
typename std::enable_if<!std::is_same<unsigned char, T>::value && !std::is_same<cryptoshim::cprng::core::blk64i_t, T>::value, int>::type
cryptoshim::cprng::core::onefill(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	T *const LIBLOCAL_RESTRICT buf) noexcept
{
	unsigned long *LIBLOCAL_RESTRICT p_counter = &state->counter;

	if (cryptoshim::cprng::core::sentinel<T>(p_counter, &state->pid))
	{
		if ((-1) == cryptoshim::cprng::core::reseed(state))
		{
			/* getentropy() just set EIO. */
			return (-1);
		}
	}
	if (sizeof(T) > (0x07ff & *p_counter))
	{
		cryptoshim::cprng::core::rekey(&state->cipherstate, &state->buf, p_counter);
	}

	cryptoshim::cprng::core::drain<T>(
		buf,
		(T*)(cryptoshim::cprng::core::getoffsetofstream<T>(static_cast<unsigned long>(0x07ff) & *p_counter) + (uintptr_t)(void*)&state->buf),
		(0x07ff & *p_counter) % sizeof(T) );

	*p_counter -= sizeof(T);
	cryptoshim::cprng::core::clean<T>(p_counter);
	return (0);
}

template<typename T>
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
typename std::enable_if<std::is_same<unsigned char, T>::value, int>::type
cryptoshim::cprng::core::onefill(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	T *const LIBLOCAL_RESTRICT buf) noexcept
{
	unsigned long *LIBLOCAL_RESTRICT p_counter = &state->counter;

	if (cryptoshim::cprng::core::sentinel<T>(p_counter, &state->pid))
	{
		if ((-1) == cryptoshim::cprng::core::reseed(state))
		{
			/* getentropy() just set EIO. */
			return (-1);
		}
	}
	if (sizeof(T) > (0x07ff & *p_counter))
	{
		cryptoshim::cprng::core::rekey(&state->cipherstate, &state->buf, p_counter);
	}

	cryptoshim::cprng::core::drain<T>(
		buf,
		(T*)(cryptoshim::cprng::core::getoffsetofstream<T>(static_cast<unsigned long>(0x07ff) & *p_counter) + (uintptr_t)(void*)&state->buf));

	*p_counter -= sizeof(T);
	cryptoshim::cprng::core::clean<T>(p_counter);
	return (0);
}

template<typename T>
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
typename std::enable_if<std::is_same<cryptoshim::cprng::core::blk64i_t, T>::value, int>::type
cryptoshim::cprng::core::onefill(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	T *const LIBLOCAL_RESTRICT buf) noexcept
{
	unsigned long *LIBLOCAL_RESTRICT p_counter = &state->counter;

	if (cryptoshim::cprng::core::sentinel<T>(p_counter, &state->pid))
	{
		if ((-1) == cryptoshim::cprng::core::reseed(state))
		{
			/* getentropy() just set EIO. */
			return (-1);
		}
	}
	if (sizeof(T) > (0x07ff & *p_counter))
	{
		cryptoshim::cprng::core::rekey(&state->cipherstate, &state->buf, p_counter);
	}

#ifdef _WIN64
	cryptoshim::cprng::core::drain<cryptoshim::cprng::core::blk64i_t>(
		buf,
		(uint64_t*)(cryptoshim::cprng::core::getoffsetofstream<uint64_t>(static_cast<unsigned long>(0x07ff) & *p_counter) + (uintptr_t)(void*)&state->buf),
		(0x07ff & *p_counter) % sizeof(uint64_t) );
#else /* _WIN64 */
	cryptoshim::cprng::core::drain<cryptoshim::cprng::core::blk64i_t>(
		buf,
		(uint32_t*)(cryptoshim::cprng::core::getoffsetofstream<uint32_t>(static_cast<unsigned long>(0x07ff) & *p_counter) + (uintptr_t)(void*)&state->buf),
		(0x07ff & *p_counter) % sizeof(uint32_t) );
#endif /* _WIN64 */

	*p_counter -= sizeof(T);
	cryptoshim::cprng::core::clean<T>(p_counter);
	return (0);
}

template<typename T>
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
int cryptoshim::cprng::core::nfill(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	T *const LIBLOCAL_RESTRICT buf,
	unsigned char nelements) noexcept
{
	unsigned long *LIBLOCAL_RESTRICT p_counter = &state->counter;

	nelements %= 1 + (rngbuflen - seedbuflen) / sizeof(T);
	if (cryptoshim::cprng::core::sentinel<T>(p_counter, &state->pid, nelements))
	{
		if ((-1) == cryptoshim::cprng::core::reseed(state))
		{
			/* getentropy() just set EIO. */
			return (-1);
		}
	}
	if (sizeof(T)*nelements > (0x07ff & *p_counter))
	{
		cryptoshim::cprng::core::rekey(&state->cipherstate, &state->buf, p_counter);
	}

	cryptoshim::cprng::core::drain<T>(
		buf,
		(T*)(cryptoshim::cprng::core::getoffsetofstream<T>(static_cast<unsigned long>(0x07ff) & *p_counter) + (uintptr_t)(void*)&state->buf),
		nelements,
		(0x07ff & *p_counter) % sizeof(T) );

	*p_counter -= sizeof(T)*nelements;
	cryptoshim::cprng::core::clean<T>(p_counter, nelements);
	return (0);
}
#ifdef _MSC_VER
#pragma warning( pop )
#endif /* _MSC_VER */
#endif /* __cplusplus */
