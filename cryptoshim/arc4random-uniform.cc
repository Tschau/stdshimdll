/*	$OpenBSD: arc4random_uniform.c,v 1.2 2015/09/13 08:31:47 guenther Exp $	*/
/*
 * Copyright (c) 2008, Damien Miller <djm@openbsd.org>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/*	$NetBSD: arc4random.c,v 1.31 2016/03/25 22:13:23 riastradh Exp $	*/
/*-
 * Copyright (c) 2014 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * This code is derived from software contributed to The NetBSD Foundation
 * by Taylor R. Campbell.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifdef  _MSC_VER
#pragma warning( disable : 4255 4668 4710 4820 )
#pragma setlocale( "C" )
#endif

#ifdef _MSC_VER
#if ( _MSC_VER < 1900 )
#define inline __inline
#endif /* _MSC_VER < 1900 */
#if __STDC_VERSION__ < 201112L
#define _Alignof __alignof
#define _Noreturn __declspec( noreturn )
#endif /* __STDC_VERSION__ < 201112L */
#define LIBLOCAL_RESTRICT __restrict
#endif /* _MSC_VER */

#include <windows.h>

#include <type_traits>
#include <system_error>

#include <cstdint>

#ifdef __AVX__
#include <intrin.h>
#endif /* __AVX__ */

#include "cryptoshim.h"

#include "arc4random.h"
#include "arc4random-signal.h"
#include "arc4random-synchronization.h"

namespace cryptoshim
{
	namespace cprng
	{
		namespace core
		{
			template<typename T> int uniform_integer_distribution(cryptoshim_arc4random_rngstate_t *rngstate, T *ret, T upper_bound) noexcept;
		} /* END: core */
	} /* END: cprng */
} /* END: cryptoshim */

extern "C" int arc4random_uniformcore(uint32_t *rcv, uint32_t upper_bound) noexcept;

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4559 )
#endif /* _MSC_VER */
/*
 * 0 --- SUCCESS
 * -1 --- getentropy() failure w/ EIO
 */
template<typename T>
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
int cryptoshim::cprng::core::uniform_integer_distribution(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT rngstate,
	T *const LIBLOCAL_RESTRICT ret,
	const T upper_bound) noexcept
{
	static_assert(std::is_integral<T>::value, "std::is_integral<T> fails.");
	
	/*_
	 * Notes within this block apply only to uint32_t.
	 * T is assumed to span as wide as 8 bits in length,
	 * with a maximum of 2**N - 1,
	 * for some positive integer N where N >= 8.
	 */

	T x = 0, r = 0;

	if (upper_bound < static_cast<T>(2))
	{
		*ret = static_cast<T>(0);
		return (0);
	}

	/* 2**32 % x == (2**32 - x) % x */
	r = (static_cast<T>(0) - upper_bound) % upper_bound;
	/*_
	 * r == 2**32 % upper_bound at this point.
	 *
	 * Also, note that x + 2**32 % x <= x*q + 2**32 % x == 2**32,
	 * with q >= 1 since 2**32 % x <= 2**32 - 1.
	 *
	 * Hence, x >= 2**31 + 2 ->> 2**32 % x <= 2**31 - 2,
	 * 2**32 % (2**31 + 1) = 2**31 - 1,
	 * x < 2**31 ->> 2**32 % x <= 2**31 - 2,
	 * and 2**32 % 2**31 = 0,
	 * which implies r <= 2**31 - 2 for x != 2**31 + 1.
	 */

	/*_
	 * It is a possibility that this will loop forever. However,
	 * each loop has P >= 0.5 (equality holds <<->> upper_bound == 2**31 + 1)
	 * of selecting a number inside the range we need. Re-roll is expected to be rare.
	 */
	do
	{
		if ((-1) == cryptoshim::cprng::core::onefill(rngstate, &x)) return (-1);
	} while (x < r);

	*ret = x % upper_bound;
	return (0);
}
#ifdef _MSC_VER
#pragma warning( pop )
#endif /* _MSC_VER */

/*
 * 0 --- SUCCESS
 * -1 --- getentropy() failure w/ EIO
 */
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
extern "C" int arc4random_uniformcore(uint32_t *const LIBLOCAL_RESTRICT rcv, const uint32_t upper_bound)
{
	int ret;
	{
		cryptoshim_globalstate_t *const LIBLOCAL_RESTRICT globalstate = cryptoshim_globalstate;
		cryptoshim_arc4random_semaphore_t *const LIBLOCAL_RESTRICT semaphore = &cryptoshim_arc4random_globalcontroller.controller.semaphore;
		DWORD idx_state;

		{
			cryptoshim_arc4random_rngstate_t *LIBLOCAL_RESTRICT rngstate;

			cryptoshim::cprng::core::acqsemaphore(globalstate, semaphore, (cryptoshim_arc4random_rngstate_t const *LIBLOCAL_RESTRICT *)&rngstate, &idx_state);
			ret = cryptoshim::cprng::core::uniform_integer_distribution<uint32_t>(rngstate, rcv, upper_bound);
		}
		cryptoshim::cprng::core::relsemaphore(globalstate, semaphore, globalstate->arc4random.aux.semaphore.implicitheap.p_ent, idx_state);
	}

	return ret;
}

/*_
 * Calculate a uniformly distributed random number strictly less than
 * upper_bound avoiding "modulo bias".
 *
 * Uniformity is achieved by generating new random numbers until
 * the one returned is outside the range [0, 2**32 % upper_bound).
 *
 * It is guaranteed that the selected random number will be within
 * [2**32 % upper_bound, 2**32) which maps back to [0, upper_bound)
 * after modular reduction upper_bound.
 */
extern "C" uint32_t arc4random_uniform(const uint32_t upper_bound)
{
	uint32_t ret;

	if ((-1) == arc4random_uniformcore(&ret, upper_bound))
	{
		/*_
		 * getentropy() FAILED!!!
		 * GAME OVER.
		 */
		// (void)TerminateProcess(GetCurrentProcess(), 0xC0000001L);
		RaiseFailFastException(NULL, NULL, 0);
		return ret;
	}
	else return ret;
}
