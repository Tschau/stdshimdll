/*	$NetBSD: cprng_fast.c,v 1.13 2015/04/13 22:43:41 riastradh Exp $	*/
/*-
 * Copyright (c) 2014 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * This code is derived from software contributed to The NetBSD Foundation
 * by Taylor R. Campbell.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*	$OpenBSD: rnd.c,v 1.182 2016/07/15 19:02:30 tom Exp $	*/
/*
 * Copyright (c) 2011 Theo de Raadt.
 * Copyright (c) 2008 Damien Miller.
 * Copyright (c) 1996, 1997, 2000-2002 Michael Shalayeff.
 * Copyright (c) 2013 Markus Friedl.
 * Copyright Theodore Ts'o, 1994, 1995, 1996, 1997, 1998, 1999.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, and the entire permission notice in its entirety,
 *    including the disclaimer of warranties.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * ALTERNATIVELY, this product may be distributed under the terms of
 * the GNU Public License, in which case the provisions of the GPL are
 * required INSTEAD OF the above restrictions.  (This clause is
 * necessary due to a potential bad interaction between the GPL and
 * the restrictions contained in a BSD-style copyright.)
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifdef  _MSC_VER
#pragma warning( disable : 4255 4668 4710 4711 4820 )
#pragma setlocale( "C" )
#endif

#ifdef _MSC_VER
#if ( _MSC_VER < 1900 )
#define inline __inline
#endif /* _MSC_VER < 1900 */
#if __STDC_VERSION__ < 201112L
#define _Alignof __alignof
#define _Noreturn __declspec( noreturn )
#endif /* __STDC_VERSION__ < 201112L */
#define LIBLOCAL_RESTRICT __restrict
#endif /* _MSC_VER */

#include <windows.h>

#include <type_traits>
#include <system_error>

#include <cstdlib>
#include <cstdint>

#ifdef __AVX__
#include <intrin.h>
#endif /* __AVX__ */

#include "cryptoshim.h"

#include "arc4random.h"
#include "arc4random-signal.h"
#include "arc4random-synchronization.h"
#include "arc4random-crypto.h"

extern "C" int arc4random_onefillcore(/* _Out_write_bytes_all_(sizeof(uint32_t)) */ uint32_t *rcv) noexcept;

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4559 )
#endif /* _MSC_VER */
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
int cryptoshim::cprng::core::onefill(
cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
unsigned char *const LIBLOCAL_RESTRICT buf) noexcept
{
	return cryptoshim::cprng::core::onefill<unsigned char>(state, buf);
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
int cryptoshim::cprng::core::onefill(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	uint16_t *const LIBLOCAL_RESTRICT buf) noexcept
{
	return cryptoshim::cprng::core::onefill<uint16_t>(state, buf);
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
int cryptoshim::cprng::core::onefill(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	uint32_t *const LIBLOCAL_RESTRICT buf) noexcept
{
	return cryptoshim::cprng::core::onefill<uint32_t>(state, buf);
}

#ifdef _WIN64
int cryptoshim::cprng::core::onefill(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	cryptoshim::cprng::core::blk64i_t *const LIBLOCAL_RESTRICT buf) noexcept
{
	return cryptoshim::cprng::core::onefill<cryptoshim::cprng::core::blk64i_t>(state, buf);
}
#else /* _WIN64 */
#ifdef __AVX__
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
int cryptoshim::cprng::core::onefill(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	cryptoshim::cprng::core::blk64i_t *const LIBLOCAL_RESTRICT buf) noexcept
{
	return cryptoshim::cprng::core::onefill<cryptoshim::cprng::core::blk64i_t>(state, buf);
}
#endif /* __AVX__ */
#endif /* _WIN64 */

#ifdef __AVX__
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
int cryptoshim::cprng::core::onefill(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	__m128i *const LIBLOCAL_RESTRICT buf) noexcept
{
	return cryptoshim::cprng::core::onefill<__m128i>(state, buf);
}
#endif /* __AVX__ */

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
int cryptoshim::cprng::core::nfill(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	uint32_t *const LIBLOCAL_RESTRICT buf,
	const unsigned char nelements) noexcept
{
	return cryptoshim::cprng::core::nfill<uint32_t>(state, buf, nelements);
}

#ifdef __AVX__
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
int cryptoshim::cprng::core::nfill(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	__m128i *const LIBLOCAL_RESTRICT buf,
	const unsigned char nelements) noexcept
{
	return cryptoshim::cprng::core::nfill<__m128i>(state, buf, nelements);
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
int cryptoshim::cprng::core::nfill(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	__m256i *const LIBLOCAL_RESTRICT buf,
	const unsigned char nelements) noexcept
{
	return cryptoshim::cprng::core::nfill<__m256i>(state, buf, nelements);
}
#endif /* __AVX__ */
#ifdef _MSC_VER
#pragma warning( pop )
#endif /* _MSC_VER */

extern "C" int arc4random_onefillcore(/* _Out_write_bytes_all_(sizeof(uint32_t)) */ uint32_t *const LIBLOCAL_RESTRICT rcv)
{
	uint32_t buf;
	int ret;

	{
		cryptoshim_globalstate_t *const LIBLOCAL_RESTRICT globalstate = cryptoshim_globalstate;
		cryptoshim_arc4random_semaphore_t *const LIBLOCAL_RESTRICT semaphore = &cryptoshim_arc4random_globalcontroller.controller.semaphore;
		DWORD idx_state;

		{
			cryptoshim_arc4random_rngstate_t *LIBLOCAL_RESTRICT rngstate;

			cryptoshim::cprng::core::acqsemaphore(\
				globalstate, \
				semaphore, \
				(cryptoshim_arc4random_rngstate_t const *LIBLOCAL_RESTRICT *)&rngstate, \
				&idx_state );
			ret = cryptoshim::cprng::core::onefill(rngstate, &buf);
		}
		cryptoshim::cprng::core::relsemaphore(globalstate, semaphore, globalstate->arc4random.aux.semaphore.implicitheap.p_ent, idx_state);
	}

	*rcv = buf;
	return ret;
}

extern "C" uint32_t arc4random(void)
{
	uint32_t ret;

	if ((-1) != arc4random_onefillcore(&ret))
	{
		return ret;
	}
	else
	{
		// (void)TerminateProcess(GetCurrentProcess(), 0xC0000001L);
		RaiseFailFastException(NULL, NULL, 0);
		return ret;
	}
}
