#ifdef  _MSC_VER
#pragma warning( disable : 4255 4668 4820 )
#pragma setlocale( "C" )
#endif

#include <windows.h>
#include <synchapi.h>
#include <bcrypt.h>

#include <type_traits>

#include <cstdlib>
#include <cstdint>

#ifdef __AVX__
#include <intrin.h>
#endif /* __AVX__ */

#ifdef _MSC_VER
#if ( _MSC_VER < 1900 )
#define inline __inline
#endif /* _MSC_VER < 1900 */
#if __STDC_VERSION__ < 201112L
#define _Alignof __alignof
#define _Noreturn __declspec( noreturn )
#endif /* __STDC_VERSION__ < 201112L */
#define LIBLOCAL_RESTRICT __restrict
#endif /* _MSC_VER */

#include "cryptoshim.h"

namespace cryptoshim
{
	namespace core
	{
#ifdef __AVX__
#ifdef _WIN64
		template<typename T> typename std::enable_if<(!std::is_same<size_t, T>::value && !std::is_same<size_t, DWORD>::value) || (std::is_same<size_t, T>::value && 64 != CHAR_BIT*sizeof(size_t) && 32 != CHAR_BIT*sizeof(size_t)), void>::type bitscanr(DWORD *, T) noexcept;
		template<typename T> typename std::enable_if<std::is_same<size_t, T>::value && 64 == CHAR_BIT*sizeof(size_t), void>::type bitscanr(DWORD *, T) noexcept;
		template<typename T> typename std::enable_if<(std::is_same<size_t, T>::value && 32 == CHAR_BIT*sizeof(size_t)) || std::is_same<size_t, DWORD>::value, void>::type bitscanr(DWORD *, T) noexcept;
#else /* _WIN64 */
		template<typename T> typename std::enable_if<(!std::is_same<size_t, T>::value && !std::is_same<size_t, DWORD>::value) || (std::is_same<size_t, T>::value && 32 != CHAR_BIT*sizeof(size_t)), void>::type bitscanr(DWORD *, T) noexcept;
		template<typename T> typename std::enable_if<(std::is_same<size_t, T>::value && 32 == CHAR_BIT*sizeof(size_t)) || std::is_same<size_t, DWORD>::value, void>::type bitscanr(DWORD *, T) noexcept;
#endif /* _WIN64 */
#else /* __AVX__ */
		template<typename T> void bitscanr(DWORD *, T) noexcept; /* Making certain T >= 1 is user's responsibility. */
#endif /* __AVX__ */

		template<typename T> T getalignedlen(T len, T alignment, DWORD *exponent) noexcept;

		inline size_t getalignedlen(size_t len, size_t alignment, DWORD *exponent) noexcept;
		inline DWORD getalignedlen(DWORD len, DWORD alignment, DWORD *exponent) noexcept;
		inline void *getalignedbase(const void *const base, const DWORD exponent) noexcept;
		DWORD getcorecount(HANDLE) noexcept;

		bool memprot(cryptoshim_globalstate_t const *globalstate, DWORD *flProtect) noexcept;
		void finalize(cryptoshim_globalstate_t const *globalstate) noexcept;
		void finalizec(cryptoshim_globalstate_t *globalstate) noexcept;
		void finalizep(cryptoshim_globalstate_t const *globalstate) noexcept;
		cryptoshim_globalstate_t *initialize(void) noexcept;
	} /* END: core */
	namespace cprng
	{
		namespace core
		{
			void closesession(cryptoshim_globalstate_t const *globalstate) noexcept;
			void closesessionc(cryptoshim_globalstate_t *globalstate) noexcept;
			void closesessionp(cryptoshim_globalstate_t const *globalstate) noexcept;
			bool opensession(cryptoshim_globalstate_t *globalstate, cryptoshim_arc4random_globalcontroller_t *globalcontroller, DWORD *flProtect) noexcept;
		} /* END: core */

	} /* END: cprng */
} /* END: cryptoshim */

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4559 )
#endif /* _MSC_VER */
template<typename T>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
#ifdef __AVX__
#ifdef _WIN64
typename std::enable_if<(!std::is_same<size_t, T>::value && !std::is_same<size_t, DWORD>::value) || (std::is_same<size_t, T>::value && 64 != CHAR_BIT*sizeof(size_t) && 32 != CHAR_BIT*sizeof(size_t)), void>::type
#else /* _WIN64 */
typename std::enable_if<(!std::is_same<size_t, T>::value && !std::is_same<size_t, DWORD>::value) || (std::is_same<size_t, T>::value && 32 != CHAR_BIT*sizeof(size_t)), void>::type
#endif /* _WIN64 */
#else /* __AVX__*/
void
#endif /* __AVX__ */
cryptoshim::core::bitscanr(
	DWORD *const LIBLOCAL_RESTRICT dst,
	T val) noexcept { for (*dst = static_cast<T>(0); val > (1); (*dst)++) val >>= (1); }

#ifdef __AVX__
#ifdef _WIN64
template<typename T>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
typename std::enable_if<std::is_same<size_t, T>::value && 64 == CHAR_BIT*sizeof(size_t), void>::type
cryptoshim::core::bitscanr(
	DWORD *const LIBLOCAL_RESTRICT dst,
	T val) noexcept { _BitScanReverse64(dst, val); }
#endif /* _WIN64 */

template<typename T>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
typename std::enable_if<(std::is_same<size_t, T>::value && 32 == CHAR_BIT*sizeof(size_t)) || std::is_same<size_t, DWORD>::value, void>::type
cryptoshim::core::bitscanr(
	DWORD *const LIBLOCAL_RESTRICT dst,
	T val) noexcept { _BitScanReverse(dst, val); }
#endif /* __AVX__ */

template<typename T>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
T cryptoshim::core::getalignedlen(
	const T len,
	const T alignment,
	DWORD *const LIBLOCAL_RESTRICT exponent) noexcept
{
	static_assert(std::is_integral<T>::value, "std::is_integral<T> fails.");

	DWORD local_exponent;
	T local_len;

	local_len = (alignment - 1) + len;
	cryptoshim::core::bitscanr<T>(&local_exponent, local_len);
	{
		T ret = static_cast<T>(1) << local_exponent;

		/*_
		 * 2**local_exponent <= len + (alignment - 1) < 2**(local_exponent + 1)
		 * holds at this point, where local_exponent >= 0 is guaranteed, since 0 < len.
		 */
		if (ret < local_len) {
			ret <<= 1;
			local_exponent++;
		}
		/* "ret" is not expected to overflow at this point. */

		*exponent = local_exponent;
		return ret;
	}
}

#ifdef __AVX__
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
inline size_t cryptoshim::core::getalignedlen(size_t len, size_t alignment, DWORD *const LIBLOCAL_RESTRICT exponent) noexcept { return cryptoshim::core::getalignedlen<size_t>(len, alignment, exponent); }
#else /* __AVX__ */
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
inline size_t cryptoshim::core::getalignedlen(size_t len, size_t alignment, DWORD *const LIBLOCAL_RESTRICT exponent) noexcept { return cryptoshim::core::getalignedlen<size_t>(len, alignment, exponent); }
#endif /* __AVX__ */

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
inline DWORD cryptoshim::core::getalignedlen(DWORD len, DWORD alignment, DWORD *const LIBLOCAL_RESTRICT exponent) noexcept{ return cryptoshim::core::getalignedlen<DWORD>(len, alignment, exponent); }

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
inline void *cryptoshim::core::getalignedbase(const void *const base, const DWORD exponent) noexcept
{
	uintptr_t ret = ((uintptr_t)base >> exponent) << exponent;

	if (base == (void*)ret) return (void*)ret;
	else return (void*)((static_cast<uintptr_t>(1) << exponent) + ret);
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
DWORD cryptoshim::core::getcorecount(HANDLE h_heap) noexcept
{
	SYSTEM_LOGICAL_PROCESSOR_INFORMATION *LIBLOCAL_RESTRICT p_ent;
	void *p_base;
	DWORD cb_len = static_cast<DWORD>(0), status, offset, ret;

	/* BEGIN */
	if (GetLogicalProcessorInformation(NULL, &cb_len)) return (0); /* GetLogicalProcessorInformation() is guaranteed to fail. */
	status = GetLastError();
	if (status != ERROR_INSUFFICIENT_BUFFER) return (0);
	SetLastError(ERROR_SUCCESS);

	cb_len += alignof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) - 1;
	if (cb_len <= alignof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) - 1) return (0);

	p_base = HeapAlloc(h_heap, 0, cb_len);
	if (NULL == p_base) return (0);
	{
		DWORD exponent;
		cryptoshim::core::bitscanr(&exponent, alignof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION));
		p_ent = static_cast<SYSTEM_LOGICAL_PROCESSOR_INFORMATION*>(cryptoshim::core::getalignedbase(p_base, exponent));
	}
	if (NULL == p_ent) { (void)HeapFree(h_heap, 0, p_base); return (0); }

	cb_len -= (_Alignof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) - 1);
	/* END */

	/* p_base != NULL holds at this point. */

	/* This loop is not going to last forever. */
	for (; !GetLogicalProcessorInformation(p_ent, &cb_len); cb_len -= _Alignof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) - 1)
	{
		status = GetLastError();
		SetLastError(ERROR_SUCCESS);
		(void)HeapFree(h_heap, 0, p_base);
		if (status != ERROR_INSUFFICIENT_BUFFER) return (0);

		cb_len += alignof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) - 1;
		if (cb_len <= alignof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) - 1) return (0);

		p_base = HeapAlloc(h_heap, 0, cb_len);
		if (NULL == p_base) return (0);
		{
			DWORD exponent;
			cryptoshim::core::bitscanr(&exponent, alignof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION));
			p_ent = static_cast<SYSTEM_LOGICAL_PROCESSOR_INFORMATION*>(cryptoshim::core::getalignedbase(p_base, exponent));
		}
		if (NULL == p_ent) { (void)HeapFree(h_heap, 0, p_base); return (0); }
	}

	for (offset = 0, ret = 0; offset <= cb_len - sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION); p_ent++)
	{
		if (p_ent->Relationship == RelationProcessorCore) ret++;

		offset += sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
	}

	(void)HeapFree(h_heap, 0, p_base);
	return ret;
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
bool cryptoshim::core::memprot(
	cryptoshim_globalstate_t const *const LIBLOCAL_RESTRICT globalstate,
	DWORD *const LIBLOCAL_RESTRICT flProtect) noexcept
{
	if (VirtualProtect(globalstate->base, globalstate->len, PAGE_READONLY, flProtect))
	{
		return true;
	}
	else return false;
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
void cryptoshim::core::finalize(cryptoshim_globalstate_t const *const LIBLOCAL_RESTRICT globalstate) noexcept
{
	/*if (NULL == globalstate) return;*/
	if (NULL != globalstate->h_alg.x_bcrypt.h_rng)
	{
		(void)BCryptCloseAlgorithmProvider(globalstate->h_alg.x_bcrypt.h_rng, 0);
	}
	if (NULL != globalstate->base)
	{
		(void)VirtualFree(globalstate->base, 0, MEM_RELEASE);
	}
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
void cryptoshim::core::finalizec(cryptoshim_globalstate_t *const LIBLOCAL_RESTRICT globalstate) noexcept
{
	if (EXIT_SUCCESS == BCryptCloseAlgorithmProvider(globalstate->h_alg.x_bcrypt.h_rng, 0))
	{
		globalstate->h_alg.x_bcrypt.h_rng = NULL;
	}
	(void)VirtualFree(globalstate->base, 0, MEM_RELEASE);
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
void cryptoshim::core::finalizep(cryptoshim_globalstate_t const *const LIBLOCAL_RESTRICT globalstate) noexcept
{
	/*(void)FreeLibrary(globalstate->h_mod.x_bcryptprimitives);*/
	(void)BCryptCloseAlgorithmProvider(globalstate->h_alg.x_bcrypt.h_rng, 0);
	(void)VirtualFree(globalstate->base, 0, MEM_RELEASE);
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
cryptoshim_globalstate_t *cryptoshim::core::initialize(void) noexcept
{
	cryptoshim_globalstate_t *LIBLOCAL_RESTRICT ret;
	void *p_base;
	HANDLE h_heap;

	{
		size_t len;
		DWORD allocalign, corecount;

		h_heap = GetProcessHeap();
		if (NULL == h_heap) return NULL;

		corecount = cryptoshim::core::getcorecount(h_heap);
		if ((0) == corecount) return NULL;

		{
			SYSTEM_INFO nfoSystem;

			GetSystemInfo(&nfoSystem);

			{
				DWORD val_lesser, val_greater;

				if (!((nfoSystem.dwPageSize >> (1)) && (nfoSystem.dwAllocationGranularity >> (1)))) return NULL;
				else if (nfoSystem.dwAllocationGranularity >= nfoSystem.dwPageSize) {
					val_lesser = nfoSystem.dwPageSize;
					val_greater = nfoSystem.dwAllocationGranularity;
				}
				else {
					val_lesser = nfoSystem.dwAllocationGranularity;
					val_greater = nfoSystem.dwPageSize;
				}
				for (allocalign = val_greater; val_lesser < val_greater; val_greater >>= (1));
				if (val_lesser != val_greater) return NULL;
			}
		}

		{
			DWORD exponent;

			len = cryptoshim::core::getalignedlen(sizeof(cryptoshim_globalstate_t), alignof(cryptoshim_globalstate_t), &exponent);

			p_base = VirtualAlloc(NULL, len, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
			if (NULL == p_base) return NULL;
			/*_
			 * "SecureZeroMemory(p_base, len);" is not needed, since the memory block has already been zeroed,
			 * per https://msdn.microsoft.com/en-us/library/windows/desktop/aa366887.aspx ,
			 * in particular, MEM_COMMIT in flAllocationType.
			 */

			ret = static_cast<cryptoshim_globalstate_t*>(cryptoshim::core::getalignedbase(p_base, exponent));
		}
		if (NULL == ret) return NULL;
		ret->h_heap = h_heap;
		ret->allocalign = allocalign;
		ret->corecount = corecount;
		ret->base = p_base;
		ret->len = len;
	}
	ret->h_alg.x_bcrypt.h_rng = NULL;

	{
		HANDLE h_alg = NULL;
		NTSTATUS status;

		/* BEGIN: RNG */
		status = BCryptOpenAlgorithmProvider(&h_alg, BCRYPT_RNG_ALGORITHM, MS_PLATFORM_CRYPTO_PROVIDER, 0); /* TPM et al. */
		if (EXIT_SUCCESS != status || NULL == h_alg)
		{
			status = BCryptOpenAlgorithmProvider(&h_alg, BCRYPT_RNG_ALGORITHM, MS_PRIMITIVE_PROVIDER, 0);
			if (EXIT_SUCCESS != status || NULL == h_alg)
			{
				(void)VirtualFree(p_base, 0, MEM_RELEASE);
				p_base = NULL;
				ret = NULL;
				return NULL;
			}
		}
		ret->h_alg.x_bcrypt.h_rng = h_alg;
		/* END: RNG */
	}

	return ret;
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
void cryptoshim::cprng::core::closesession(cryptoshim_globalstate_t const *const LIBLOCAL_RESTRICT globalstate) noexcept
{
	/*if (NULL == globalstate) return;*/
	if (NULL != globalstate->arc4random.aux.semaphore.implicitheap.p_base)
	{
		(void)HeapFree(globalstate->h_heap, 0, globalstate->arc4random.aux.semaphore.implicitheap.p_base);
	}
	if (NULL != globalstate->arc4random.mem.p_base)
	{
		(void)UnmapViewOfFile(globalstate->arc4random.mem.p_base);
	}
	if (NULL != globalstate->arc4random.mem.h_mmap)
	{
		(void)CloseHandle(globalstate->arc4random.mem.h_mmap);
	}

	return;
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
void cryptoshim::cprng::core::closesessionc(cryptoshim_globalstate_t *const LIBLOCAL_RESTRICT globalstate) noexcept
{
	if (HeapFree(globalstate->h_heap, 0, globalstate->arc4random.aux.semaphore.implicitheap.p_base))
	{
		globalstate->arc4random.aux.semaphore.implicitheap.p_ent = NULL;
		globalstate->arc4random.aux.semaphore.implicitheap.p_base = NULL;
	}
	if (UnmapViewOfFile(globalstate->arc4random.mem.p_base))
	{
		globalstate->arc4random.mem.p_base = NULL;
	}
	if (CloseHandle(globalstate->arc4random.mem.h_mmap))
	{
		globalstate->arc4random.mem.h_mmap = NULL;
	}

	return;
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
void cryptoshim::cprng::core::closesessionp(cryptoshim_globalstate_t const *const LIBLOCAL_RESTRICT globalstate) noexcept
{
	(void)HeapFree(globalstate->h_heap, 0, globalstate->arc4random.aux.semaphore.implicitheap.p_base);
	(void)UnmapViewOfFile(globalstate->arc4random.mem.p_base);
	(void)CloseHandle(globalstate->arc4random.mem.h_mmap);

	return;
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
bool cryptoshim::cprng::core::opensession(
	cryptoshim_globalstate_t *const LIBLOCAL_RESTRICT globalstate,
	cryptoshim_arc4random_globalcontroller_t *const LIBLOCAL_RESTRICT globalcontroller,
	DWORD *const LIBLOCAL_RESTRICT flProtect) noexcept
{
	/* #1 */
	{
		DWORD nstates;

		{
			const DWORD allocalign = globalstate->allocalign;

			if (allocalign <= _Alignof(cryptoshim_arc4random_rngstate_t) - 1)
			{
				nstates = (1);
			}
			else
			{
				nstates = (allocalign - (_Alignof(cryptoshim_arc4random_rngstate_t) - 1)) / sizeof(cryptoshim_arc4random_rngstate_t);
				{
					const DWORD corecount = globalstate->corecount;

					if (nstates >> (1))
					{
						if (corecount < nstates) nstates = corecount;
					}
					else nstates = (1);
				}
			}
		}
		globalstate->arc4random.mem.nstates = nstates;
	}
	globalstate->mask |= CRYPTOSHIM_GLOBALSTATE_MASK_ARC4RANDOM_SEMAPHORE;

	/* #2 */
	for (; globalstate->arc4random.mem.nstates >= (1); globalstate->arc4random.mem.nstates >>= (1))
	{
		void *p_base;
		HANDLE h_mmap;
		void *p_implicitheapbase;

		{
			DWORD *p_implicitheapent;

			p_implicitheapbase = HeapAlloc(globalstate->h_heap, 0, globalstate->arc4random.mem.nstates*sizeof(DWORD) + (_Alignof(DWORD) - 1));
			if (NULL == p_implicitheapbase) continue;
			{
				DWORD exponent;

				cryptoshim::core::bitscanr(&exponent, alignof(DWORD));
				p_implicitheapent = (DWORD*)cryptoshim::core::getalignedbase(p_implicitheapbase, exponent);
			}

			if (NULL == p_implicitheapent)
			{
				(void)HeapFree(globalstate->h_heap, 0, p_implicitheapbase);
				continue;
			}
			globalstate->arc4random.aux.semaphore.implicitheap.p_base = p_implicitheapbase;
			globalstate->arc4random.aux.semaphore.implicitheap.p_ent = p_implicitheapent;

			{
				DWORD i = 0;

				for (; i < globalstate->arc4random.mem.nstates; i++) p_implicitheapent[i] = i;
			}
			globalcontroller->controller.semaphore.counter_heap = globalstate->arc4random.mem.nstates;
		}

		InitializeSRWLock(&globalcontroller->controller.semaphore.lock_semaphore);
		InitializeConditionVariable(&globalcontroller->controller.semaphore.cond_semaphore_nonsaturation);
		InitializeConditionVariable(&globalcontroller->controller.semaphore.cond_semaphore_notinprogress);

		{
			DWORD nbytes;

			/* BEGIN: mmap */
			nbytes = (alignof(cryptoshim_arc4random_rngstate_t) - 1) + sizeof(cryptoshim_arc4random_rngstate_t)*globalstate->arc4random.mem.nstates;

			h_mmap = CreateFileMappingW(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE | SEC_COMMIT, 0, nbytes, NULL);
			if (NULL == h_mmap) { (void)HeapFree(globalstate->h_heap, 0, p_implicitheapbase); continue; }

			p_base = MapViewOfFile(h_mmap, FILE_MAP_WRITE, 0, 0, nbytes);
			if (NULL == p_base) { (void)CloseHandle(h_mmap); (void)HeapFree(globalstate->h_heap, 0, p_implicitheapbase); continue; }

			globalstate->arc4random.mem.nbytes = nbytes;
		}

		{
			cryptoshim_arc4random_rngstate_t *LIBLOCAL_RESTRICT p_states;

			{
				DWORD exponent;

				cryptoshim::core::bitscanr(&exponent, alignof(DWORD));
				p_states = (cryptoshim_arc4random_rngstate_t*)cryptoshim::core::getalignedbase(p_base, exponent);
			}
			if (NULL == p_states)
			{
				(void)UnmapViewOfFile(p_base);
				(void)CloseHandle(h_mmap);
				(void)HeapFree(globalstate->h_heap, 0, p_implicitheapbase);
				continue;
			}
			globalstate->arc4random.mem.h_mmap = h_mmap;
			globalstate->arc4random.mem.p_base = p_base;
			globalstate->arc4random.mem.p_ent = p_states;

			/* The initial contents of the pages in a file mapping object backed by the operating system paging file are 0 (zero). */
			/* 0 is invalid per MSDN's GetProcessIdOfThread() page. */
			/*{
				DWORD i = 0;

				for (i = 0; i < globalstate->arc4random.mem.nstates; i++)
				{
					p_states[i].pid = 0;
					p_states[i].counter = 0;
				}
			}*/
		}
		if (!VirtualProtect(globalstate->arc4random.mem.p_base, globalstate->arc4random.mem.nbytes, PAGE_WRITECOPY, flProtect))
		{
			(void)UnmapViewOfFile(p_base);
			(void)CloseHandle(h_mmap);
			(void)HeapFree(globalstate->h_heap, 0, p_implicitheapbase);
			continue;
		}
		if (!SetHandleInformation(h_mmap, HANDLE_FLAG_INHERIT, HANDLE_FLAG_INHERIT))
		{
			(void)UnmapViewOfFile(p_base);
			(void)CloseHandle(h_mmap);
			(void)HeapFree(globalstate->h_heap, 0, p_implicitheapbase);
			continue;
		}
		/* END: mmap */

		return true;
	}
	return false;
}
#ifdef _MSC_VER
#pragma warning( pop )
#endif /* _MSC_VER */

namespace
{
	namespace declaration
	{
		struct globalstate
		{
			explicit globalstate(void) noexcept;
			~globalstate(void) noexcept;
			globalstate operator =(globalstate) = delete;

			cryptoshim_globalstate_t *e = NULL;
		};
	}
	namespace definition
	{
		::declaration::globalstate instance;
	}
}

::declaration::globalstate::globalstate(void) noexcept
{
	DWORD flProtect;

	e = cryptoshim::core::initialize();
	if (NULL == e) return;

	if (false == cryptoshim::cprng::core::opensession(e, &cryptoshim_arc4random_globalcontroller, &flProtect))
	{
		cryptoshim::core::finalizec(e);
		e = NULL;
		return;
	}
	if (false == cryptoshim::core::memprot(e, &flProtect))
	{
		MEMORY_BASIC_INFORMATION nfoMemory;

		if (!VirtualQuery(e->base, &nfoMemory, sizeof(MEMORY_BASIC_INFORMATION)))
		{
			cryptoshim::cprng::core::closesessionp(e);
			cryptoshim::core::finalizep(e);
			e = NULL;
			return;
		}
		if (PAGE_READONLY & nfoMemory.Protect)
		{
			cryptoshim::cprng::core::closesessionp(e);
			cryptoshim::core::finalizep(e);
			e = NULL;
			return;
		}
		else
		{
			cryptoshim::cprng::core::closesessionc(e);
			cryptoshim::core::finalizec(e);
			e = NULL;
			return;
		}
	}
	else
	{
		cryptoshim_globalstate = e;
		return;
	}
}

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4559 )
#endif /* _MSC_VER */
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
::declaration::globalstate::~globalstate(void) noexcept
{
	if (NULL == e) return;
	cryptoshim::cprng::core::closesession(e);
	cryptoshim::core::finalize(e);
	return;
}
#ifdef _MSC_VER
#pragma warning( pop )
#endif /* _MSC_VER */

cryptoshim_arc4random_globalcontroller_t cryptoshim_arc4random_globalcontroller;
cryptoshim_globalstate_t *cryptoshim_globalstate;
