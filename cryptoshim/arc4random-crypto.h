namespace cryptoshim
{
	namespace cprng
	{
		namespace core
		{
			/*_
			 * 0 --- SUCCESS
			 * -1 --- failure in getentropy() w/ errno valued at EIO
			 */
			int reseed(cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state) noexcept;

			void rekey(cryptoshim_arc4random_rngstate_cipherstate_t *p_rngcipherstate, cryptoshim_arc4random_rngstate_buf_t *p_rngbuf, unsigned long *p_rngctr) noexcept;
#ifdef __AVX__
			/* SSE */ /*void rekey(cryptoshim_arc4random_rngstate_cipherstate_t *p_rngcipherstate, cryptoshim_arc4random_rngstate_buf_t *p_rngbuf, unsigned long *p_rngctr, __m128i *seedbuf) noexcept;*/
			/* AVX */ void rekey(cryptoshim_arc4random_rngstate_cipherstate_t *p_rngcipherstate, cryptoshim_arc4random_rngstate_buf_t *p_rngbuf, unsigned long *p_rngctr, __m256i *seedbuf) noexcept;
#else /* __AVX__ */
			void rekey(cryptoshim_arc4random_rngstate_cipherstate_t *p_rngcipherstate, cryptoshim_arc4random_rngstate_buf_t *p_rngbuf, unsigned long *p_rngctr, uint32_t *seedbuf) noexcept;
#endif /* __AVX__ */
		} /* END: core*/
	} /* END: cprng */

} /* END: cryptoshim */
