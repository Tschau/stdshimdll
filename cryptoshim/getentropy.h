#ifdef __cplusplus
namespace cryptoshim
{
	int getentropy(void *buf, size_t buflen) noexcept;
}
#else /* __cpluslpus */
int getentropy(void *buf, size_t buflen);
#endif /* __cplusplus */
