/*	$NetBSD: cprng_fast.c,v 1.13 2015/04/13 22:43:41 riastradh Exp $	*/
/*-
 * Copyright (c) 2014 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * This code is derived from software contributed to The NetBSD Foundation
 * by Taylor R. Campbell.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*	$OpenBSD: rnd.c,v 1.182 2016/07/15 19:02:30 tom Exp $	*/
/*
 * Copyright (c) 2011 Theo de Raadt.
 * Copyright (c) 2008 Damien Miller.
 * Copyright (c) 1996, 1997, 2000-2002 Michael Shalayeff.
 * Copyright (c) 2013 Markus Friedl.
 * Copyright Theodore Ts'o, 1994, 1995, 1996, 1997, 1998, 1999.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, and the entire permission notice in its entirety,
 *    including the disclaimer of warranties.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * ALTERNATIVELY, this product may be distributed under the terms of
 * the GNU Public License, in which case the provisions of the GPL are
 * required INSTEAD OF the above restrictions.  (This clause is
 * necessary due to a potential bad interaction between the GPL and
 * the restrictions contained in a BSD-style copyright.)
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifdef  _MSC_VER
#pragma warning( disable : 4255 4505 4668 4710 4711 4820 )
#pragma setlocale( "C" )
#endif

#ifdef _MSC_VER
#if ( _MSC_VER < 1900 )
#define inline __inline
#endif /* _MSC_VER < 1900 */
#if __STDC_VERSION__ < 201112L
#define _Alignof __alignof
#define _Noreturn __declspec( noreturn )
#endif /* __STDC_VERSION__ < 201112L */
#define LIBLOCAL_RESTRICT __restrict
#endif /* _MSC_VER */

#include <windows.h>

#include <type_traits>
#include <system_error>

#include <cstdlib>
#include <cstdint>

#ifdef __AVX__
#include <intrin.h>
#endif /* __AVX__ */

#include "cryptoshim.h"
#include "getentropy.h"

#include "arc4random.h"
#include "arc4random-signal.h"
#include "arc4random-synchronization.h"
#include "arc4random-crypto.h"

namespace cryptoshim
{
	namespace cprng
	{
		namespace core
		{
			/*_
			 * 1 --- SUCCESS W/ TERMINATION
			 * 0 --- SUCCESS W/ CONTINUATION
			 * -1 --- getentropy() failure w/ EIO
			 */
			template<typename TSRC, typename TDST> int trimfore(cryptoshim_arc4random_rngstate_t *state, TSRC *LIBLOCAL_RESTRICT *buf, unsigned char *gap, size_t *nbytes) noexcept;
			template<typename TSRC, typename TDST> int trimrear(cryptoshim_arc4random_rngstate_t *state, TDST *buf, size_t *quot, size_t *nbytes) noexcept;
			template<typename TSRC, typename TDST> int trimboth(cryptoshim_arc4random_rngstate_t *state, TSRC *LIBLOCAL_RESTRICT *buf, unsigned char *gap, size_t *quot, size_t *nbytes) noexcept;
			template<typename T> int discharge(cryptoshim_arc4random_rngstate_t *state, T *LIBLOCAL_RESTRICT *buf, size_t *nbytes_quot) noexcept;
			template<typename T> int consume(cryptoshim_arc4random_rngstate_t *state, T *buf, size_t nbytes_quot) noexcept;
		} /* END: core */
	} /* END: cprng */
} /* END: cryptoshim */

extern "C" int arc4random_fillbufcore(void *buf, size_t nbytes) noexcept;

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4559 )
#endif /* _MSC_VER */
template<typename TSRC, typename TDST>
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
int cryptoshim::cprng::core::trimfore(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	TSRC *LIBLOCAL_RESTRICT *const LIBLOCAL_RESTRICT buf,
	unsigned char *const LIBLOCAL_RESTRICT gap,
	size_t *const LIBLOCAL_RESTRICT nbytes) noexcept
{
	static_assert(sizeof(TSRC) <= sizeof(TDST), "sizeof(TSRC) > sizeof(TDST)");
	static_assert((0) == sizeof(TDST) % sizeof(TSRC), "(0) != sizeof(TDST) % sizeof(TSRC)");

	unsigned char rem;

	rem = (uintptr_t)((void*)*buf) % sizeof(TDST);
	*gap = rem ? sizeof(TDST) - rem : 0;

	if ((0) == rem) return (0);

	if ((-1) == cryptoshim::cprng::core::onefill(state, *buf))
	{
		return (-1);
	}

	if (*nbytes <= (size_t)*gap) return (1);
	else
	{
		*buf = (TSRC*)(*gap + (uintptr_t)((void*)*buf));
		*nbytes -= *gap;
		return (0);
	}
}

template<typename TSRC, typename TDST>
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
int cryptoshim::cprng::core::trimrear(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	TDST *const LIBLOCAL_RESTRICT buf,
	/* _Out_writes_bytes_all(sizeof(size_t)) */ size_t *const LIBLOCAL_RESTRICT quot,
	size_t *const LIBLOCAL_RESTRICT nbytes) noexcept
{
	static_assert(sizeof(TSRC) <= sizeof(TDST), "sizeof(TSRC) > sizeof(TDST)");
	static_assert((0) == sizeof(TDST) % sizeof(TSRC), "(0) != sizeof(TDST) % sizeof(TSRC)");

	unsigned char rem;

	*quot = *nbytes / sizeof(TDST);
	rem = *nbytes % sizeof(TDST);

	if (rem)
	{
		if ((-1) == cryptoshim::cprng::core::onefill(state, (TSRC*)(*quot + buf)))
		{
			return (-1);
		}
		*nbytes -= rem;
	}

	if ((0) == *quot) return (1);
	else return (0);
}

template<typename TSRC, typename TDST>
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
int cryptoshim::cprng::core::trimboth(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	TSRC *LIBLOCAL_RESTRICT *const LIBLOCAL_RESTRICT buf,
	/* _Out_writes_bytes_all(sizeof(unsigned char)) */ unsigned char *const LIBLOCAL_RESTRICT gap,
	/* _Out_writes_bytes_all(sizeof(unsigned char)) */ size_t *const LIBLOCAL_RESTRICT quot,
	size_t *const LIBLOCAL_RESTRICT nbytes) noexcept
{
	int ret;

	ret = cryptoshim::cprng::core::trimfore<TSRC, TDST>(state, buf, gap, nbytes);
	if (ret) return ret;
	return cryptoshim::cprng::core::trimrear<TSRC, TDST>(state, (TDST*)*buf, quot, nbytes);
}

template<typename T>
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
int cryptoshim::cprng::core::discharge(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	T *LIBLOCAL_RESTRICT *const LIBLOCAL_RESTRICT buf,
	size_t *const LIBLOCAL_RESTRICT nbytes_quot) noexcept
{
	static_assert(sizeof(T) >= sizeof(uint32_t), "sizeof(T) < sizeof(uint32_t)");

	unsigned long *p_counter;
	T *LIBLOCAL_RESTRICT stream;

	p_counter = &state->counter;
	stream = (T*)(cryptoshim::cprng::core::offsetofstream<T> + (uintptr_t)(void*)&state->buf);

	while (*nbytes_quot >= cryptoshim::cprng::core::rngbufcch<T>)
	{
		if (cryptoshim::cprng::core::sentinel<sizeof(T)*cryptoshim::cprng::core::rngbufcch<T>>(p_counter, &state->pid))
		{
			if ((-1) == cryptoshim::cprng::core::reseed(state))
			{
				/* getentropy() just set EIO. */
				return (-1);
			}
		}
		cryptoshim::cprng::core::rekey(&state->cipherstate, &state->buf, p_counter);

		cryptoshim::cprng::core::drain<T>(*buf, stream);

		cryptoshim::cprng::core::clean<sizeof(T)*cryptoshim::cprng::core::rngbufcch<T>>(p_counter);

		*buf += cryptoshim::cprng::core::rngbufcch<T>;
		*nbytes_quot -= cryptoshim::cprng::core::rngbufcch<T>;
	}
	*p_counter &= ~0x07ff;

	if ((0) == *nbytes_quot) return (1);
	else return (0);
}

template<typename T>
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
int cryptoshim::cprng::core::consume(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	T *LIBLOCAL_RESTRICT buf,
	size_t nbytes_quot) noexcept
{
	/* #1 */
	if (sizeof(T) <= (0x07ff & state->counter))
	{
		unsigned long cch_len = (0x07ff & state->counter) / sizeof(T);

		if (nbytes_quot < (size_t)cch_len)
		{
			cch_len = 0x07ff & nbytes_quot;
		}

		if ((-1) == cryptoshim::cprng::core::nfill(state, buf, (unsigned char)cch_len))
		{
			return (-1);
		}

		buf += cch_len;
		nbytes_quot -= cch_len;

		if ((0) == nbytes_quot) return (0);
	}
	/*_
	 * nbytes_quot > 0 holds still
	 * at this point, and
	 * ctr_low11 reaches its best.
	 */

	/* #2 */
	{
		int ret;

		ret = cryptoshim::cprng::core::discharge<T>(state, &buf, &nbytes_quot);
		if (ret) return ret;
	}
	/* nbytes_quot > 0 && ctr_low11 > sizeof(T)* nbytes_quot holds at this point. */

	/* #3 */
	if ((-1) == cryptoshim::cprng::core::nfill(state, buf, (unsigned char)nbytes_quot))
	{
		return (-1);
	}

	return (0);
}

/*_
 * Ensuring 0 != nbytes AND NULL != buf is the responsibility
 * of the calling function.
 *
 * 0 --- SUCCESS
 * -1 --- getentropy() failure w/ EIO
 */
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
int cryptoshim::cprng::core::fillbuf(
	cryptoshim_arc4random_rngstate_t *const LIBLOCAL_RESTRICT state,
	uint32_t *LIBLOCAL_RESTRICT buf,
	size_t nbytes) noexcept
{
	size_t nbytes_quot;
	int ret;
	unsigned char buf_gap;

	ret = cryptoshim::cprng::core::trimboth<unsigned char, uint16_t>(state, (unsigned char**)&buf, &buf_gap, &nbytes_quot, &nbytes);
	if (ret) return ret;
	ret = cryptoshim::cprng::core::trimboth<uint16_t, uint32_t>(state, (uint16_t**)&buf, &buf_gap, &nbytes_quot, &nbytes);
	if (ret) return ret;
#ifdef __AVX__
	ret = cryptoshim::cprng::core::trimboth<uint32_t, cryptoshim::cprng::core::blk64i_t>(state, (uint32_t**)&buf, &buf_gap, &nbytes_quot, &nbytes);
	if (ret) return ret;
#endif /* __AVX__ */

	/* nbytes_quot > 0 holds while buf is aligned with either sizeof(uint32_t) * 2 or sizeof(uint64_t). */

#ifdef __AVX__
	ret = cryptoshim::cprng::core::trimboth<cryptoshim::cprng::core::blk64i_t, __m128i>(state, (cryptoshim::cprng::core::blk64i_t**)&buf, &buf_gap, &nbytes_quot, &nbytes);
	if (ret) return ret;

	ret = cryptoshim::cprng::core::trimboth<__m128i, __m256i>(state, (__m128i**)&buf, &buf_gap, &nbytes_quot, &nbytes);
	if (ret) return ret;
	/* nbytes_quot > 0 holds at this point. */

	/* SSE */ /*return cryptoshim::cprng::consume<__m128i>(state, (__m128i*)buf, nbytes_quot);*/
	return cryptoshim::cprng::core::consume<__m256i>(state, (__m256i*)buf, nbytes_quot);
#else /* __AVX__ */
	return cryptoshim::cprng::core::consume<uint32_t>(state, buf, nbytes_quot);
#endif /* __AVX__ */
}
#ifdef _MSC_VER
#pragma warning( pop )
#endif /* _MSC_VER */

/*
 * 0 --- SUCCESS
 * -1 --- getentropy() failure w/ EIO
 */
extern "C" int arc4random_fillbufcore(void *buf, size_t nbytes)
{
	cryptoshim_globalstate_t *const LIBLOCAL_RESTRICT globalstate = cryptoshim_globalstate;
	cryptoshim_arc4random_semaphore_t *const LIBLOCAL_RESTRICT semaphore = &cryptoshim_arc4random_globalcontroller.controller.semaphore;
	DWORD idx_state;
	int ret;

	{
		cryptoshim_arc4random_rngstate_t *LIBLOCAL_RESTRICT rngstate;

		cryptoshim::cprng::core::acqsemaphore(globalstate, semaphore, (cryptoshim_arc4random_rngstate_t const *LIBLOCAL_RESTRICT *)&rngstate, &idx_state);
		ret = cryptoshim::cprng::core::fillbuf(rngstate, (uint32_t*)buf, nbytes); /* Ironically, buf need not align. */
	}
	cryptoshim::cprng::core::relsemaphore(globalstate, semaphore, globalstate->arc4random.aux.semaphore.implicitheap.p_ent, idx_state);

	return ret;
}

extern "C" void arc4random_buf(void *buf, size_t nbytes)
{
	/*
	if (buf == NULL || 0 == nbytes) return;
	*/

	if ((-1) == arc4random_fillbufcore(buf, nbytes))
	{
		// (void)TerminateProcess(GetCurrentProcess(), 0xC0000001L);
		RaiseFailFastException(NULL, NULL, 0);
		return;
	}
	else return;
}
