#ifdef  _MSC_VER
#pragma warning( disable : 4255 4577 4668 4820 )
#pragma setlocale( "C" )
#endif

#ifdef _MSC_VER
#if ( _MSC_VER < 1900 )
#define inline __inline
#endif
#if __STDC_VERSION__ < 201112L
#define _Alignof __alignof
#define _Noreturn __declspec( noreturn )
#endif
#define LIBLOCAL_RESTRICT __restrict
#endif

#include <windows.h>

#include <utility>

#include <cstdint>

#ifdef __AVX__
#include <intrin.h>
#endif /* __AVX__ */

#include "cryptoshim.h"
#include "arc4random-synchronization.h"

namespace cryptoshim
{
	namespace cprng
	{
		namespace core
		{
			inline DWORD getheapheight(DWORD nstates) noexcept;
			DWORD percolatedown(cryptoshim_arc4random_rngstate_t const *rngstate, DWORD *p_heap, DWORD nstates, DWORD height) noexcept;
			void percolateup(cryptoshim_arc4random_rngstate_t const *rngstate, DWORD *p_heap, DWORD idx_this ) noexcept;
		} /* END: core */
	} /* END: cprng */
} /* END: cryptoshim */

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4559 )
#endif /* _MSC_VER */
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
inline DWORD cryptoshim::cprng::core::getheapheight(
#ifdef __AVX__
	const DWORD nstates
#else /* __AVX__ */
	DWORD nstates
#endif /* __AVX__ */
) noexcept
{
	DWORD i;

#ifdef __AVX__
	_BitScanReverse(&i, nstates);
	return ++i;
#else
	for (i = 1; nstates >= (2); i++) nstates >>= (1);
	return i;
#endif /* __AVX__ */
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
DWORD cryptoshim::cprng::core::percolatedown(
	cryptoshim_arc4random_rngstate_t const *const LIBLOCAL_RESTRICT rngstate,
	DWORD *const LIBLOCAL_RESTRICT p_heap,
	DWORD nstates,
	DWORD height) noexcept
{
	size_t size_left, size_right;

	DWORD ret = 0;
	DWORD idx_left, idx_right;
	DWORD key_left, key_right;
	DWORD idx_exception;

	/*ret = 0;*/
	idx_exception = nstates / 2;
	idx_exception--;

	for (; --height > 0 && ret != idx_exception;)
	{
		idx_right = ((1 + ret) << 1);
		idx_left = idx_right - (1);
		key_right = p_heap[idx_right];
		key_left = p_heap[idx_left];

		size_left = rngstate[idx_left].counter;
		size_right = rngstate[idx_right].counter;
		if (size_left > size_right)
		{
			p_heap[ret] = key_left;
			ret = idx_left;
			continue;
		}
		else if (size_left < size_right)
		{
			p_heap[ret] = key_right;
			ret = idx_right;
			continue;
		}
		else
		{
			LARGE_INTEGER ctr_tick;

			(void)QueryPerformanceCounter(&ctr_tick);
			if ((1) & ctr_tick.LowPart)
			{
				p_heap[ret] = key_left;
				ret = idx_left;
			}
			else
			{
				p_heap[ret] = key_right;
				ret = idx_right;
			}
		}
	}
	if (ret == idx_exception)
	{
		unsigned char const rem = nstates % 2;

		if (rem)
		{
			idx_right = --nstates, key_right = p_heap[nstates];
			idx_left = --nstates, key_left = p_heap[nstates];

			size_left = rngstate[idx_left].counter;
			size_right = rngstate[idx_right].counter;
			if (size_left > size_right)
			{
				p_heap[ret] = key_left;
				return idx_left;
			}
			else if (size_left < size_right)
			{
				p_heap[ret] = key_right;
				return idx_right;
			}
			else
			{
				LARGE_INTEGER ctr_tick;

				(void)QueryPerformanceCounter(&ctr_tick);
				if ((1) & ctr_tick.LowPart)
				{
					p_heap[ret] = key_left;
					return idx_left;
				}
				else
				{
					p_heap[ret] = key_right;
					return idx_right;
				}
			}
		}
		else
		{
			p_heap[ret] = p_heap[--nstates];

			return nstates;
		}
	}

	return ret;
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
void cryptoshim::cprng::core::percolateup(
	cryptoshim_arc4random_rngstate_t const *const LIBLOCAL_RESTRICT rngstate,
	DWORD *const LIBLOCAL_RESTRICT p_heap,
	DWORD idx_this) noexcept
{
	size_t size_this, size_parent;
	DWORD idx_parent;

	for (; idx_this; idx_this = idx_parent)
	{
		idx_parent = ((1 + idx_this) >> 1) - 1;

		size_this = rngstate[idx_this].counter;
		size_parent = rngstate[idx_parent].counter;
		if (size_this <= size_parent) break;
		else if (size_this > size_parent)
		{
			std::swap<DWORD>(p_heap[idx_parent], p_heap[idx_this]);
			continue;
		}
	}
}
#ifdef _MSC_VER
#pragma warning( pop )
#endif /* _MSC_VER */

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4559 )
#endif /* _MSC_VER */
#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
void cryptoshim::cprng::core::acqsemaphore(
	cryptoshim_globalstate_t *const LIBLOCAL_RESTRICT globalstate,
	cryptoshim_arc4random_semaphore_t *const LIBLOCAL_RESTRICT semaphore,
	cryptoshim_arc4random_rngstate_t const *volatile LIBLOCAL_RESTRICT *const LIBLOCAL_RESTRICT rngstate,
	DWORD volatile *const LIBLOCAL_RESTRICT idx_state) noexcept
{
	AcquireSRWLockExclusive(&semaphore->lock_semaphore);
	if (CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_SATURATION & semaphore->mask_semaphore)
	{
		semaphore->mask_semaphore |= CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_COND_NONSATURATION;
		do
		{
			SleepConditionVariableSRW(&semaphore->cond_semaphore_nonsaturation, &semaphore->lock_semaphore, INFINITE, 0);
			AcquireSRWLockExclusive(&semaphore->lock_semaphore);
		} while (CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_SATURATION & semaphore->mask_semaphore);
	}
	if (globalstate->arc4random.mem.nstates == ++(semaphore->counter_semaphore))
	{
		semaphore->mask_semaphore |= CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_SATURATION;
	}
	if (CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_INPROGRESS & semaphore->mask_semaphore)
	{
		semaphore->counter_semaphore_queue++;
		do
		{
			SleepConditionVariableSRW(&semaphore->cond_semaphore_notinprogress, &semaphore->lock_semaphore, INFINITE, 0);
			AcquireSRWLockExclusive(&semaphore->lock_semaphore);
		} while (CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_INPROGRESS & semaphore->mask_semaphore);
	}
	semaphore->mask_semaphore |= CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_INPROGRESS;
	ReleaseSRWLockExclusive(&semaphore->lock_semaphore);

	{
		DWORD local_idx_state;
		{
			DWORD counter_heap = semaphore->counter_heap--;
			DWORD height = cryptoshim::cprng::core::getheapheight(counter_heap);
			{
				DWORD *const LIBLOCAL_RESTRICT p_heap = globalstate->arc4random.aux.semaphore.implicitheap.p_ent;
				DWORD idx_tail, idx_heap;

				local_idx_state = p_heap[0];
				idx_heap = cryptoshim::cprng::core::percolatedown(globalstate->arc4random.mem.p_ent, p_heap, counter_heap, height);
				idx_tail = --counter_heap;
				if (idx_heap != idx_tail)
				{
					std::swap<DWORD>(p_heap[idx_tail], p_heap[idx_heap]);
					cryptoshim::cprng::core::percolateup(globalstate->arc4random.mem.p_ent, p_heap, idx_heap);
				}
			}
		}
		*idx_state = local_idx_state;
		*rngstate = globalstate->arc4random.mem.p_ent + local_idx_state;
	}

	AcquireSRWLockExclusive(&semaphore->lock_semaphore);
	if (semaphore->counter_semaphore_queue)
	{
		semaphore->counter_semaphore_queue--;
		WakeConditionVariable(&semaphore->cond_semaphore_notinprogress);
	}
	else
	{
		semaphore->mask_semaphore &= ~CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_INPROGRESS;
	}
	ReleaseSRWLockExclusive(&semaphore->lock_semaphore);

	return;
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
void cryptoshim::cprng::core::relsemaphore(
	cryptoshim_globalstate_t *const LIBLOCAL_RESTRICT globalstate,
	cryptoshim_arc4random_semaphore_t *const LIBLOCAL_RESTRICT semaphore,
	DWORD *const LIBLOCAL_RESTRICT p_heap,
	const DWORD key_state) noexcept
{
	AcquireSRWLockExclusive(&semaphore->lock_semaphore);
	if (CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_INPROGRESS & semaphore->mask_semaphore)
	{
		semaphore->counter_semaphore_queue++;
		do
		{
			SleepConditionVariableSRW(&semaphore->cond_semaphore_notinprogress, &semaphore->lock_semaphore, INFINITE, 0);
			AcquireSRWLockExclusive(&semaphore->lock_semaphore);
		} while (CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_INPROGRESS & semaphore->mask_semaphore);
	}
	semaphore->mask_semaphore |= CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_INPROGRESS;
	ReleaseSRWLockExclusive(&semaphore->lock_semaphore);

	{
		DWORD idx_this = semaphore->counter_heap++;

		p_heap[--idx_this] = key_state;
		cryptoshim::cprng::core::percolateup(globalstate->arc4random.mem.p_ent, p_heap, idx_this);
	}

	AcquireSRWLockExclusive(&semaphore->lock_semaphore);
	semaphore->mask_semaphore &= ~CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_SATURATION;
	semaphore->counter_semaphore--;
	if (semaphore->counter_semaphore_queue)
	{
		semaphore->counter_semaphore_queue--;
		WakeConditionVariable(&semaphore->cond_semaphore_notinprogress);
	}
	else
	{
		semaphore->mask_semaphore &= ~CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_INPROGRESS;
		if (CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_COND_NONSATURATION & semaphore->mask_semaphore)
		{
			semaphore->mask_semaphore &= ~CRYPTOSHIM_ARC4RANDOM_SEMAPHORE_MASK_COND_NONSATURATION;
			WakeAllConditionVariable(&semaphore->cond_semaphore_nonsaturation);
		}
	}
	ReleaseSRWLockExclusive(&semaphore->lock_semaphore);
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
void cryptoshim::cprng::core::acqmutex(
	SRWLOCK *const LIBLOCAL_RESTRICT lock,
	CONDITION_VARIABLE *const LIBLOCAL_RESTRICT cond_nonsaturation,
	unsigned char *const LIBLOCAL_RESTRICT mask) noexcept
{
	AcquireSRWLockExclusive(lock);
	if (CRYPTOSHIM_ARC4RANDOM_MUTEX_MASK_SATURATION & *mask)
	{
		*mask |= CRYPTOSHIM_ARC4RANDOM_MUTEX_MASK_COND_NONSATURATION;
		do
		{
			SleepConditionVariableSRW(cond_nonsaturation, lock, INFINITE, 0);
			AcquireSRWLockExclusive(lock);
		} while (CRYPTOSHIM_ARC4RANDOM_MUTEX_MASK_SATURATION & *mask);
	}
	*mask |= CRYPTOSHIM_ARC4RANDOM_MUTEX_MASK_SATURATION;
	ReleaseSRWLockExclusive(lock);
}

#ifdef _MSC_VER
__declspec(noalias)
#endif /* _MSC_VER */
void cryptoshim::cprng::core::relmutex(
	SRWLOCK *const LIBLOCAL_RESTRICT lock,
	CONDITION_VARIABLE *const LIBLOCAL_RESTRICT cond_nonsaturation,
	unsigned char *const LIBLOCAL_RESTRICT mask) noexcept
{
	AcquireSRWLockExclusive(lock);
	*mask &= ~CRYPTOSHIM_ARC4RANDOM_MUTEX_MASK_SATURATION;
	if (CRYPTOSHIM_ARC4RANDOM_MUTEX_MASK_COND_NONSATURATION & *mask)
	{
		*mask &= ~CRYPTOSHIM_ARC4RANDOM_MUTEX_MASK_COND_NONSATURATION;
		WakeAllConditionVariable(cond_nonsaturation);
	}
	ReleaseSRWLockExclusive(lock);
}
#ifdef _MSC_VER
#pragma warning( pop )
#endif /* _MSC_VER */
