CC = cl /TC
CXX = cl /TP

## BEGIN: CFLAGS
!ifdef CFLAGS
CFLAGS = $(CFLAGS) /nologo
!else
CFLAGS = /nologo
!endif
CFLAGS = $(CFLAGS) /GS
CFLAGS = $(CFLAGS) /fp:strict
CFLAGS = $(CFLAGS) /volatile:iso
CFLAGS = $(CFLAGS) /GT
CFLAGS = $(CFLAGS) /arch:AVX
CFLAGS = $(CFLAGS) /Iinc
CFLAGS = $(CFLAGS) /Zc:forScope,wchar_t,auto,rvalueCast
CFLAGS = $(CFLAGS) /sdl
CFLAGS = $(CFLAGS) /Wall /WX
!ifdef CXXFLAGS
CXXFLAGS = $(CXXFLAGS) $(CFLAGS)
!else
CXXFLAGS = $(CFLAGS)
!endif
CEFLAGS = /EHa- /EHs- /EHc-
CXXEFLAGS = /EHac
## END: CFLAGS

RTCFLAGS = /RTCc /RTCs /RTCu
CXXRTCFLAGS = /RTCs /RTCu

OFLAGS = /O2

## BEGIN: LINK
LINK = link
LINKFLAGS = /nologo
LINKFLAGS = $(LINKFLAGS) /dynamicbase /nxcompat
## END: LINK

## BEGIN: _WIN64
!ifdef _WIN64
LINKFLAGS = $(LINKFLAGS) /highentropyva
!endif
## END: _WIN64

## BEGIN: NDEBUG
!ifdef NDEBUG
CFLAGS = $(CFLAGS) /DNDEBUG
!else
CFLAGS = $(CFLAGS) /Zi
LINKFLAGS = $(LINKFLAGS) /debug
!endif
## END: NDEBUG
