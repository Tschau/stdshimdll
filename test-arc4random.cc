#ifdef  _MSC_VER
#pragma warning(disable : 4101 4189 4255 4668 4710 4820 )
#pragma setlocale( "C" )
#endif

#include <windows.h>

#include <sstream>
#include <iomanip>
#include <system_error>

#include <cstdlib>
#include <cstdio>
#include <cstdint>

#include <fcntl.h>
#include <io.h>

#include "cryptoshim.h"

int wmain(void) noexcept
{
	std::ostringstream stream;

	uint32_t m, n = 0, l = 0, i, s;
	void *p = NULL;

	std::ios_base::sync_with_stdio(false);

	m = 1 << 10;
	stream << std::setfill('0');
	stream << std::hex;
	stream << std::boolalpha;
	for (i = 0, s = 0; i < m; i++)
	{
		try
		{
			n = arc4random();
		}
		catch (const std::system_error& e)
		{
			stream << e.what() << '\n' << std::ends;
			puts(std::move(stream.str()).data());
			stream.seekp(0);
			return EXIT_FAILURE;
		}
		try
		{
			l = arc4random_uniform(n);
		}
		catch (const std::system_error& e)
		{
			stream << e.what() << '\n' << std::ends;
			puts(std::move(stream.str()).data());
			stream.seekp(0);
			return EXIT_FAILURE;
		}
		stream << "n: 0x" << std::setw(8);
		stream << n << ' ';
		stream << "l: 0x" << std::setw(8);
		stream << l << ' ';
		stream << "b: " << (n > l);
		stream << std::ends;
		puts(std::move(stream.str()).data());
		stream.seekp(0);
		if (n % 2) s++;
	}
	stream << "s: " << std::dec << s;
	stream << std::ends;
	puts(std::move(stream.str()).data());
	stream.seekp(0);
	try
	{
		arc4random_buf(&p, sizeof(void*));
	}
	catch (const std::system_error& e)
	{
		stream << e.what() << '\n' << std::ends;
		puts(std::move(stream.str()).data());
		stream.seekp(0);
		return EXIT_FAILURE;
	}
	stream << "p: " << "0x" << std::hex << p;
	stream << std::ends;
	puts(std::move(stream.str()).data());
	stream.seekp(0);

	return EXIT_SUCCESS;
}
