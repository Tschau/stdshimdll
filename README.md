For now, stdshimdll consists of two DLLs: stdioshim.dll and cryptoshim.dll.

stdioshim.dll currently provides {,f}{,w}printf() series of functions to output
Unicode to Windows Console without having to call _setmode(). The console buffer
does not need to be flushed, nor is the console code page affected. The series
of functions should behave in the same way as their MSFT counterpart in the
event of redirection to a file. EILSEQ will be set if the format string includes
illegal sequences.

This implementation conforms to C11's specification and limits the number of
output characters, or code units at 4095, discounting the terminating null. This
implementation also expects the size of the format string to top at 65,535
bytes, including the terminating null. This implementation outputs to Windows
Console Unicode, and ONLY Unicode, or UTF-16, with source encoding being UTF-8
and UTF-16, and ONLY the two of them.

cryptoshim.dll currently implements part of SHA256 hashing functions from
OpenBSD, and the arc4random*() series of functions. Effort is made to take
full advantage of Windows NT's DllMain() and keep initialization and
finalization from the bulk of arc4random*() functions.

Unlike libressl's arc4random*() implementation, cryptoshim.dll's takes into full
consideration the possibility of fork() within Windows NT, that is
RtlCloneUserProcess(), or {Zw,Nt}CreateProcess() with Section valued at NULL. As
such, CreateFileMapping() is used in place of Theo de Raadt's VirtualAlloc(),
which, as a result, is able to give copy-on-write resilience.

With reference to Taylor Campbell's work on NetBSD's own implementation of ARND,
this implementation attempts to assign CPRNG state per thread instead of per
process, though per-process state is saved as a fallback option. Further, this
implementation depends on SRWLOCK for the mutex of the per-process state,
instead of the kernel-bound mutex, as is present in libressl's implementation
at the time.

It is to be noted, however, that this implementation depends a lot on Windows
NT's native interface, as in the WIN32 API, and should, in many cases, be
considered a wrapper.

This particular arc4random*() implementation processes CPRNG state in
chunks of uint32_t, though the old byte-based behavior can be restored.

To populate the source tree with proper symbolic links, run mklinkcmd.cmd. Also,
do remember to run vcvars*.bat first.

Credits and references to respective authors shall be found in individual files.
