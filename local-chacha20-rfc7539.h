/*_
 * vide Section 2.2 of RFC7539
 *
 * local-chacha20-rfc7539.h is meant to be embedded as
 * #include "local-chacha20-rfc7539.h"
 * for every consumer of the file.
 *
 * limits.h,
 * stdlib.h, and
 * stdint.h is required.
 */

/*_
 * Intrinsics from BOTH Intel and ARM
 * _rotl
 *
 * Intrinsics from Intel, Available
 * <local-chacha20-rfc7539.h>
 * _mm_load_si128 : SSE2
 * _mm_store_si128 : SSE2
 * _mm_setr_epi32 : SSE2
 * _mm_insert_epi32 : SSE4.1
 * _mm_add_epi32 : SSE2
 * _mm_xor_si128 : SSE2
 * _mm_or_si128 : SSE2
 * _mm shuffle_epi8 : SSSE3
 * _mm_slli_epi32 : SSE2
 * _mm_srli_epi32 : SSE2
 * _mm_setzero_si128 : SSE2
 *
 * Intrinsics from Intel, Unavailable
 * _mm_rol_epi32 : AVX512VL + AVX512F
 * _mm_ror_epi32 : AVX512VL + AVX512F
 */

#define LOCAL_CHACHA_C0 0x61707865
#define LOCAL_CHACHA_C1 0x3320646e
#define LOCAL_CHACHA_C2 0x79622d32
#define LOCAL_CHACHA_C3 0x6b206574
#define LOCAL_CHACHA_CROUND_RFC7539 20
#define LOCAL_CHACHA_CITERATION_RFC7539 (LOCAL_CHACHA_CROUND_RFC7539 / 2)

/*
typedef struct {
	uint32_t e[16];
} local_chacha_state_t;
*/

#ifdef __AVX__
/*
typedef struct {
	__m128i e[4];
} local_chacha_state_vec_t;
*/
#endif /* __AVX__ */

/* chacha_key256_t is 256-bit-ONLY! */
/*
typedef struct {
	uint8_t e[32];
} local_chacha_key256_t;
*/

/*_
 * blk32 types and functions depend on machine-endianness.
 * DO NOT PRESUME LITTLE-ENDIANNESS.
 */
/*
typedef struct {
	uint32_t e[8];
} local_chacha_key256_blk32_t;
*/

/* chacha_nonce96_t is 96-bit-ONLY! */
/*
typedef struct {
	uint8_t e[12];
} local_chacha_nonce96_t;
*/

/*
typedef struct {
	uint32_t e[3];
} local_chacha_nonce96_blk32_t;
*/

/*
typedef uint32_t local_chacha_cblock32_t;
*/

/* chacha_keyblk_t is 512-bit-ONLY! */
/*
typedef struct {
	unsigned char e[64];
} local_chacha_keyblk_t;
*/

/*
typedef struct {
	uint32_t e[16];
} local_chacha_keyblk_blk32_t;
*/

#ifdef _MSC_VER
#pragma intrinsic(_rotl)
#define LOCAL_CHACHA_ROTL32 _rotl
#else
/*
 * "unsigned long" is guaranteed as 4-byte
 * per https://msdn.microsoft.com/en-us/library/s3f49ktz.aspx .
 *
 * _lrotl is defined
 * at https://msdn.microsoft.com/en-us/library/a0w705h5.aspx .
 */
static __inline uint32_t
local_rotl32(uint32_t x, uint32_t n)
{
	uint32_t n_min, mask = 0;
	n_min = n ^ (n >> 5 << 5);
	mask = (CHAR_BIT * sizeof(uint32_t));
	return (x << n_min | x >> (mask - n_min));
}
#define LOCAL_CHACHA_ROTL32 local_rotl32
#endif

#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
static __inline void
local_chacha_deserialize(
	/* _Inout_updates_bytes_(sizeof(uint32_t)) */ uint32_t *const __restrict dst_le,
	/* _In_reads_(4) */ unsigned char *const __restrict a)
{
	/* NULL == dst_le is NOT tested. */
#if 8 == CHAR_BIT
	CopyMemory(dst_le, a, 4);
	/*
	*dst_le = (uint32_t)a[0] | (uint32_t)a[1] << 8 | (uint32_t)a[2] << 16 | (uint32_t)a[3] << 24;
	*/
#else
	uint32_t v[4];

	v[0] = 0xff & a[0];
	v[1] = (0xff & a[1]) << 8;
	v[2] = (0xff & a[2]) << 16;
	v[3] = (0xff & a[3]) << 24;

	*dst_le = v[0] | v[1] | v[2] | v[3];
#endif /* 8 == CHAR_BIT */
}

#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
static __inline void
local_chacha_rfc7539_initstate(
	/* _Inout_updates_(16) */ uint32_t *const __restrict cipherstate,
	/* _In_reads_(32) */ const unsigned char *const __restrict key,
	/* _In_reads_(12) */ const unsigned char *const __restrict nonce,
	_In_ const uint32_t cblock)
{
	long i, j, k;
	static const uint32_t v[4] = { LOCAL_CHACHA_C0, LOCAL_CHACHA_C1, LOCAL_CHACHA_C2, LOCAL_CHACHA_C3 };
	unsigned char a[4];

	/*
	if (s == NULL || key == NULL || nonce == NULL) { errno = EINVAL; return; }
	*/

	for (i = 0; i < 4; i++) cipherstate[i] = v[i];

	for (j = 0; i < 12; i++, j += 4) {
		for (k = 0; k < 4; k++) a[k] = key[k | j];
		local_chacha_deserialize(cipherstate + i, a);
	} /* #(2, 1)--#(3, 4), or #4--#11 */

	/* i == 12 holds at this point. */
	cipherstate[i++] = cblock; /* #(4, 1), or #12 */

	for (j = 0; i < 16; i++, j += 4) {
		for (k = 0; k < 4; k++) a[k] = nonce[k | j];
		local_chacha_deserialize(cipherstate + i, a);
	} /* #(4, 2)--#(4, 4), or #13--#15 */
}

#ifdef __AVX__
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
static __inline void
local_chacha_rfc7539_initstate_v(
	/* _Inout_updates_(4) */ __m128i *const __restrict cipherstate,
	/* _In_reads_(32) */ const unsigned char *const __restrict key,
	/* _In_reads_(12) */ const unsigned char *const __restrict nonce,
	_In_ const uint32_t cblock)
{
	static const union {
		uint32_t blk32[4];
		__m128i blk128;
	} v = { { LOCAL_CHACHA_C0, LOCAL_CHACHA_C1, LOCAL_CHACHA_C2, LOCAL_CHACHA_C3 } };

	/*
	if (s == NULL || key == NULL || nonce == NULL) { errno = EINVAL; return; }
	*/

	_mm_store_si128(cipherstate, v.blk128);

	_mm_store_si128((1) + cipherstate, _mm_setr_epi8(
		((char*)key)[0x00], ((char*)key)[0x01], ((char*)key)[0x02], ((char*)key)[0x03],
		((char*)key)[0x04], ((char*)key)[0x05], ((char*)key)[0x06], ((char*)key)[0x07],
		((char*)key)[0x08], ((char*)key)[0x09], ((char*)key)[0x0a], ((char*)key)[0x0b],
		((char*)key)[0x0c], ((char*)key)[0x0d], ((char*)key)[0x0e], ((char*)key)[0x0f]));

	_mm_store_si128((2) + cipherstate, _mm_setr_epi8(
		((char*)key)[0x10], ((char*)key)[0x11], ((char*)key)[0x12], ((char*)key)[0x13],
		((char*)key)[0x14], ((char*)key)[0x15], ((char*)key)[0x16], ((char*)key)[0x17],
		((char*)key)[0x18], ((char*)key)[0x19], ((char*)key)[0x1a], ((char*)key)[0x1b],
		((char*)key)[0x1c], ((char*)key)[0x1d], ((char*)key)[0x1e], ((char*)key)[0x1f]));

	_mm_store_si128((3) + cipherstate, _mm_setr_epi8(
#ifndef __cplusplus
		0xff & cblock,
		0xff & cblock >> 8,
		0xff & cblock >> 16,
		0xff & cblock >> 24,
#else /* __cplusplus */
		static_cast<char>(0xff & cblock),
		static_cast<char>(0xff & cblock >> 8),
		static_cast<char>(0xff & cblock >> 16),
		static_cast<char>(0xff & cblock >> 24),
#endif /* __cplusplus */
		((char*)nonce)[0x0], ((char*)nonce)[0x1], ((char*)nonce)[0x2], ((char*)nonce)[0x3],
		((char*)nonce)[0x4], ((char*)nonce)[0x5], ((char*)nonce)[0x6], ((char*)nonce)[0x7],
		((char*)nonce)[0x8], ((char*)nonce)[0x9], ((char*)nonce)[0xa], ((char*)nonce)[0xb]));
}
#endif /* __AVX__ */

/* Little-Endianness is not guaranteed. */
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
static __inline void
local_chacha_rfc7539_blk32_initstate(
	/* _Inout_updates_(16) */ uint32_t *const __restrict cipherstate,
	/*_
	 * The ensuant results from MSFT's retarded compiler cl,
	 * which fails to support declarations such as "uint32_t a[static 8]",
	 * or "uint32_t a[restrict 8]", or a combination thereof.
	 */
	/* _In_reads(8)_ */ const uint32_t *const __restrict key,
	/* _In_reads(3)_ */ const uint32_t *const __restrict nonce,
	/* _In_ */ const uint32_t cblock)
{
	long i = 0, j;

	/*
	if (s == NULL || key == NULL || nonce == NULL) { errno = EINVAL; return; }
	*/

	cipherstate[0] = LOCAL_CHACHA_C0; /* #(1, 1), or #0 */
	cipherstate[1] = LOCAL_CHACHA_C1; /* #(1, 2), or #1 */
	cipherstate[2] = LOCAL_CHACHA_C2; /* #(1, 3), or #2 */
	cipherstate[3] = LOCAL_CHACHA_C3; /* #(1, 4), or #3 */

	for (i = 4, j = 0; i < 12; i++, j++) {
		cipherstate[i] = key[j];
	} /* #(2, 1)--#(3, 4), or #4--#11 */

	cipherstate[i++] = cblock; /* #(4, 1), or #12 */

	for (j = 0; i < 16; i++, j++) {
		cipherstate[i] = nonce[j];
	} /* #(4, 2)--#(4, 4), or #13--#15 */
}

#ifdef __AVX__
/* Little-Endianness is not guaranteed. */
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
static __inline void
local_chacha_rfc7539_blk32_initstate_v(
	/* _Inout_updates_(4) */ __m128i *const __restrict cipherstate,
	/* _In_reads_(8) */ const uint32_t *const __restrict key,
	/* _In_reads_(3) */ const uint32_t *const __restrict nonce,
	/* _In_ */ const uint32_t cblock)
{
	static const union {
		uint32_t blk32[4];
		__m128i blk128;
	} v32 = { { LOCAL_CHACHA_C0, LOCAL_CHACHA_C1, LOCAL_CHACHA_C2, LOCAL_CHACHA_C3 } };

	/*
	if (s == NULL || key == NULL || nonce == NULL) { errno = EINVAL; return; }
	*/

	_mm_store_si128(cipherstate, v32.blk128);

	_mm_store_si128(1 + cipherstate, _mm_setr_epi32(
#ifndef __cplusplus
		key[0], key[1], key[2], key[3]
#else
		static_cast<int>(key[0]), static_cast<int>(key[1]), static_cast<int>(key[2]), static_cast<int>(key[3])
#endif /* __cplusplus */
		));

	_mm_store_si128(2 + cipherstate, _mm_setr_epi32(
#ifndef __cplusplus
		key[4], key[5], key[6], key[7]
#else
		static_cast<int>(key[4]), static_cast<int>(key[5]), static_cast<int>(key[6]), static_cast<int>(key[7])
#endif /* __cplusplus */
		));

	_mm_store_si128(3 + cipherstate, _mm_setr_epi32(
#ifndef __cplusplus
		cblock, nonce[0], nonce[1], nonce[2]
#else
		static_cast<int>(cblock), static_cast<int>(nonce[0]), static_cast<int>(nonce[1]), static_cast<int>(nonce[2])
#endif /* __cplusplus */
		));
}

/* Yes, block counter is user's responsibility. */
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
static __inline void
local_chacha_rfc7539_blk128_initstate_v(
	/* _Inout_updates_(4) */ __m128i *const __restrict cipherstate,
	/* _In_reads_(3) */ const __m128i *const __restrict v)
{
	static const union {
		uint32_t blk32[4];
		__m128i blk128;
	} v128 = { { LOCAL_CHACHA_C0, LOCAL_CHACHA_C1, LOCAL_CHACHA_C2, LOCAL_CHACHA_C3 } };
	long i;

	/*
	if (s == NULL || key == NULL || nonce == NULL) { errno = EINVAL; return; }
	*/

	_mm_store_si128(cipherstate, v128.blk128);
	for (i = 0; i < 3; i++)
	{
		_mm_store_si128((1 + i) + cipherstate, _mm_load_si128(i + v));
	}
	/* Charge is user's to set cblock. */
	/*
	_mm_store_si128(3 + cipherstate, _mm_shuffle_epi32(
		_mm_load_si128(3 + cipherstate), 0x27
		));
	*((uint32_t*)(3 + cipherstate)) = cblock;
	*/
}

#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
static __inline void
local_chacha_rfc7539_blk256_initstate_v(
	__m256i const kh_ctr_nonce,
	__m128i const kl,
	/* _Inout_updates_(2) */ __m256i *const __restrict cipherstate)
{
	static const union {
		uint32_t blk32[4];
		__m128i blk128;
	} v128 = { { LOCAL_CHACHA_C0, LOCAL_CHACHA_C1, LOCAL_CHACHA_C2, LOCAL_CHACHA_C3 } };

	_mm256_store_si256(1 + cipherstate, kh_ctr_nonce);
	_mm256_store_si256(cipherstate, _mm256_setr_m128i(v128.blk128, kl));
}
/*local_chacha_rfc7539_blk256_initstate_v(&buf->blk256, &buf->blk128, cipherstate->blk128);*/
#endif /* __AVX__ */

/*
 * {a, b, c, d} defines a column/diagonal vector.
 *
 * vide Section 2.2 of RFC7539
 */
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
#ifdef _MSC_VER
#if ( _MSC_VER < 1900 )
static __inline void
#else
static inline void
#endif /* ( _MSC_VER < 1900 ) */
#else
static inline void
#endif /* _MSC_VER */
local_chacha_qround(
	/* _Inout_updates(16) */ uint32_t *const __restrict cipherstate,
	/* _In_ */ const long a,
	/* _In_ */ const long b,
	/* _In_ */ const long c,
	/* _In_ */ const long d)
{
	/*
	if (NULL == s) { errno = EINVAL; return; }
	else if (NULL == s->e) { errno = EINVAL; return; }
	*/
	
	/* #1 */
	cipherstate[a] += cipherstate[b];
	cipherstate[d] ^= cipherstate[a];
	cipherstate[d] = LOCAL_CHACHA_ROTL32(cipherstate[d], 16);
	
	/* #2 */
	cipherstate[c] += cipherstate[d];
	cipherstate[b] ^= cipherstate[c];
	cipherstate[b] = LOCAL_CHACHA_ROTL32(cipherstate[b], 12);

	/* #3 */
	cipherstate[a] += cipherstate[b];
	cipherstate[d] ^= cipherstate[a];
	cipherstate[d] = LOCAL_CHACHA_ROTL32(cipherstate[d], 8);

	/* #4 */
	cipherstate[c] += cipherstate[d];
	cipherstate[b] ^= cipherstate[c];
	cipherstate[b] = LOCAL_CHACHA_ROTL32(cipherstate[b], 7);
}

#ifdef __AVX__
#ifdef LOCAL_CHACHA_QROUND_VEC128_CORE
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
static void
local_chacha_qround_vec128_core(
	/* _Inout_updates_(4) */ __m128i *const __restrict cipherstate)
{
	/*_
	 * The ensuant results from the predominant dearth of
	 * AVX-512 instructions amongst x86 processors, which
	 * offers
	 * __m128i _mm_rol_epi32(__m128i a, int imm8) and
	 * __m128i _mm_ror_epi32(__m128i a, const int imm8) ,
	 * for which _mm_shuffle_epi8 is resorted to.
	 */
	static const union
	{
		unsigned char i8[16];
		__m128i i128;
	} u = { { 2, 3, 0, 1, 6, 7, 4, 5, 10, 11, 8, 9, 14, 15, 12, 13 } };
	static const union
	{
		unsigned char i8[16];
		__m128i i128;
	} v = { { 3, 0, 1, 2, 7, 4, 5, 6, 11, 8, 9, 10, 15, 12, 13, 14 } };
	register __m128i xmm0;
	register __m128i xmm1;

	/*
	if (NULL == s) { errno = EINVAL; return; }
	*/

	/* #1 */
	/*cipherstate[0] = _mm_add_epi32(cipherstate[0], cipherstate[1]);
	cipherstate[3] = _mm_xor_si128(cipherstate[3], cipherstate[0]);
	cipherstate[3] = _mm_rol_epi32(cipherstate[3], 16);*/
	xmm0 = _mm_load_si128(cipherstate);
	xmm1 = _mm_load_si128(1 + cipherstate);
	xmm0 = _mm_add_epi32(xmm0, xmm1);
	_mm_store_si128(cipherstate, xmm0);

	xmm1 = _mm_load_si128(3 + cipherstate);
	xmm1 = _mm_xor_si128(xmm1, xmm0);
	xmm1 = _mm_shuffle_epi8(xmm1, u.i128);
	_mm_store_si128(3 + cipherstate, xmm1);

	/* #2 */
	/*cipherstate[2] = _mm_add_epi32(cipherstate[2], cipherstate[3]);
	cipherstate[1] = _mm_xor_si128(cipherstate[1], cipherstate[2]);
	cipherstate[1] = _mm_rol_epi32(cipherstate[1], 12);*/
	xmm0 = _mm_load_si128(2 + cipherstate);
	xmm1 = _mm_load_si128(3 + cipherstate);
	xmm0 = _mm_add_epi32(xmm0, xmm1);
	_mm_store_si128(2 + cipherstate, xmm0);

	xmm1 = _mm_load_si128(1 + cipherstate);
	xmm1 = _mm_xor_si128(xmm1, xmm0);
	xmm0 = _mm_slli_epi32(xmm1, 12);
	xmm1 = _mm_srli_epi32(xmm1, 20);
	xmm1 = _mm_or_si128(xmm0, xmm1);
	_mm_store_si128(1 + cipherstate, xmm1);

	/* #3 */
	/*cipherstate[0] = _mm_add_epi32(cipherstate[0], cipherstate[1]);
	cipherstate[3] = _mm_xor_si128(cipherstate[3], cipherstate[0]);
	cipherstate[3] = _mm_rol_epi32(cipherstate[3], 8);*/
	xmm0 = _mm_load_si128(cipherstate);
	xmm1 = _mm_load_si128(1 + cipherstate);
	xmm0 = _mm_add_epi32(xmm0, xmm1);
	_mm_store_si128(cipherstate, xmm0);

	xmm1 = _mm_load_si128(3 + cipherstate);
	xmm1 = _mm_xor_si128(xmm1, xmm0);
	xmm1 = _mm_shuffle_epi8(xmm1, v.i128);
	_mm_store_si128(3 + cipherstate, xmm1);

	/* #4 */
	/*cipherstate[2] = _mm_add_epi32(cipherstate[2], cipherstate[3]);
	cipherstate[1] = _mm_xor_si128(cipherstate[1], cipherstate[2]);
	cipherstate[1] = _mm_rol_epi32(cipherstate[1], 7);*/
	xmm0 = _mm_load_si128(2 + cipherstate);
	xmm1 = _mm_load_si128(3 + cipherstate);
	xmm0 = _mm_add_epi32(xmm0, xmm1);
	_mm_store_si128(2 + cipherstate, xmm0);

	xmm1 = _mm_load_si128(1 + cipherstate);
	xmm1 = _mm_xor_si128(xmm1, xmm0);
	xmm0 = _mm_slli_epi32(xmm1, 7);
	xmm1 = _mm_srli_epi32(xmm1, 25);
	xmm1 = _mm_or_si128(xmm0, xmm1);
	_mm_store_si128(1 + cipherstate, xmm1);
}
#endif /* LOCAL_CHACHA_QROUND_VEC128_CORE */
#endif /* __AVX__ */

#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
#ifdef _MSC_VER
#if ( _MSC_VER < 1900 )
static __inline void
#else
static inline void
#endif /* ( _MSC_VER < 1900 ) */
#else
static inline void
#endif /* _MSC_VER */
local_chacha20_rfc7539_getblock_core(
	/* _Inout_updates_(16) */ uint32_t *const __restrict s)
{
	long i;

	/*
	if (NULL == s) { errno = EINVAL; return; }
	else if (NULL == s->e) { errno = EINVAL; return; }
	*/

	for (i = 0; i < LOCAL_CHACHA_CITERATION_RFC7539; i++)
	{
		/* BEGIN: Columnal Rounds */
		local_chacha_qround(s, 0x0, 0x4, 0x8, 0xc);
		local_chacha_qround(s, 0x1, 0x5, 0x9, 0xd);
		local_chacha_qround(s, 0x2, 0x6, 0xa, 0xe);
		local_chacha_qround(s, 0x3, 0x7, 0xb, 0xf);
		/* END: Columnal Rounds */
		/* BEGIN: Diagonal Rounds */
		local_chacha_qround(s, 0x0, 0x5, 0xa, 0xf);
		local_chacha_qround(s, 0x1, 0x6, 0xb, 0xc);
		local_chacha_qround(s, 0x2, 0x7, 0x8, 0xd);
		local_chacha_qround(s, 0x3, 0x4, 0x9, 0xe);
		/* END: Diagonal Rounds */
	}
}

#ifdef LOCAL_CHACHA20_RFC7539_GETBLOCK_VEC128_CORE
#ifdef __AVX__
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
static void
local_chacha20_rfc7539_getblock_vec128_core(
	/* _Inout_updates_(4) */ __m128i *const __restrict cipherstate)
{
	long i;

	/*
	if (NULL == s) { errno = EINVAL; return; }
	else if (NULL == s->e) { errno = EINVAL; return; }
	*/

	for (i = 0; i < LOCAL_CHACHA_CITERATION_RFC7539; i++)
	{
		/* BEGIN: Columnal Rounds */
		local_chacha_qround_vec128_core(cipherstate);
		_mm_store_si128(1 + cipherstate, _mm_shuffle_epi32(_mm_load_si128(1 + cipherstate), 0x39));
		_mm_store_si128(2 + cipherstate, _mm_shuffle_epi32(_mm_load_si128(2 + cipherstate), 0x4e));
		_mm_store_si128(3 + cipherstate, _mm_shuffle_epi32(_mm_load_si128(3 + cipherstate), 0x93));
		/* END: Columnal Rounds */
		/* BEGIN: Diagonal Rounds */
		local_chacha_qround_vec128_core(cipherstate);
		_mm_store_si128(1 + cipherstate, _mm_shuffle_epi32(_mm_load_si128(1 + cipherstate), 0x93));
		_mm_store_si128(2 + cipherstate, _mm_shuffle_epi32(_mm_load_si128(2 + cipherstate), 0x4e));
		_mm_store_si128(3 + cipherstate, _mm_shuffle_epi32(_mm_load_si128(3 + cipherstate), 0x39));
		/* END: Diagonal Rounds */
	}
}
#endif /* __AVX__ */
#endif /* LOCAL_CHACHA20_RFC7539_GETBLOCK_VEC_CORE */

/* vide Section 2.3 of RFC7539 */
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
static __inline void
local_chacha20_rfc7539_getblock(
	/* _Out_writes_all_(64) */ unsigned char *const __restrict keyblk,
	/* _In_reads_(16) */ const uint32_t *const __restrict cipherstate)
{
	uint32_t local_cipherstate[16];
	long i;

	/*
	if (NULL == s || NULL == keyblk) { errno = EINVAL; return; }
	else if (NULL == s->e) { errno = EINVAL; return; }
	*/

	for (i = 0; i < 16; i++) local_cipherstate[i] = cipherstate[i];

	local_chacha20_rfc7539_getblock_core(local_cipherstate);

	for (i = 0; i < 16; i++) local_cipherstate[i] += cipherstate[i];

	/* keyblk->e is little-endian. */
	for (i = 0; i < 16; i++)
	{
		keyblk[i << 2] = local_cipherstate[i] & 0xff;
		keyblk[1 | (i << 2)] = local_cipherstate[i] >> 8 & 0xff;
		keyblk[2 | (i << 2)] = local_cipherstate[i] >> 16 & 0xff;
		keyblk[3 | (i << 2)] = local_cipherstate[i] >> 24 & 0xff;
		local_cipherstate[i] = 0;
	}
}

#ifdef __AVX__
/* vide Section 2.3 of RFC7539 */
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
static __inline void
local_chacha20_rfc7539_getblock_v(
	/* _Out_writes_all_(64) */ unsigned char *const __restrict keyblk,
	/* _Inout_updates_(4) */ const __m128i *const __restrict cipherstate)
{
	__m128i local_cipherstate[4];
	union
	{
		__m128i blk128[1];
		uint32_t blk32[4];
	} v;
	long i, j;

	/*
	if (NULL == s || NULL == keyblk) { errno = EINVAL; return; }
	else if (NULL == s->e) { errno = EINVAL; return; }
	*/

	for (i = 0; i < 4; i++)
	{
		_mm_store_si128(local_cipherstate + i,
			_mm_load_si128(i + cipherstate));
	}

	local_chacha20_rfc7539_getblock_vec128_core(local_cipherstate);

	for (i = 0; i < 4; i++)
	{
		_mm_store_si128(i + local_cipherstate, _mm_add_epi32(
			_mm_load_si128(i + local_cipherstate),
			_mm_load_si128(i + cipherstate)));
	}

	/* keyblk->e is little-endian. */
	for (i = 0; i < 4; i++)
	{
		_mm_store_si128(v.blk128, _mm_load_si128(local_cipherstate + i));
		for (j = 0; j < 4; j++) {
			keyblk[(i << 4) | (j << 2)] = v.blk32[j] & 0xff;
			keyblk[1 | (i << 4) | (j << 2)] = v.blk32[j] >> 8 & 0xff;
			keyblk[2 | (i << 4) | (j << 2)] = v.blk32[j] >> 16 & 0xff;
			keyblk[3 | (i << 4) | (j << 2)] = v.blk32[j] >> 24 & 0xff;
		}
		_mm_stream_si128(i + local_cipherstate, _mm_setzero_si128());
	}
}
#endif /* __AVX__ */

#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
#ifdef _MSC_VER
#if ( _MSC_VER < 1900 )
static __inline void
#else
static inline void
#endif /* ( _MSC_VER < 1900 ) */
#else
static inline void
#endif /* _MSC_VER */
local_chacha20_rfc7539_blk32_getblock(
	/* _Out_writes_all_(16) */ uint32_t *const __restrict keyblk,
	/* _In_reads_(16) */ const uint32_t *const __restrict cipherstate)
{
	uint32_t local_cipherstate[16];
	long i;

	/*
	if (NULL == s || NULL == keyblk) { errno = EINVAL; return; }
	else if (NULL == s->e) { errno = EINVAL; return; }
	*/

	for (i = 0; i < 16; i++) local_cipherstate[16] = cipherstate[16];

	local_chacha20_rfc7539_getblock_core(local_cipherstate);

	for (i = 0; i < 16; i++) local_cipherstate[i] += cipherstate[i];

	/* keyblk[i] is little-endian for each i. */
	for (i = 0; i < 16; i++) {
		keyblk[i] = local_cipherstate[i];
		local_cipherstate[i] = 0;
	}
}

#ifdef __AVX__
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
#ifdef _MSC_VER
#if ( _MSC_VER < 1900 )
static __inline void
#else
static inline void
#endif /* ( _MSC_VER < 1900 ) */
#else
static inline void
#endif /* _MSC_VER */
local_chacha20_rfc7539_blk32_getblock_v(
	/* _Out_writes_all_(16) */ uint32_t *const __restrict keyblk,
	/* _In_reads_(4) */ const __m128i *const __restrict cipherstate)
{
	__m128i local_cipherstate[4];
	union
	{
		__m128i blk128[1];
		uint32_t blk32[4];
	} v;
	long i, j;

	/*
	if (NULL == s || NULL == keyblk) { errno = EINVAL; return; }
	else if (NULL == s->e) { errno = EINVAL; return; }
	*/

	for (i = 0; i < 4; i++)
	{
		_mm_store_si128(local_cipherstate + i,
			_mm_load_si128(i + cipherstate));
	}

	local_chacha20_rfc7539_getblock_vec128_core(local_cipherstate);

	for (i = 0; i < 4; i++)
	{
		_mm_store_si128(i + local_cipherstate, _mm_add_epi32(
			_mm_load_si128(i + local_cipherstate),
			_mm_load_si128(i + cipherstate)));
	}

	/* keyblk[i] is little-endian for each i. */
	for (i = 0; i < 4; i++)
	{
		_mm_store_si128(v.blk128, _mm_load_si128(local_cipherstate + i));
		for (j = 0; j < 4; j++)
		{
			keyblk[j | (i << 2)] = v.blk32[j];
		}
		_mm_stream_si128(i + local_cipherstate, _mm_setzero_si128());
	}
}

#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
#ifdef _MSC_VER
#if ( _MSC_VER < 1900 )
static __inline void
#else
static inline void
#endif /* ( _MSC_VER < 1900 ) */
#else
static inline void
#endif /* _MSC_VER */
local_chacha20_rfc7539_blk128_getblock_v(
	/* _Out_writes_all_(16) */ __m128i *const __restrict keyblk,
	/* _In_reads_(4) */ const __m128i *const __restrict cipherstate)
{
	__m128i local_cipherstate[4];
	long i;

	/*
	if (NULL == s || NULL == keyblk) { errno = EINVAL; return; }
	else if (NULL == s->e) { errno = EINVAL; return; }
	*/

	for (i = 0; i < 4; i++)
	{
		_mm_store_si128(local_cipherstate + i,
			_mm_load_si128(i + cipherstate));
	}

	local_chacha20_rfc7539_getblock_vec128_core(local_cipherstate);

	for (i = 0; i < 4; i++)
	{
		_mm_store_si128(i + local_cipherstate, _mm_add_epi32(
			_mm_load_si128(i + local_cipherstate),
			_mm_load_si128(i + cipherstate)));
	}

	/* keyblk[i] is little-endian for each i. */
	for (i = 0; i < 4; i++)
	{
		_mm_store_si128(i + keyblk, _mm_load_si128(i + local_cipherstate));
		_mm_stream_si128(i + local_cipherstate, _mm_setzero_si128());
	}
}

#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
#ifdef _MSC_VER
#if ( _MSC_VER < 1900 )
static __inline void
#else
static inline void
#endif /* ( _MSC_VER < 1900 ) */
#else
static inline void
#endif /* _MSC_VER */
local_chacha20_rfc7539_blk256_getblock_v(
	/* _Out_writes_all_(8) */ __m256i *const __restrict keyblk,
	/* _In_reads_(2) */ const __m256i *const __restrict cipherstate)
{
	__m256i local_cipherstate[2];
	long i;

	/*
	if (NULL == s || NULL == keyblk) { errno = EINVAL; return; }
	else if (NULL == s->e) { errno = EINVAL; return; }
	*/

	for (i = 0; i < 2; i++)
	{
		_mm256_store_si256(local_cipherstate + i,
			_mm256_load_si256(i + cipherstate));
	}

	local_chacha20_rfc7539_getblock_vec128_core((__m128i*)local_cipherstate);

	/* _mm256_add_epi32 is an AVX2 intrinsic... */
	for (i = 0; i < 4; i++)
	{
		_mm_store_si128(i + (__m128i*)local_cipherstate, _mm_add_epi32(
			_mm_load_si128(i + (__m128i*)local_cipherstate),
			_mm_load_si128(i + (__m128i*)cipherstate)));
	}

	/* keyblk[i] is little-endian for each i. */
	for (i = 0; i < 2; i++)
	{
		_mm256_store_si256(i + keyblk, _mm256_load_si256(i + local_cipherstate));
		_mm256_stream_si256(i + local_cipherstate, _mm256_setzero_si256());
	}
}
#endif /* __AVX__ */

/*_
 * ciphertext == plaintext is a possibility.
 * Perhaps, _xor_copy() and _xor_move()
 * are more preferrable?
 */
#ifdef _MSC_VER
__declspec( noalias )
#endif /* _MSC_VER */
static __inline void
local_chacha20_rfc7539_xor(
	/* _Out_writes_bytes_(len) */ unsigned char *const ciphertext,
	/* _Inout_updates_bytes_(len) */ unsigned char *const plaintext,
	/* _In_ */ size_t len,
	/* _In_reads_(32) */ unsigned char *__restrict key,
	/* _In_reads_(12) */ unsigned char *__restrict nonce,
	/* _In_ */ const uint32_t cblock)
{
#ifdef __AVX__
	union {
		__m128i blk128[4];
		uint32_t blk32[16];
	} cipherstate;
#endif /* __AVX__ */
	size_t len_q;
	size_t index;
#ifndef __AVX__
	uint32_t cipherstate[16];
#endif /* __AVX__ */
	long i;
	unsigned char keyblk[64];
	unsigned char len_r;

	/*
	if (ciphertext == NULL || plaintext == NULL || len == 0 || key == NULL || nonce == NULL) { errno = EINVAL; return; }
	else if (len >= 64)
	{
		if (len >> 6 >= (size_t)UINT32_MAX) { errno = EOVERFLOW; return; }
		else if (len >> 6 > UINT32_MAX - cblock) { errno = EOVERFLOW; return; }
	}
	*/

	len_q = len >> 6;
	len_r = len & 0x3f;

#ifdef __AVX__
	local_chacha_rfc7539_initstate_v(cipherstate.blk128, key, nonce, cblock);
#else
	local_chacha_rfc7539_initstate(cipherstate, key, nonce, cblock);
#endif /* __AVX__ */

	/*
	while (len >= 64)
	*/
	for (index = 0; len_q > 0;)
	{
#ifdef __AVX__
		local_chacha20_rfc7539_getblock_v(keyblk, cipherstate.blk128);
#else
		local_chacha20_rfc7539_getblock(keyblk, cipherstate);
#endif /* __AVX__ */

		for (i = 0; i < 64; i++, index++)
		{
#ifndef __cplusplus
			ciphertext[index] = plaintext[index] ^ keyblk[index];
#else
			ciphertext[index] = static_cast<unsigned char>(plaintext[index] ^ keyblk[index]);
#endif /* __cplusplus */
		}
#ifdef __AVX__
		cipherstate.blk32[12]++;
#else
		cipherstate[12]++;
#endif /* __AVX__ */
		len_q--;
	}

	if (len_r == 0) return;

#ifdef __AVX__
	local_chacha20_rfc7539_getblock_v(keyblk, cipherstate.blk128);
#else
	local_chacha20_rfc7539_getblock(keyblk, cipherstate);
#endif /* __AVX__ */
	for (i = 0; i < len_r; i++, index++)
	{
#ifndef __cplusplus
		ciphertext[index] = plaintext[index] ^ keyblk[i];
#else
		ciphertext[index] = static_cast<unsigned char>(plaintext[index] ^ keyblk[i]);
#endif /* __cplusplus */
	}
	return;
}
