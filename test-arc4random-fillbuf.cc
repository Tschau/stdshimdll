#ifdef  _MSC_VER
#pragma warning(disable : 4101 4189 4255 4668 4710 4820 )
#pragma setlocale( "C" )
#endif

#include <windows.h>

#include <sstream>
#include <iomanip>
#include <system_error>

#include <cstdlib>
#include <cstdio>
#include <cstdint>

#include <fcntl.h>
#include <io.h>

#include "cryptoshim.h"

int wmain(void) noexcept
{
	std::ostringstream stream;

	long i;
	unsigned char a[32768];

	std::ios_base::sync_with_stdio(false);

	try
	{
		arc4random_buf(a, sizeof(a));
	}
	catch (const std::system_error& e)
	{
		stream << e.what() << '\n' << std::ends;
		puts(std::move(stream.str()).data());
		stream.seekp(0);
		return EXIT_FAILURE;
	}
	for (i = 0; i < sizeof(a); i++)
	{
		stream << "0x" << std::hex << (uintptr_t)(i + a) << ": " << "a: 0x" << i << ": " << "0x" << std::hex << (0xff & a[i]);
		stream << std::ends;
		puts(std::move(stream.str()).data());
		stream.seekp(0);
	}

	return EXIT_SUCCESS;
}
