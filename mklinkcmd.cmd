whoami /priv /fo csv /nh | findstr SeCreateSymbolicLinkPrivilege > NUL
if %ERRORLEVEL% NEQ 0 (
    rem SeCreateSymbolicLinkPrivilege is required.
    exit /b %ERRORLEVEL%
) else (
	mklink stdioshim.h stdioshim\inc.stdioshim.h
	attrib +r stdioshim.h /l
	mklink stdioshim.lib stdioshim\stdioshim.lib
	attrib +r stdioshim.lib /l
	mklink stdioshim.dll stdioshim\stdioshim.dll
	attrib +r stdioshim.dll /l
	mklink cryptoshim.h cryptoshim\inc.cryptoshim.h
	attrib +r cryptoshim.h /l
	mklink cryptoshim.lib cryptoshim\cryptoshim.lib
	attrib +r cryptoshim.lib /l
	mklink cryptoshim.dll cryptoshim\cryptoshim.dll
	attrib +r cryptoshim.dll /l
	pushd stdioshim
	mklink GetClArch.c ..\GetClArch.c
	attrib +r GetClArch.c /l
	mklink cleanexe.c ..\cleanexe.c
	attrib +r cleanexe.c /l
	popd
	pushd cryptoshim
	mklink GetClArch.c ..\GetClArch.c
	attrib +r GetClArch.c /l
	mklink cleanexe.c ..\cleanexe.c
	attrib +r cleanexe.c /l
	mklink local-chacha20-rfc7539.h ..\local-chacha20-rfc7539.h
	attrib +r local-chacha20-rfc7539.h /l
	popd
)