#ifdef  _MSC_VER
#pragma warning( disable : 4255 4668 4710 4820 )
#pragma setlocale( "C" )
#endif

#include <sstream>
#include <iomanip>

#include <windows.h>

#include <cstdlib>
#include <cstdio>
#include <cstdint>

#include <fcntl.h>
#include <io.h>

#ifdef __AVX__
#include <intrin.h>
#endif

#ifdef __AVX__
#define LOCAL_CHACHA_QROUND_VEC128_CORE
#define LOCAL_CHACHA20_RFC7539_GETBLOCK_VEC128_CORE
#endif
#include "local-chacha20-rfc7539.h"

std::ostringstream stream;

template<typename T>
void c0(T dst, T src) {}

template<>
void c0<uint32_t*>(uint32_t *dst, uint32_t *src)
{
	long i;
	for (i = 0; i < 16; i++)
	{
		dst[i] = src[i];
	}
}

#ifdef __AVX__
template<>
void c0<__m128i*>(__m128i *dst, __m128i *src)
{
	long i;
	for (i = 0; i < 4; i++)
	{
		dst[i] = src[i];
	}
}
#endif /* __AVX__ */

template<typename T>
void w0(T s) {}

template<>
void w0<uint32_t*>(uint32_t *s)
{
	long i;
	for (i = 0; i < 4; i++)
	{
		stream << std::setw(8) << s[i << 2] << ' ';
		stream << std::setw(8) << s[1 | (i << 2)] << ' ';
		stream << std::setw(8) << s[2 | (i << 2)] << ' ';
		stream << std::setw(8) << s[3 | (i << 2)];
		stream << std::ends;
		puts(std::move(stream.str()).data());
		stream.seekp(0);
	}
}

#ifdef __AVX__
template<>
void w0<__m128i*>(__m128i *s)
{
	long i;
	for (i = 0; i < 4; i++)
	{
		stream << std::setw(8) << ((uint32_t*)s)[i << 2] << ' ';
		stream << std::setw(8) << ((uint32_t*)s)[1 | (i << 2)] << ' ';
		stream << std::setw(8) << ((uint32_t*)s)[2 | (i << 2)] << ' ';
		stream << std::setw(8) << ((uint32_t*)s)[3 | (i << 2)];
		stream << std::ends;
		puts(std::move(stream.str()).data());
		stream.seekp(0);
	}
}
#endif /* __AVX__ */

void w1(unsigned char *p)
{
	long i, j;

	for (i = 0; i < 7; i++)
	{
		for (j = 0; j < 16; j++)
		{
			stream << std::setw(2) << (0xff & p[(i << 4) ^ j]);
			if (j != 15) stream << ' ';
		}
		stream << std::ends;
		puts(std::move(stream.str()).data());
		stream.seekp(0);
	}
	stream << std::setw(2) << (0xff & p[112]) << ' ';
	stream << std::setw(2) << (0xff & p[113]);
	stream << std::ends;
	puts(std::move(stream.str()).data());
	stream.seekp(0);
}

int wmain(void)
{
#ifdef __AVX__
	__m128i s[4], local_s[4];
#endif /* __AVX__ */
	long i;
	uint32_t cblock = 1;
#ifndef __AVX__
	uint32_t s[16], local_s[16];
#endif /* __AVX__ */
	unsigned char key[32] = {
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
		0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
		0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
		0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f
	};
	unsigned char nonce[12] = {
		0x00, 0x00, 0x00, 0x09,
		0x00, 0x00, 0x00, 0x4a,
		0x00, 0x00, 0x00, 0x00
	};
	unsigned char plaintext[] = {
		0x4c, 0x61, 0x64, 0x69, 0x65, 0x73, 0x20, 0x61,
		0x6e, 0x64, 0x20, 0x47, 0x65, 0x6e, 0x74, 0x6c,
		0x65, 0x6d, 0x65, 0x6e, 0x20, 0x6f, 0x66, 0x20,
		0x74, 0x68, 0x65, 0x20, 0x63, 0x6c, 0x61, 0x73,
		0x73, 0x20, 0x6f, 0x66, 0x20, 0x27, 0x39, 0x39,
		0x3a, 0x20, 0x49, 0x66, 0x20, 0x49, 0x20, 0x63,
		0x6f, 0x75, 0x6c, 0x64, 0x20, 0x6f, 0x66, 0x66,
		0x65, 0x72, 0x20, 0x79, 0x6f, 0x75, 0x20, 0x6f,
		0x6e, 0x6c, 0x79, 0x20, 0x6f, 0x6e, 0x65, 0x20,
		0x74, 0x69, 0x70, 0x20, 0x66, 0x6f, 0x72, 0x20,
		0x74, 0x68, 0x65, 0x20, 0x66, 0x75, 0x74, 0x75,
		0x72, 0x65, 0x2c, 0x20, 0x73, 0x75, 0x6e, 0x73,
		0x63, 0x72, 0x65, 0x65, 0x6e, 0x20, 0x77, 0x6f,
		0x75, 0x6c, 0x64, 0x20, 0x62, 0x65, 0x20, 0x69,
		0x74, 0x2e, 0x00
	};
	unsigned char ciphertext[sizeof(plaintext)];

	*ciphertext = '\0';

	std::ios_base::sync_with_stdio(false);

	puts("BEGIN: Section 2.3.2");

#ifdef __AVX__
	local_chacha_rfc7539_initstate_v(s, key, nonce, cblock);
	c0<__m128i*>(local_s, s);
#else /* __AVX__ */
	local_chacha_rfc7539_initstate(s, key, nonce, cblock);
	c0<uint32_t*>(local_s, s);
#endif /* __AVX__ */
	puts("ChaCha state with the key setup found in Section 2.3.2 of RFC7539.");
	stream << std::setfill('0') << std::hex;
#ifdef __AVX__
		w0<__m128i*>(s);
#else
		w0<uint32_t*>(s);
#endif /* __AVX__ */
	puts("END\n");

#ifdef __AVX__
	local_chacha20_rfc7539_getblock_vec128_core(s);
#else /* __AVX__ */
	local_chacha20_rfc7539_getblock_core(s);
#endif /* __AVX__ */
	puts("After running 20 rounds, ChaCha state looks like this:");
#ifdef __AVX__
	w0<__m128i*>(s);
#else
	w0<uint32_t*>(s);
#endif /* __AVX__ */
	puts("END\n");

#ifdef __AVX__
	for (i = 0; i < 4; i++) s[i] = _mm_add_epi32(local_s[i], s[i]);
#else /* __AVX__ */
	for (i = 0; i < 16; i++) s[i] += local_s[i];
#endif /* __AVX__ */
	puts("ChaCha state at the end of ChaCha20 operation:");
#ifdef __AVX__
	w0<__m128i*>(s);
#else
	w0<uint32_t*>(s);
#endif /* __AVX__ */
	puts("END");
	puts("END: Section 2.3.2\n");

	puts("BEGIN: Section 2.4.2");

	nonce[3] = 0x00;

	local_chacha20_rfc7539_xor(ciphertext, plaintext, sizeof(plaintext) - 1, key, nonce, cblock);

	puts("plaintext:");
	w1(plaintext);
	putc('\n', stdout);

	puts("ciphertext:");
	w1(ciphertext);

	puts("END: Section 2.4.2");

	return EXIT_SUCCESS;
}
