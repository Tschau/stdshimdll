#ifdef  _MSC_VER
#pragma warning(disable : 4255 4668 4710 4820 )
#pragma setlocale( "C" )
#endif

#include <windows.h>
#include <strsafe.h>

#include <fcntl.h>
#include <io.h>

#include <stdio.h>
#include <wchar.h>

#define TERMINAL_VT100_CCHWIDTH 80
#define CLEANEXE_FILENAME_WSTR L"cleanexe.exe"
#define CLEANEXE_FILENAME_CCHLENGTH_W_NULL (sizeof(CLEANEXE_FILENAME_WSTR)/sizeof(wchar_t))
#define CLEANEXE_FILENAME_CCHLENGTH_WO_NULL (CLEANEXE_FILENAME_CCHLENGTH_W_NULL - 1)

/*
 * PathBuffer is expected to be L"*.obj", L"*.exp", L"*.lib", L"*.dll", L"*.exe", or L"*.pdb",
 * in that particular order.
 */
static DWORD cleanexe_cleansuffix(const wchar_t *__restrict PathBuffer)
{
	HANDLE hFile = NULL, hHeap = NULL;
	WIN32_FIND_DATAW nfoEntry;
	DWORD MessageId = ERROR_SUCCESS;
	int ret = CSTR_EQUAL;
	size_t CchLength = 0;
	wchar_t *FmtBuffer = NULL;
	DWORD FmtCchLength = 0;

	SecureZeroMemory(&nfoEntry, sizeof(WIN32_FIND_DATA));

	if (NULL == PathBuffer)
	{
		wprintf_s(L"NULL as PathBuffer is passed to cleanexe_cleansuffix.\n");
		MessageId = ERROR_INVALID_PARAMETER;
		goto jreturn;
	}

	hFile = FindFirstFileExW(PathBuffer, FindExInfoBasic, &nfoEntry, FindExSearchNameMatch, NULL, FIND_FIRST_EX_CASE_SENSITIVE);
	if (INVALID_HANDLE_VALUE == hFile || NULL == hFile)
	{
		MessageId = GetLastError();
		FmtCchLength = 1 + TERMINAL_VT100_CCHWIDTH - sizeof(L"FindFirstFileExW: ") / sizeof(wchar_t) + 1;
		hHeap = GetProcessHeap();
		if (NULL == hHeap) { MessageId = GetLastError(); goto jreturn; }
		FmtBuffer = HeapAlloc(hHeap, HEAP_ZERO_MEMORY, FmtCchLength*sizeof(wchar_t));
		if (NULL == FmtBuffer) { MessageId = GetLastError(); goto jreturn; }
		else if (FormatMessageW(
			FORMAT_MESSAGE_FROM_SYSTEM | TERMINAL_VT100_CCHWIDTH,
			NULL,
			MessageId,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL),
			FmtBuffer,
			FmtCchLength,
			NULL
			))
		{
			(void)fwprintf_s(stderr, L"FindFirstFileExW on %s: %s\n", PathBuffer, FmtBuffer);
			goto jreturn_w_HeapFree;
		}
		else
		{
			MessageId = GetLastError();
			goto jreturn_w_HeapFree;
		}
	}
	do
	{
		if (NULL == nfoEntry.cFileName) break;
		else if (FILE_ATTRIBUTE_DIRECTORY & nfoEntry.dwFileAttributes) continue;
		(void)StringCchLengthW(nfoEntry.cFileName, MAX_PATH, &CchLength);
		if (CLEANEXE_FILENAME_CCHLENGTH_WO_NULL == CchLength)
		{
			ret = CompareStringEx(
				LOCALE_NAME_INVARIANT,
				NORM_LINGUISTIC_CASING,
				CLEANEXE_FILENAME_WSTR,
				CLEANEXE_FILENAME_CCHLENGTH_WO_NULL,
				nfoEntry.cFileName,
				CLEANEXE_FILENAME_CCHLENGTH_WO_NULL,
				NULL,
				NULL,
				0
				);
			if (CSTR_EQUAL == ret) continue;
		}
		if (!DeleteFileW(nfoEntry.cFileName)) (void)wprintf_s(L"FAILURE:\t%s\n", nfoEntry.cFileName);
		else (void)wprintf_s(L"SUCCESS:\t%s\n", nfoEntry.cFileName);
	} while (FindNextFileW(hFile, &nfoEntry));
	MessageId = GetLastError();
	(void)CloseHandle(hFile); hFile = NULL;
	if (ERROR_NO_MORE_FILES != MessageId)
	{
		FmtCchLength = 1 + TERMINAL_VT100_CCHWIDTH - sizeof(L"FindNextFileW: ") / sizeof(wchar_t) + 1;
		FmtBuffer = HeapAlloc(hHeap, HEAP_ZERO_MEMORY, FmtCchLength*sizeof(wchar_t));
		if (NULL == FmtBuffer)
		{
			MessageId = GetLastError();
			goto jreturn;
		}
		else if (FormatMessageW(
			FORMAT_MESSAGE_FROM_SYSTEM | TERMINAL_VT100_CCHWIDTH,
			NULL,
			MessageId,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL),
			FmtBuffer,
			FmtCchLength,
			NULL
			))
		{
			(void)fwprintf_s(stderr, L"FindNextFileW on %s: %s\n", PathBuffer, FmtBuffer);
			goto jreturn_w_HeapFree;
		}
		else
		{
			MessageId = GetLastError();
			goto jreturn_w_HeapFree;
		}
	}
	else
	{
		SetLastErrorEx(ERROR_SUCCESS, 0);
		MessageId = ERROR_SUCCESS;
		goto jreturn;
	}

jreturn_w_HeapFree:
	(void)HeapFree(hHeap, 0, FmtBuffer); FmtBuffer = NULL;
	goto jreturn;

jreturn:
	return MessageId;
}

int wmain(void)
{
	DWORD MessageId = ERROR_SUCCESS;
	HANDLE hConsole = NULL;
	DWORD ConsoleMode = 0;

	/* stdout is made certain to be "CONOUT$". */
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	if (INVALID_HANDLE_VALUE == hConsole || NULL == hConsole)
	{
		MessageId = GetLastError();
		return MessageId;
	}
	if (!GetConsoleMode(hConsole, &ConsoleMode))
	{
		MessageId = GetLastError();
		return MessageId;
	}
	if (-1 == _setmode(_fileno(stdout), _O_WTEXT))
	{
		MessageId = GetLastError();
		return MessageId;
	}

	/* stderr is made certain to be "CONOUT$". */
	hConsole = GetStdHandle(STD_ERROR_HANDLE);
	if (INVALID_HANDLE_VALUE == hConsole || NULL == hConsole)
	{
		MessageId = GetLastError();
		return MessageId;
	}
	if (!GetConsoleMode(hConsole, &ConsoleMode))
	{
		MessageId = GetLastError();
		return MessageId;
	}
	if (-1 == _setmode(_fileno(stderr), _O_WTEXT))
	{
		MessageId = GetLastError();
		return MessageId;
	}

	MessageId = cleanexe_cleansuffix(L"*.obj");
	if (ERROR_NOT_ENOUGH_MEMORY == MessageId) goto jexit;
	MessageId = cleanexe_cleansuffix(L"*.exp");
	if (ERROR_NOT_ENOUGH_MEMORY == MessageId) goto jexit;
	MessageId = cleanexe_cleansuffix(L"*.lib");
	if (ERROR_NOT_ENOUGH_MEMORY == MessageId) goto jexit;
	MessageId = cleanexe_cleansuffix(L"*.dll");
	if (ERROR_NOT_ENOUGH_MEMORY == MessageId) goto jexit;
	MessageId = cleanexe_cleansuffix(L"*.exe");
	if (ERROR_NOT_ENOUGH_MEMORY == MessageId) goto jexit;
	MessageId = cleanexe_cleansuffix(L"*.pdb");
	if (ERROR_NOT_ENOUGH_MEMORY == MessageId) goto jexit;

jexit:
	return MessageId;
}
