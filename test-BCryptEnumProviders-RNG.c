#ifdef  _MSC_VER
#pragma warning(disable : 4255 4668 4820 )
#pragma setlocale( "C" )
#endif

#include <windows.h>

#include <stdlib.h>
#include <wchar.h>

#include "stdioshim.h"

NTSTATUS wmain(void)
{
	NTSTATUS Status = EXIT_SUCCESS;
	ULONG ImplCount = 0, i = 0;
	BCRYPT_PROVIDER_NAME *ImplList = NULL;

	Status = BCryptEnumProviders(BCRYPT_RNG_ALGORITHM, &ImplCount, &ImplList, 0);
	if (NULL == ImplList)
	{
		printf("BCryptEnumProviders() failed.\n");
		return Status;
	}
	printf("ImplCount: %lu\n", ImplCount);
	printf("BEGIN ImplList\n");
	for (i = 0; i < ImplCount; i++)
	{
		wprintf(L"%s\n", ImplList[i]);
	}
	printf("END ImplList\n");
	BCryptFreeBuffer(ImplList); ImplList = NULL;

	return Status;
}
