!include own.mak

all: prog.exe cleanexe.exe

prog.exe: prog.obj
!ifndef NDEBUG
	$(LINK) $(LINKFLAGS) /incremental:no /out:prog.exe /pdb:prog.pdb prog.obj
!else
	$(LINK) $(LINKFLAGS) /incremental:no /out:prog.exe prog.obj
!endif

#prog.exe: prog.obj stdioshim.dll
#!ifndef NDEBUG
#	$(LINK) $(LINKFLAGS) /incremental:no /out:prog.exe /pdb:prog.pdb prog.obj stdioshim.lib
#!else
#	$(LINK) $(LINKFLAGS) /incremental:no /out:prog.exe prog.obj stdioshim.lib
#!endif

cleanexe.exe: cleanexe.c
	$(CC) $(CFLAGS) $(CEFLAGS) $(RTCFLAGS) cleanexe.c /link $(LINKFLAGS) /incremental:no user32.lib

test-arc4random.exe: test-arc4random.obj local-init.obj cryptoshim.dll
	$(LINK) $(LINKFLAGS) /incremental:no /out:test-arc4random.exe local-init.obj test-arc4random.obj cryptoshim.lib

test-arc4random-fillbuf.exe: test-arc4random-fillbuf.obj local-init.obj cryptoshim.dll
	$(LINK) $(LINKFLAGS) /incremental:no /out:test-arc4random-fillbuf.exe local-init.obj test-arc4random-fillbuf.obj cryptoshim.lib

prog.obj: prog.c
	$(CC) $(CFLAGS) $(CEFLAGS) $(RTCFLAGS) /c prog.c

test-arc4random.obj: test-arc4random.cc
	$(CXX) $(CXXFLAGS) $(CXXEFLAGS) $(CXXRTCFLAGS) /c test-arc4random.cc

test-arc4random-fillbuf.obj: test-arc4random-fillbuf.cc
	$(CXX) $(CXXFLAGS) $(CXXEFLAGS) $(CXXRTCFLAGS) /c test-arc4random-fillbuf.cc

clean:
	del /q /a:-r *.obj *.exp *.lib *.dll *.exe *.pdb

test: test-BCryptEnumProviders-RNG.exe test-BCryptGenRandom.exe test-getentropy.exe test-chacha20-rfc7539.exe test-arc4random.exe test-arc4random-fillbuf.exe

test-BCryptEnumProviders-RNG.exe: test-BCryptEnumProviders-RNG.obj stdioshim.dll
	$(LINK) $(LINKFLAGS) /incremental:no /out:test-BCryptEnumProviders-RNG.exe test-BCryptEnumProviders-RNG.obj stdioshim.lib bcrypt.lib

test-BCryptGenRandom.exe: test-BCryptGenRandom.obj stdioshim.dll
	$(LINK) $(LINKFLAGS) /incremental:no /out:test-BCryptGenRandom.exe test-BCryptGenRandom.obj stdioshim.lib bcrypt.lib

test-getentropy.exe: test-getentropy.obj cryptoshim.dll stdioshim.dll
 	$(LINK) $(LINKFLAGS) /incremental:no /out:test-getentropy.exe test-getentropy.obj cryptoshim.lib stdioshim.lib

test-chacha20-rfc7539.exe: test-chacha20-rfc7539.obj local-init.obj
	$(LINK) $(LINKFLAGS) /incremental:no /out:test-chacha20-rfc7539.exe local-init.obj test-chacha20-rfc7539.obj

test-BCryptEnumProviders-RNG.obj: test-BCryptEnumProviders-RNG.c stdioshim.dll
	$(CC) $(CFLAGS) $(CEFLAGS) $(RTCFLAGS) /c test-BCryptEnumProviders-RNG.c

test-BCryptGenRandom.obj: test-BCryptGenRandom.c stdioshim.dll
	$(CC) $(CFLAGS) $(CEFLAGS) $(RTCFLAGS) /c test-BCryptGenRandom.c

test-getentropy.obj: test-getentropy.c
	$(CC) $(CFLAGS) $(CEFLAGS) $(RTCFLAGS) /c test-getentropy.c

test-chacha20-rfc7539.obj: test-chacha20-rfc7539.cc local-chacha20-rfc7539.h
	$(CXX) $(CXXFLAGS) $(CXXEFLAGS) $(CXXRTCFLAGS) /c test-chacha20-rfc7539.cc

local-init.obj: local-init.cc
	$(CXX) $(CXXFLAGS) $(CXXEFLAGS) $(CXXRTCFLAGS) /c local-init.cc