if exist cleanexe.exe (
	nmake /nologo /f local.mak clean
	del cleanexe.exe
) else (
	del /q /a:-r *.obj *.exp *.lib *.dll *.exe *.pdb
)