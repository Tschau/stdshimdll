echo off
setlocal EnableDelayedExpansion
IF EXIST .\_WIN.lock (
	IF EXIST .\_WIN64 (
		nmake /nologo /errorreport:none /f local.mak _WIN64= all
		exit /b !ERRORLEVEL!
	) ELSE (
		nmake /nologo /errorreport:none /f local.mak all
		exit /b !ERRORLEVEL!
	)
) ELSE (
	cl /nologo /TC /Fe:.\GetClArch.exe GetClArch.c 1>NUL 2>NUL
	IF !ERRORLEVEL! EQU 0 (
		.\GetClArch.exe
		IF !ERRORLEVEL! EQU 0 (
			copy /y NUL _WIN64 1>NUL 2>NUL
			copy /y NUL _WIN.lock 1>NUL 2>NUL
			nmake /nologo /errorreport:none /f local.mak _WIN64= all
			exit /b !ERRORLEVEL!
		) ELSE (
			copy /y NUL _WIN32 1>NUL 2>NUL
			copy /y NUL _WIN.lock 1>NUL 2>NUL
			nmake /nologo /errorreport:none /f local.mak all
			exit /b !ERRORLEVEL!
		)
	)
)
endlocal