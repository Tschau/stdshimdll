#ifdef  _MSC_VER
#pragma warning(disable : 4255 4668 4820 )
#pragma setlocale( "C" )
#endif

#include <windows.h>

#include <stdlib.h>
#include <wchar.h>
#include <stdint.h>

#include "stdioshim.h"

NTSTATUS wmain(void)
{
	NTSTATUS Status = EXIT_SUCCESS;
	BCRYPT_ALG_HANDLE hAlgorithm = NULL;
	uint32_t a[1024];
	unsigned int i = 0;

	(void)SecureZeroMemory(a, sizeof(a));

	/*
	 * AES counter mode is used per https://msdn.microsoft.com/en-us/library/windows/desktop/aa375534.aspx . 
	 *
	 * MS_PRIMITIVE_PROVIDER is dictated per https://msdn.microsoft.com/en-us/library/windows/desktop/bb204776.aspx .
	 */
	Status = BCryptOpenAlgorithmProvider(&hAlgorithm, BCRYPT_RNG_ALGORITHM, MS_PRIMITIVE_PROVIDER, 0);
	if (NULL == hAlgorithm) return Status;
	for (i = 0; i < 1024; i++)
	{
		(void)BCryptGenRandom(hAlgorithm, (PUCHAR)&a[i], sizeof(uint32_t), 0);
		printf("a[%04u]: 0x%08lx\n", i, a[i]);
	}
	Status = BCryptCloseAlgorithmProvider(hAlgorithm, 0); hAlgorithm = NULL;

	return Status;
}
