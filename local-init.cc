#ifdef  _MSC_VER
#pragma warning(disable : 4668 4820 )
#pragma setlocale( "C" )
#endif

#include <windows.h>

#include <cstdlib>
#include <cstdio>

#include <fcntl.h>
#include <io.h>

#pragma init_seg( user )
namespace
{
	namespace declaration
	{
		struct globalstate
		{
			explicit globalstate(void) noexcept;
			~globalstate(void) noexcept;
			globalstate operator =(globalstate) = delete;

			DWORD Status = 0;
			UINT CodePage = 0xfde9;
		};
	}
	namespace definition
	{
		::declaration::globalstate instance;
	}
}

::declaration::globalstate::globalstate(void) noexcept
{
	HANDLE hConsole;
	DWORD ConsoleMode;

	(void)_set_fmode(_O_BINARY);

	hConsole = GetStdHandle(STD_ERROR_HANDLE);
	if (NULL == hConsole) return;
	else if (INVALID_HANDLE_VALUE != hConsole)
	{
		if (GetConsoleMode(hConsole, &ConsoleMode))
		{
			(void)_setmode(_fileno(stderr), _O_TEXT);
			Status |= (1);
		}
		else Status |= (2);
	}

	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	if (NULL == hConsole) return;
	else if (INVALID_HANDLE_VALUE != hConsole)
	{
		if (GetConsoleMode(hConsole, &ConsoleMode))
		{
			(void)_setmode(_fileno(stdout), _O_TEXT);
			Status |= (4);
		}
		else Status |= (8);
	}

	if ((1 & Status) || 0 < (4 & Status))
	{
		if (IsValidCodePage(0xfde9))
		{
			CodePage = GetConsoleOutputCP();
			if (SetConsoleOutputCP(0xfde9))
			{
				Status |= 16;
			}
		}
	}
}

::declaration::globalstate::~globalstate(void) noexcept
{
	if (16 & Status)
	{
		(void)SetConsoleOutputCP(CodePage);
	}
}
